function [costfun,gradfun] = negLogLikelihood_sum(man,costfun_unit,gradfun_unit)
    costfun = @(theta,x) aux_cost(theta,x,costfun_unit);
    gradfun = @(theta,x) aux_grad(theta,x,man,gradfun_unit);
end

function cost = aux_cost(theta,x,costfun_unit)
    t    = size(x,3);
    cost = 0;
    for tix=1:t
        cost = cost+costfun_unit(theta,x(:,:,tix))/t;
    end
end

function grad = aux_grad(theta,x,man,gradfun_unit)
    t = size(x,3);
    grad = man.zerovec();
    for tix=1:t
        grad_unit = gradfun_unit(theta,x(:,:,tix));
        grad      = man.lincomb(theta,1,grad,1/t,grad_unit);
    end
end
