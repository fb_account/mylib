%% parameters
p=10;
n=15;
t=10;

%% random parameters
U     = orth(randn(p)+1i*randn(p));
L     = diag(randn(p,1).^2);
L     = L / (prod(diag(L))^(1/p));
Sigma = U*L*U';

tau = gamrnd(1,1,n,1);

theta = struct('Sigma',Sigma,'tau',tau);


%% some data
x      = zeros(p,n,t);
S_sqrt = U*diag(diag(L).^.5)*U';
for i=1:n
    x(:,i,:) = sqrt(tau(i))*S_sqrt*(randn(p,t)+1i*randn(p,t))/sqrt(2);
end

%% check gradients and stuff

man = cGfactory(p,n);
[costfun,gradfun] = cG_negLogLikelihood();

problem = struct('M',man,'cost',@(theta) costfun(theta,x(:,:,1)),'grad',@(theta) gradfun(theta,x(:,:,1)));
figure;
checkgradient(problem);


[costfun,gradfun] = negLogLikelihood_sum(man,costfun,gradfun);

problem = struct('M',man,'cost',@(theta) costfun(theta,x),'grad',@(theta) gradfun(theta,x));
figure;
checkgradient(problem);
