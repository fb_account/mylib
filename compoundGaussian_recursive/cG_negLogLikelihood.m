function [costfun,rgradfun] = cG_negLogLikelihood()
    costfun  = @aux_cost;
    rgradfun = @aux_rgrad;
end

function cost = aux_cost(theta,x)
    [p,n]   = size(x);
    [U,L]   = svd(theta.Sigma);
    S_isqrt = U*diag(diag(L).^-.5)*U';
    v       = S_isqrt*x;
    a       = real(sum(v.*conj(v))) ./ theta.tau';
    %
    cost  = p*sum(log(theta.tau)) + sum(a);
end

function rgrad = aux_rgrad(theta,x)
    [p,n]   = size(x);
    [U,L]   = svd(theta.Sigma);
    S_isqrt = U*diag(diag(L).^-.5)*U';
    v       = S_isqrt*x;
    a       = real(sum(v.*conj(v)))';
    lambda  = sum(a./ theta.tau)/p;
    %
    y       = x./ sqrt(theta.tau(:,ones(p,1))');
    %
    rgradS = lambda*theta.Sigma - y*y';
    rgradt = p*theta.tau - a;
    %
    rgrad = struct('Sigma',p*rgradS,'tau',n*rgradt);
end