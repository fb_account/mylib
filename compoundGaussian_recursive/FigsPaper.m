%mix = 1 % arithmetic
%mix = 2 % recursif
% nix=1 % n=20
% nix=2 % n=50

clear all
close all

load results_p10_n20_n50_time

%% Display shape
figure(1)
semilogx(t_all,10*log10(mean(squeeze(err_theta_est(1,:,:)),2)),'b-*'); % MLE
hold on
semilogx(t_all,10*log10(mean(squeeze(err_theta_rec(1,1,t_all,:)),2)),'s-r'); % Arithmetic Mean
semilogx(t_all,10*log10(mean(squeeze(err_theta_rec(1,2,t_all,:)),2)),'o-g'); % Recursif
semilogx(1:10:t_max,10*log10((p^2-1+n_all(1))/n_all(1)/p./(1:10:t_max)),'k')
axis([1 1000 -33 0.60])
grid on
ylabel('Natural MSE (dB)')
xlabel('T')
h=legend('MLE','Arithmetic mean','Proposed recursive method','ICRB');
title(['p=10 and n=' int2str(n_all(1))])
set(h,'Interpreter','latex')
hold off

figure(2)
semilogx(t_all,10*log10(mean(squeeze(err_theta_est(2,:,:)),2)),'b-*'); % MLE
hold on
semilogx(t_all,10*log10(mean(squeeze(err_theta_rec(2,1,t_all,:)),2)),'s-r'); % Arithmetic Mean
semilogx(t_all,10*log10(mean(squeeze(err_theta_rec(2,2,t_all,:)),2)),'o-g'); % Recursif
semilogx((1:10:t_max),10*log10((p^2-1+n_all(2))/n_all(2)/p./(1:10:t_max)),'k')
axis([1 1000 -36 -3.80])
grid on
ylabel('Natural MSE (dB)')
xlabel('T')
h=legend('MLE','Arithmetic mean','Proposed recursive method','ICRB');
title(['p=10 and n=' int2str(n_all(2))])
set(h,'Interpreter','latex')
hold off

%% computation time
figure(3)
semilogx((t_all),log10(mean(squeeze(time_est(1,:,:)),2)),'b-*');
hold on;
semilogx((t_all),log10(mean(squeeze(time_rec(1,1,t_all,:)),2)),'s-r'); % Arithmetic Mean
semilogx((t_all),log10(mean(squeeze(time_rec(1,2,t_all,:)),2)),'o-g'); % recursif
%axis([1 1000 -36 -3.80])
grid on
ylabel('log10(t)')
xlabel('T')
h=legend('MLE','Arithmetic mean','Proposed recursive method');
title(['p=10 and n=' int2str(n_all(1))])
set(h,'Interpreter','latex')
hold off

figure(4)
plot(log10(t_all),log10(mean(squeeze(time_est(2,:,:)),2)),'b-*');
hold on;
plot(log10(t_all),log10(mean(squeeze(time_rec(2,1,t_all,:)),2)),'s-r'); % Arithmetic Mean
plot(log10(t_all),log10(mean(squeeze(time_rec(2,2,t_all,:)),2)),'o-g'); % recursif
%axis([1 1000 -36 -3.80])
grid on
ylabel('log10(t)')
xlabel('T')
h=legend('MLE','Arithmetic mean','Proposed recursive method');
title(['p=10 and n=' int2str(n_all(2))])
set(h,'Interpreter','latex')
hold off

%%
load results_p20_n40_n100_time

figure(5)
semilogx(t_all,10*log10(mean(squeeze(err_theta_est(1,:,:)),2)),'b-*'); % MLE
hold on
semilogx(t_all,10*log10(mean(squeeze(err_theta_rec(1,1,t_all,:)),2)),'s-r'); % Arithmetic Mean
semilogx(t_all,10*log10(mean(squeeze(err_theta_rec(1,2,t_all,:)),2)),'o-g'); % Recursif
semilogx(1:10:t_max,10*log10((p^2-1+n_all(1))/n_all(1)/p./(1:10:t_max)),'k')
axis([1 1000 -33 0.60])
grid on
ylabel('Natural MSE (dB)')
xlabel('T')
h=legend('MLE','Arithmetic mean','Proposed recursive method','ICRB');
title(['p=20 and n=' int2str(n_all(1))])
set(h,'Interpreter','latex')
hold off


% Footnotesize dans chaque {} des adslegendentry
% legend style={at={(0,0)}, anchor=south west, legend cell align=left, align=left, draw=black}]
% matlab2tikz('myfigure.tex','width', '\fwidth' );

% - exemple pour inclure dans le tex
% \begin{figure}[!t]
% \setlength\fwidth{0.38\textwidth}
% \begin{center}
% \input{figures/MMconvergence/vs_n_it_r1.tex}
% \end{center}
% \caption{Distance to optimal value w.r.t. number of iterations and execution time. $M=20$, $R=1$}
% \label{fig:convergence_r1}
% \end{figure}