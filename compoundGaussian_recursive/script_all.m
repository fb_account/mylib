%% parameters
p     = 20; % 10
n_all = [40,100]; % [20,50]
n_max = max(n_all);
nN    = length(n_all);
t_all = [1,3,10,30,100,300,1000];
t_max = max(t_all);
nT    = length(t_all);
nIt   = 10;

%% data
% parameter
U     = orth(randn(p)+1i*randn(p));
L     = diag(randn(p,1).^2);
L     = L / (prod(diag(L))^(1/p));
Sigma = U*L*U';

% rho = 0.9 * (1+1i)/sqrt(2) ;
% vec_rho = rho.^( 0:1:(p-1) ) ;
% Sigma = toeplitz(vec_rho);
% for i=1:p % just to avoid some numerical issues with cholesky
%     Sigma(i,i) = real(Sigma(i,i));
% end
% Sigma = Sigma / (real(det(Sigma))^(1/p));

tau = gamrnd(1,1,n_max,1);


% data
x      = zeros(p,n_max,t_max,nIt);
S_sqrt = U*diag(diag(L).^.5)*U';    
for it=1:nIt
    for i=1:n_max
        x(:,i,:,it) = sqrt(tau(i))*S_sqrt*(randn(p,t_max)+1i*randn(p,t_max))/sqrt(2);
    end
end
%% estimation

man2 = specialHPDfactory(p);

err_theta_est = zeros(nN,nT,nIt);
err_Sigma_est = zeros(nN,nT,nIt);
err_tau_est   = zeros(nN,nT,nIt);
%
nM            = 2;
% theta_rec_fun     = [
%   @(problem,theta_rec) man.projonman(struct('Sigma',(1-1/t)*theta_rec.Sigma + 1/t*problem.theta_cur.Sigma,'tau',(1-1/t)*theta_rec.tau + 1/t*problem.theta_cur.tau));
%   @(problem,theta_rec) recursive_grad_update(problem,theta_rec(2));
% ];
err_theta_rec = zeros(nN,nM,t_max,nIt);
err_Sigma_rec = zeros(nN,nM,t_max,nIt);
err_tau_rec   = zeros(nN,nM,t_max,nIt);


% timing stuff
time_est = zeros(nN,nT,nIt);
time_rec = zeros(nN,nM,t_max,nIt);

for nix=1:nN
    n = n_all(nix);
    % true parameter
    theta = struct('Sigma',Sigma,'tau',tau(1:n));
    % manifold
    man                         = cGfactory(p,n);
    % cost functions
    [costfun_unit,gradfun_unit] = cG_negLogLikelihood();
    [costfun_sum,gradfun_sum]   = negLogLikelihood_sum(man,costfun_unit,gradfun_unit);
    % stepsize
    alpha0                      = 1/n/p;
    stepsize                    = @(t) alpha0/(1+t);


    for it=1:nIt
        fprintf(['n= ' int2str(n) '  it= ' int2str(it) '  t= ']);
        for tix=1:nT
            fprintf([int2str(t_all(tix)) ' ']);
            x_cur     = x(:,1:n,1:t_all(tix),it);
            problem   = struct('M',man,'cost',@(theta) costfun_sum(theta,x_cur),'grad',@(theta) gradfun_sum(theta,x_cur));
            
            tic;
            
            theta_est = conjugategradient(problem,struct('Sigma',eye(p),'tau',ones(n,1)),struct('verbosity',0));
            
            time_est(nix,tix,it) = toc;
            
            %
            err_theta_est(nix,tix,it) = man.dist(theta,theta_est)^2;
            err_Sigma_est(nix,tix,it) = man2.dist(theta.Sigma,theta_est.Sigma)^2/p;
            err_tau_est(nix,tix,it)   = err_theta_est(nix,tix,it) - err_Sigma_est(nix,tix,it); 
        end


        fprintf(' recursive estimation')

        theta_cur = struct('Sigma',eye(p),'tau',[]); % no need of tau at initialization
        for t=1:t_max
            x_cur      = squeeze(x(:,1:n,t,it));
            problem    = struct('M',man,'cost',@(theta) costfun_unit(theta,x_cur),'grad',@(theta) gradfun_unit(theta,x_cur));
            
            tic;
            theta_cur  = cG_viaTyler_unit(x_cur,theta_cur.Sigma,struct('verbosity',0));
            time_tmp0  = toc;
                            


            if t==1
                for mix=1:nM
                    theta_rec(mix) = theta_cur;
                    time_rec(nix,mix,t,it) = time_tmp0;
                end
            else                
                % arithmetic mean % could be embedded in function euclidean_geodesic(.,.,1/t)
                tic;
                theta_rec(1) = man.projonman(struct('Sigma',(1-1/t)*theta_rec(1).Sigma + 1/t*theta_cur.Sigma,'tau',(1-1/t)*theta_rec(1).tau + 1/t*theta_cur.tau));
                time_tmp1 = toc;
                time_rec(nix,1,t,it) = time_tmp0 + time_tmp1;
%                 % geometric mean
%                 theta_rec(2) = man.geodesic(theta_rec(2),theta_cur,1/t);
%                 % hybrid : geometric on Sigma, arithmetic on tau
%                 theta_rec(3) = struct('Sigma',man2.geodesic(theta_rec(3).Sigma,theta_cur.Sigma,1/t),'tau',(1-1/t)*theta_rec(3).tau + 1/t*theta_cur.tau);
                % online Salem 1
                tic;
                theta_rec(2) = recursive_grad_update(problem,theta_rec(2),stepsize(t-1));
                time_tmp2 = toc;
                time_rec(nix,2,t,it) = time_tmp2;
            end

            for mix=1:nM
                err_theta_rec(nix,mix,t,it) = man.dist(theta,theta_rec(mix))^2;
                err_Sigma_rec(nix,mix,t,it) = man2.dist(theta.Sigma,theta_rec(mix).Sigma)^2/p;
                err_tau_rec(nix,mix,t,it)   = err_theta_rec(nix,mix,t,it) - err_Sigma_rec(nix,mix,t,it);
            end

        end
        fprintf('\n');
    end
 save results_p20_n40_n100_time
end



%% some figure

for nix=1:nN

figure;

subplot(3,1,1);
plot(log10(t_all),10*log10(mean(squeeze(err_theta_est(nix,:,:)),2)));
hold on;
for mix=1:nM
    plot(log10(1:t_max),10*log10(mean(squeeze(err_theta_rec(nix,mix,:,:)),2)));
end
plot(log10(1:t_max),10*log10((p^2-1+n_all(nix))/n_all(nix)/p./(1:t_max)));

title(['n=' int2str(n_all(nix))]);

subplot(3,1,2);
plot(log10(t_all),10*log10(mean(squeeze(err_Sigma_est(nix,:,:)),2)));
hold on;
for mix=1:nM
    plot(log10(1:t_max),10*log10(mean(squeeze(err_Sigma_rec(nix,mix,:,:)),2)));
end
plot(log10(1:t_max),10*log10((p^2-1)/n_all(nix)/p./(1:t_max)));

subplot(3,1,3);
plot(log10(t_all),10*log10(mean(squeeze(err_tau_est(nix,:,:)),2)));
hold on;
for mix=1:nM
    plot(log10(1:t_max),10*log10(mean(squeeze(err_tau_rec(nix,mix,:,:)),2)));
end
plot(log10(1:t_max),10*log10(1/p./(1:t_max)));

figure;
plot(log10(t_all),10*log10(mean(squeeze(time_est(nix,:,:)),2)));
hold on;
for mix=1:nM
    plot(log10(1:t_max),10*log10(mean(squeeze(time_rec(nix,mix,:,:)),2)));
end
title(['computation time, n=' int2str(n_all(nix))])

end



