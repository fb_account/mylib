function theta = cG_viaTyler_unit(x,Sigma0,options)

if nargin<3, options=[]; end

[p,n] = size(x);
[costfun,gradfun]=tyler_HPD(x);

problem = struct('M',specialHPDfactory(p),'cost',costfun,'egrad',gradfun);

Sigma = conjugategradient(problem,Sigma0,options);

tau = zeros(n,1);
for i=1:n
    tau(i) = real(x(:,i)'*(Sigma\x(:,i)))/p;
end

theta = struct('Sigma',Sigma,'tau',tau);