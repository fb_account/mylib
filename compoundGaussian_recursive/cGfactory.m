function M = cGfactory(p,n)

M.name = @() sprintf('manifold SH^++(%d)xD^++(%d)',p,n);

M.dim = @() p^2-1 + n;

M.inner = @inner;
function in = inner(X,xi,eta)
    in = trace((X.Sigma\xi.Sigma)*(X.Sigma\eta.Sigma))/p;
    in = in + (xi.tau./X.tau)'*(eta.tau./X.tau)/n;
    in = real(in);
end

M.norm = @(X, xi) sqrt(M.inner(X, xi, xi));

M.dist = @dist;
function d=dist(X,Y)
    [U,L]    = svd(X.Sigma);
    XS_isqrt = U*diag(diag(L).^-.5)*U';
    S        = XS_isqrt * Y.Sigma * XS_isqrt;
    d2_S     = sum((log(svd(S))).^2);
    %
    t    = Y.tau ./ X.tau;
    d2_t = sum(log(t).^2);
    %
    d = sqrt(d2_S/p + d2_t/n);
end

M.geodesic = @geodesic;
function Z=geodesic(X,Y,t)
   [U,L]    = svd(X.Sigma);
   XS_sqrt  = U*diag(diag(L).^.5)*U'; 
   XS_isqrt = U*diag(diag(L).^-.5)*U'; 
   [U_,L_]  = svd(XS_isqrt * Y.Sigma * XS_isqrt);
   ZS       = herm(XS_sqrt * (U_ * diag(diag(L_).^t) * U_') * XS_sqrt);
   ZS       = ZS / (det(ZS)^(1/p));
   %
   Zt = (X.tau.^(1-t)).*(Y.tau.^t);
   %
   Z = struct('Sigma',ZS,'tau',Zt);
end

M.typicaldist = @() sqrt(p^2-1 + n);

herm = @(x) (x+x')/2;

M.proj = @projection;
function xi = projection(X, Z)
    xiS = herm(Z.Sigma);
    xiS = xiS - (trace(X.Sigma\xiS)/p)*X.Sigma;
    xi  = struct('Sigma',xiS,'tau',Z.tau);
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    rgradS = X.Sigma * herm(egrad.Sigma) * X.Sigma;
    rgradt = egrad.tau ./ (X.tau.^2);
    rgrad  = projection(X,struct('Sigma',p*rgradS,'tau',n*rgradt));
end

M.retr = @retraction;
function Y = retraction(X,xi,t)
    if nargin<3
        t=1.0;
    end
    YS = herm(X.Sigma + t*xi.Sigma + .5*t^2*(xi.Sigma*(X.Sigma\xi.Sigma)));
    YS = YS/(det(YS)^(1/p));
    Yt = X.tau + t*xi.tau + .5*t^2*((xi.tau.^2) ./ X.tau);
    Y  = struct('Sigma',YS,'tau',Yt);
end

M.exp = @exponential;
function Y = exponential(X,xi,t)
    if nargin<3
        t=1.0;
    end
    [U,L]    = svd(X.Sigma);
    XS_isqrt = U*diag(diag(L).^-.5)*U';
    XS_sqrt  = U*diag(diag(L).^.5)*U';
    [U_,L_]  = eig(XS_isqrt * (t*xi.Sigma) * XS_isqrt);
    YS       = herm(XS_sqrt * (U_*diag(exp(diag(L_)))*U_') * XS_sqrt);
    YS       = YS/(det(YS)^(1/p));
    %
    Yt = X.tau.*(exp(t*xi.tau./X.tau));
    %
    Y = struct('Sigma',YS,'tau',Yt);
end


M.transp = @transport;
function eta2 = transport(x1,x2,eta1)
    eta2 = projection(x2,eta1);
end

M.rand = @random;
function X = random()
    U = orth(randn(p)+1i*randn(p));
    L = randn(p,1).^2;
    L = L / (prod(L)^(1/p));
    X = struct('Sigma', U*diag(L)*U','tau', gamrnd(1,1,n,1));
end

M.randvec = @(X) randomvec(X,M.norm);
function xi = randomvec(X,normfun)
    xiS = herm(randn(p)+1i*randn(p));
    xit = randn(n,1);
    xi  = projection(X,struct('Sigma',xiS,'tau',xit));
    nrm = normfun(X,xi);
    xi  = struct('Sigma',xi.Sigma/nrm,'tau',xi.tau/nrm);
end

M.lincomb = @lincomb;
function d = lincomb(x, a1, d1, a2, d2)
    
    if nargin == 3
        d.Sigma = a1*d1.Sigma;
        d.tau   = a1*d1.tau;
    elseif nargin == 5
        d.Sigma = a1*d1.Sigma + a2*d2.Sigma;
        d.tau   = a1*d1.tau + a2*d2.tau;
    else
        error('Bad use of BSSfactory.lincomb.');
    end
end

M.zerovec = @(X) struct('Sigma', zeros(p), 'tau', zeros(n,1));

M.projonman = @projonman;
function Y=projonman(X)
    scale = real(det(X.Sigma)^(1/p));
    Y     = struct('Sigma', X.Sigma / scale, 'tau', X.tau * scale);
end


end