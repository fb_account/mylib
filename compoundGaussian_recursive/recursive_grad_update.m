function theta = recursive_grad_update(problem,theta0,stepsize)

grad0 = problem.grad(theta0);

theta = problem.M.retr(theta0,grad0,-stepsize);

end