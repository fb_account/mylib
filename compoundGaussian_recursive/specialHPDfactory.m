function M = specialHPDfactory(p)

M.name = @() sprintf('manifold SH^++(%d)',p);

M.dim = @() p^2;

M.inner = @inner;
function in = inner(X,xi,eta)
    in = real(trace((X\xi)*(X\eta)));
end

M.norm = @(X, xi) sqrt(M.inner(X, xi, xi));

M.dist = @dist;
function d=dist(X,Y)
    [U,L]    = svd(X);
    X_isqrt = U*diag(diag(L).^-.5)*U';
    S        = X_isqrt * Y * X_isqrt;
    d2_S     = sum((log(svd(S))).^2);
    %
    d = sqrt(d2_S);
end

M.geodesic = @geodesic;
function Z=geodesic(X,Y,t)
   [U,L]   = svd(X);
   X_sqrt  = U*diag(diag(L).^.5)*U'; 
   X_isqrt = U*diag(diag(L).^-.5)*U'; 
   [U_,L_] = svd(X_isqrt * Y * X_isqrt);
   Z       = herm(X_sqrt * (U_ * diag(diag(L_).^t) * U_') * X_sqrt);
   Z       = Z / (det(Z)^(1/p));
end

M.typicaldist = @() p;

herm = @(x) (x+x')/2;

M.proj = @projection;
function xi = projection(X, Z)
    xi = herm(Z);
    xi = xi - (trace(X\xi)/p)*X;
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    rgrad = X * herm(egrad) * X;
    rgrad = projection(X,rgrad);
end

M.retr = @retraction;
function Y = retraction(X,xi,t)
    if nargin<3
        t=1.0;
    end
    Y = herm(X + t*xi + .5*t^2*(xi*(X\xi)));
    Y = Y/(det(Y)^(1/p));
end

M.exp = @exponential;
function Y = exponential(X,xi,t)
    if nargin<3
        t=1.0;
    end
    [U,L]    = svd(X);
    X_isqrt = U*diag(diag(L).^-.5)*U';
    X_sqrt  = U*diag(diag(L).^.5)*U';
    [U_,L_]  = eig(X_isqrt * (t*xi) * X_isqrt);
    Y       = herm(X_sqrt * (U_*diag(exp(diag(L_)))*U_') * X_sqrt);
    Y       = Y/(det(Y)^(1/p));
end


M.transp = @transport;
function eta2 = transport(x1,x2,eta1)
    eta2 = projection(x2,eta1);
end

M.rand = @random;
function X = random()
    U = orth(randn(p)+1i*randn(p));
    L = randn(p,1).^2;
    L = L / (prod(L)^(1/p));
    X = U*diag(L)*U';
end

M.randvec = @(X) randomvec(X,M.norm);
function xi = randomvec(X,normfun)
    xi  = herm(randn(p)+1i*randn(p));
    xi  = projection(X,xi);
    nrm = normfun(X,xi);
    xi  = xi/nrm;
end

M.lincomb = @lincomb;
function d = lincomb(x, a1, d1, a2, d2)
    
    if nargin == 3
        d = a1*d1;
    elseif nargin == 5
        d = a1*d1 + a2*d2;
    else
        error('Bad use of BSSfactory.lincomb.');
    end
end

M.zerovec = @(X) zeros(p);

end