function Xnorm=normEEG(X,Method,Param)
% Xnorm=normEEG(X,Method,Param)
if nargin<2
    Method='';
end
if strcmp(Method,'fro')
    
    for k=1:size(X,3)
        coef(k)=norm(X(:,:,k),'fro')/length(X(:,:,k));
        Xnorm(:,:,k)=X(:,:,k)/coef(k);
    end
    
elseif strcmp(Method,'baseline')
    if nargin<2 || isempty(Param)
        winBaseline=96:128; % HARDCODED NOT GOOD !!!!!!!!!!!!! ! ! !
    else
        winBaseline=Param;
    end
    for k=1:size(X,3)
        coef(:,:,k)=repmat(mean(X(:,winBaseline,k),2),1,size(X,2)); %compute the baseline
    end
            Xnorm=X-coef;
            
else
    Xnorm=X./repmat(sqrt(var(X,[],2)), [1,size(X,2)]);
    
    for k=1:size(Xnorm,3)
        Xnorm(:,:,k)=Xnorm(:,:,k)-repmat(mean(Xnorm(:,:,k),2),[1 size(Xnorm,2)]);
    end
    
end