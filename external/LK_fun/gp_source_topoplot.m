function gp_source_topoplot(figDIR,EEG,B,D,title_str)
% gp_source_topoplot(figDIR,EEG,B,D)
% eeglab required
if nargin<4
    D=eye(size(B));
    title_str = 'test';
end

[~, localization]=findElectrodes(EEG.ElectrodesName, EEG.ElectrodesName);

nb_columns=5;
nb_rows=floor(size(B,1)/(nb_columns-1))+1
CompLABELS=Generate_Components_Numbers(1:size(D,2))';
FontSizec=24;
[As]=pinv(B)'; %estimated spatial distribution of the components
As=normPOW(As');
% [At]=(pinv(D)'); %estimated temporal distribution of the components
% At=(normEEG(At(:,1:size(As,1))')); %remove sur-numerous temporal components


TemporalCellsc=(1:nb_columns:nb_columns*nb_rows)
subplot(nb_rows,nb_columns,TemporalCellsc);plotEEG(D',4,EEG.Fs,CompLABELS);xlabel('Time (s)')
for i=1:size(As,1)
    SpatialCells=(1:nb_columns*nb_rows);
    SpatialCells=SpatialCells(~ismember(SpatialCells,TemporalCellsc));
    subplot(nb_rows,nb_columns,[SpatialCells(i)]);
    topoplot((As(i,:)),localization,'plotrad',0.55);title(CompLABELS{i},'FontSize',FontSizec);
    set(gcf, 'PaperPosition', [0 0 40 60]);
    caxis([0 1]);
end

subplot(nb_rows,nb_columns,22:25);
text(0.5,0.5,title_str); axis off


hb=colorbar;
set(hb,'Units','normalized', 'position', [0.92 0.3 0.02 0.6],'FontSize',FontSizec);
set(gcf,'position',[0.5 0.1 0.45 0.8],'unit','normalized');    set(gcf,'position',[0.5 0.1 0.45 0.8],'unit','normalized')

colormap(gray);
colormap(flipud(colormap));
set(gcf,'color',[1 1 1],'paperposition',[0 0 35 25])


% print(gcf, figDIR,'-dpng','-r450')
end