function [B,C,info,options] = m_jadiag(C,options)

% This function has been re-coded for input/output format compatibility
% reasons.
% It is also to be able to have the same stopping criterion for all our
% algorithms (to be added).
%
% The original code can be found at : *add*
% All credit goes to Dinh-Tuan Pham.
% Please cite:
%
% *add*
%
% B such that B*C(:,:,k)*B' as much diagonal as possible for all k
%


% variables:
nChan = size(C,1);
nMat  = size(C,3);

% initialize parameters according to options input
%%% default

B0 = eye(nChan); % maybe we can put a better initialization as the one in the original function

opt = struct('B0',B0,'weights',ones(1,nMat),'tol',1e-6,'maxiter',100);
%%% get final options
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
%%% get parameters
B0      = options.B0;
w       = options.weights;
tol     = options.tol;
maxiter = options.maxiter;

% initialize B
B = B0;

% initialize some variables
C_tmp = zeros(nChan,nChan,nMat); % tmp transformed matrices

one = 1 + 10e-12;			% considered as equal to 1

% symmetrize input matrices and initialize C_tmp and diagC
for k=1:nMat
    C(:,:,k)     = .5*(C(:,:,k)+C(:,:,k)');
    C_tmp(:,:,k) = B*C(:,:,k)*B';
end

% initialize cost and dcost
cost  = 0;
for k=1:nMat
    cost = cost + w(k)*log(det(diag(diag(C_tmp(:,:,k))))/det(C_tmp(:,:,k)));
end
stopcrit = tol+1; % dumb initialization to enter loop

% initialize info struct
info = struct('it',0,'cost',cost,'stopcrit',nan,'B',B);

% Main Loop
it = 0;
while it<maxiter && stopcrit>tol
    it = it + 1;
    
    for i=2:nChan
        for j=1:i-1
            c1 = squeeze(C_tmp(i,i,:));
            c2 = squeeze(C_tmp(j,j,:));

            g12 = mean(w'.*squeeze(C_tmp(i,j,:))./c1)/sum(w)*nMat; % this is g_{ij}
            g21 = mean(w'.*squeeze(C_tmp(i,j,:))./c2)/sum(w)*nMat; % this is the conjugate of g_{ji}
            
            omega21 = mean(w'.*c1./c2)/sum(w)*nMat;
            omega12 = mean(w'.*c2./c1)/sum(w)*nMat;
            omega   = sqrt(omega12*omega21);
            
            tmp     = sqrt(omega21/omega12);
            tmp1    = (tmp*g12+g21)/(omega+1);
            omega   = max(omega,one);
            tmp2    = (tmp*g12-g21)/(omega-1);
            
            h12 = tmp1+tmp2;              % this is twice h_{ij}
            h21 = conj((tmp1-tmp2)/tmp);  % this is twice h_{ji}
            
            tmp = 1 + 0.5i*imag(h12*h21); % = 1 + (h12*h21 - conj(h12*h21))/4
            T = eye(2) - [0 h12; h21 0]/(tmp + sqrt(tmp^2 - h12*h21));
            % update target matrices
            for k=1:nMat
                C_tmp([i,j],:,k) = T*C_tmp([i,j],:,k);
                C_tmp(:,[i,j],k) = C_tmp(:,[i,j],k)*T';
            end
            % update B
            B([i,j],:) = T*B([i,j],:);
        end
    end
    
    % get cost, dcost
    cost_tmp = 0;
    for k=1:nMat
        cost_tmp = cost_tmp + w(k)*log(det(diag(diag(C_tmp(:,:,k))))/det(C_tmp(:,:,k)));
    end
    stopcrit = norm(info(it).B/B-eye(nChan),'fro')^2/nChan;
    cost     = cost_tmp;
    % fill info struct
    info(it+1) = struct('it',it,'cost',cost,'stopcrit',stopcrit,'B',B);

    % print some info
%     disp(['it: ' int2str(it) '     cost: ' num2str(cost) '     dcost: ' num2str(dcost)]);
end

% fill out C
C = C_tmp;