function [B,C,info,options] = RAJD_inv(C,options)

% get some dimensions
n = size(C,1); % matrices size
K = size(C,3); % number of matrices
% default options parameters
B0       = eye(n);
manifold = polarfactory(n);
ddiag    = @(x) diag(diag(x));
opt      = struct('B0',B0,'tol',1e-6,'maxiter',100,'weights',ones(1,K),'manifold',manifold,'optmethod',@conjugategradient,'GLfun2opt',@F_inv_ind,'diagmat',ddiag,'man_opt',[]);
% get options from input and default
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
% get man_opt from input and default
man_opt         = struct('verbosity',0);
man_opt         = getOptions(options.man_opt,man_opt);
man_opt.maxiter = options.maxiter;
options.man_opt = man_opt; % for output purposes
% initialize parameters
man       = options.manifold;
B0        = options.B0;
weights   = options.weights;
optmethod = options.optmethod;
GLfun2opt = options.GLfun2opt;
diagmat   = options.diagmat;
%
if isempty(GLfun2opt), GLfun2opt = @F_inv_ind;         end
if isempty(diagmat),   diagmat   = @(x) diag(diag(x)); end
%
tol       = options.tol;
maxiter   = options.maxiter;
verbosity = man_opt.verbosity;
%
problem   = [];
problem.M = man;



info = struct('stopcrit',nan,'A',eye(n),'B',B0);
%
B = B0;
man_opt.maxiter    = 1;
man_opt. verbosity = 0;

% main loop
it      = 0;
stopnow = false;
while ~stopnow
    % compute diagonal matrices
    M = zeros(n,n,K);
    D = zeros(n,n,K);
    for k=1:K
        M(:,:,k) = B*C(:,:,k)*B';
        D(:,:,k) = diagmat(M(:,:,k));
    end
    % initialize A
    A0 = eye(n);
    if isfield(man,'GL2man'), A0 = man.GL2man(A0); end
    % get fun2opt for this iteration
    [cost, egrad, ehess] = GLfun2opt(M,D,weights);
    if isfield(man,'fun2opt_GL2man')
        [cost,egrad,ehess] = man.fun2opt_GL2man(cost,egrad,ehess); 
    end
    % fill problem cost, grad and hess
    problem.cost  = cost;
    problem.egrad = egrad;
    problem.ehess = ehess;
    % optimization
    [A,~,info_tmp] = optmethod(problem,A0,man_opt);
    % update B
    if isfield(man,'man2GL'), A = man.man2GL(A); end
    B = A\B;
    % update some variables 
    it       = it+1;
    man_opt  = aux_mo(man_opt,info_tmp(end));
    stopcrit = aux_sc(A,info,info_tmp(end));
    stopnow  = (it>=maxiter || stopcrit < tol);
    info     = aux_info(info,info_tmp,cost(A0),stopcrit,A,B,it);
    
    if verbosity
        disp(['it: ' int2str(it-1) '       f: ' num2str(info(it).cost,'%10.6e') '      |grad|: ' num2str(info(it).gradnorm,'%10.6e') '      stop_crit: ' num2str(info(it).stopcrit,'%10.6e')]);
    end
end
% fill last info.cost field
M = zeros(n,n,K);
D = zeros(n,n,K);
for k=1:K
    M(:,:,k) = B*C(:,:,k)*B';
    D(:,:,k) = diagmat(M(:,:,k));
end
GLcost = GLfun2opt(M,D,weights);
info(it+1).cost = GLcost(eye(n));
if verbosity
    disp(['it: ' int2str(it) '       f: ' num2str(info(it+1).cost,'%10.6e') '      |grad|: ' num2str(info(it+1).gradnorm,'%10.6e') '      stop_crit: ' num2str(info(it+1).stopcrit,'%10.6e')]);
end

% get some outputs
C=M;

end

% auxiliary function: update man_opt struct
function man_opt = aux_mo(man_opt,info_manopt)
    if isfield(info_manopt,'Delta')
        man_opt.Delta0 = info_manopt.Delta;
    end
end
% auxiliary function to compute stopcrit
function stopcrit = aux_sc(A,info,info_manopt)
    if isfield(info_manopt,'accepted') && ~info_manopt.accepted
        stopcrit = info(end).stopcrit;
    else
        n        = size(A,1);
        stopcrit = norm(A-eye(n),'fro')^2/n;
    end
end
% auxiliary function: update info struct
function info = aux_info(info,info_manopt,cost_prev,stopcrit,A,B,it)
    if it==1 % first iterate, initialize info
        tmp          = info_manopt(1);
        tmp.stopcrit = info.stopcrit;
        tmp.A        = info.A;
        tmp.B        = info.B;
        info         = tmp;
    end
    % cost of previous iterate
    info(it).cost = cost_prev;
    % info on current iterate
    tmp          = info_manopt(end);
    tmp.iter     = it;
    tmp.stopcrit = stopcrit;
    tmp.A        = A;
    tmp.B        = B;
    info(it+1)   = tmp;
end