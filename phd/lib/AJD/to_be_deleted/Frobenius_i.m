function [GLcost, GLgrad, GLhess] = Frobenius_ind(B,C,D,w)
    GLcost = @(A) aux_cost(A,B,C,D,w);
    GLgrad = @(A) aux_grad(A,B,C,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,B,C,D,w);
end

function cost=aux_cost(A,B,C,D,w)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        cost = cost + w(k)*norm(B*C(:,:,k)*B'-A*D(:,:,k)*A','fro')^2;
    end
end

function grad=aux_grad(A,B,C,D,w)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        grad = grad + 4*w(k)*(A*D(:,:,k)*A'-B*C(:,:,k)*B')*A*D(:,:,k);
    end
end

function hess=aux_hess(A,Z,B,C,D,w)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        hess = hess + 4*w(k)*((Z*D(:,:,k)*A' + A*D(:,:,k)*Z')*A + (A*D(:,:,k)*A'-B*C(:,:,k)*B')*Z)*D(:,:,k);
    end
end