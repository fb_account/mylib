%% set of matrices to diagonalize
n   = 8;
K   = 32;
snr = 100;

A = randn(n);
while cond(A)>10
    A = randn(n);
end

d = randn(n,K).^2;


Cs = zeros(n,n,K);
N  = zeros(n,n,K);
for k=1:K
    tmp       = randn(n);
    N(:,:,k)  = tmp*tmp';
    N(:,:,k)  = N(:,:,k)/norm(N(:,:,k),'fro');
    Cs(:,:,k) = A*diag(d(:,k))*A';
    Cs(:,:,k) = Cs(:,:,k)/norm(Cs(:,:,k),'fro');
end
C = Cs + N/snr;

%% AJD

w       = ones(1,K);
tol     = 0;
maxiter = 100;

optStrategy = 'indirect';
GLfun2opt   = @Frobenius_f;
diagmat     = @(x) diag(diag(x));

manifold  = obliqueGLleftfactory(n);
optSolver = @trustregions;

B0 = eye(n);

opt = struct('weights',w,'B0',B0,'tol',tol,'maxiter',maxiter,'GLfun2opt',GLfun2opt,'manifold',manifold,'optStrategy',optStrategy,'diagmat',diagmat,'optSolver',optSolver,'man_opt',[]);

[B,C_out,info,options] = RAJD(C,opt);