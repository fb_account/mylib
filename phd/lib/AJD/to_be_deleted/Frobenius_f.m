function [GLcost, GLgrad, GLhess] = Frobenius_f(C,D,w)
    GLcost = @(B) aux_cost(B,C,D,w);
    GLgrad = @(B) aux_grad(B,C,D,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,D,w);
end

function cost=aux_cost(B,C,D,w)
    n = size(C,1);
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        tmp = diag(diag(D(:,:,k)).^-.5);
        cost = cost + w(k)*norm(tmp*B*C(:,:,k)*B'*tmp-eye(n),'fro')^2;
    end
end

function grad=aux_grad(B,C,D,w)
    n = size(C,1);
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        grad = grad + 4*w(k)*(D(:,:,k)\((B*C(:,:,k)*B')/D(:,:,k)-eye(n)))*B*C(:,:,k);
    end
end

function hess=aux_hess(B,Z,C,D,w)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        O  = B*C(:,:,k)*B';
        O  = O-diag(diag(O)); % must be a mistake here !
        dM = Z*C(:,:,k)*B' + B*C(:,:,k)*Z';
        hess = hess + 4*w(k)*(D(:,:,k)\((dM/D(:,:,k))*B - (O/D(:,:,k))*Z))*C(:,:,k);
    end
end