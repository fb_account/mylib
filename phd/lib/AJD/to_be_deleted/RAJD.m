function [B,C,info,options] = RAJD(C,options)

% [B,C,info,options] = RAJD(C,options)
%
% Performs the Riemannian approximate joint diagonalization (AJD) of the
% matrix set C using classical optimization approach.
% details to be added.
%
% author: Florent BOUCHARD, GIPSA-Lab, 2017

% handle input C
[C,n,K] = handle_input_C(C);
% handle input options
if nargin<2
    options = handle_input_options([],n,K);
else
    options = handle_input_options(options,n,K);
end
% perform optimization depending on strategy selected
switch options.optStrategy % no otherwise needed, dealt with it already
    case 'direct'
        [B,info] = optim_direct(C,options);
    case 'indirect'
        [B,info] = optim_indirect(C,options);
    case 'inv_indirect'
        [B,info] = optim_inv_indirect(C,options);
end

% compute final outputs
M = options.manifold;
if isfield(M,'man2GL'), B = M.man2GL(B); end % convert B if needed
for k=1:K
    C(:,:,k) = B*C(:,:,k)*B';
end

end

%% auxiliary functions

function [C,n,K] = handle_input_C(C)
    % get and check the dimensions of C
    if ndims(C)~=3, error('RAJD - C should be a set of K matrices of size n x n, i.e., a n x n x K array'); end
    [n,m,K] = size(C);
    if m~=n, error('RAJD - matrices in C need to be square'); end
    % check that matrices in C are real and symmetric
    if ~isreal(C), error('RAJD - matrices in C are not all real'); end
    for k=1:K
        if sum(sum((C(:,:,k)-C(:,:,k)')>eps))
            warning(['RAJD - C(:,:,' int2str(k) ') is not symmetric, taking its symmetrical part']);
            C(:,:,k) = .5*(C(:,:,k)+C(:,:,k)');
        end
    end
    % check that matrices in C are positive definite
    for k=1:K
        [~,p] = chol(C(:,:,k));
        if p~=0
            warning(['RAJD - C(:,:,' int2str(k) ') is not positive definite, this might be a problem depending on the criterion to be optimized']);
        end
    end
end

function options = handle_input_options(options,n,K)
    % function to check wether a variable is a function 
    % (perhaps should be put somewhere else) 
    isfun = @(f) isa(f, 'function_handle');
    % default parameters
    B0 = eye(n);
    manifold = GLrightfactory(n);
    opt = struct('weights',ones(1,K),'B0',B0,'tol',1e-6,'maxiter',100,'GLfun2opt',[],'manifold',manifold,'optStrategy','direct','diagmat',@(x) diag(diag(x)),'optSolver',@conjugategradient,'man_opt',[]);
    % get options from input and default
    options = gp_getOpt(options,opt);
    % check the compatibility of variables in options
    if ~isvector(options.weights) || length(options.weights)~=K
        warning('RAJD - options.weights does not have the good format, it should be a vector of length K, switching to default: ones(1,K)');
        options.weights = ones(1,K);
    end
    if ~ismatrix(options.B0) || any(size(options.B0)~=[n,n]) || rcond(options.B0)<eps
        warning('RAJD - options.B0 does not have the good format, it should be an invertible matrix of size n x n, switching to default: eye(n)');
        options.B0 = eye(n);
    end
    if ~strcmp(options.optStrategy,'direct') && ~strcmp(options.optStrategy,'indirect') && ~strcmp(options.optStrategy,'inv_indirect')
        warning('RAJD - options.optStrategy unrecognized, switching to default: ''direct''');
        options.optStrategy = 'direct';
    end
    if ~strcmp(options.optStrategy,'direct') && (isempty(options.diagmat)||~isfun(options.diagmat))
        warning('RAJD - chosen options.optStrategy needs a correct options.diagmat definition, switching to default: @(x) diag(diag(x))');
        options.diagmat = @(x) diag(diag(x));
    end
    if ~isfun(options.optSolver)
        warning('RAJD - options.optSolver should be a solver function from manopt or compatible with manopt, switching to default: @conjugategradient');
        options.optSolver = @conjugategradient;
    end
    % deal with default GLfun2opt depending on chosen optStrategy and check if
    % it is a function
    if ~isempty(options.GLfun2opt) && ~isfun(options.GLfun2opt)
        warning('RAJD - options.GLfun2opt should be a function, switching to default: Frobenius distance');
        options.GLfun2opt = [];
    end
    if isempty(options.GLfun2opt)
        switch options.optStrategy
            case 'direct'
                options.GLfun2opt = @Frobenius_c;
            case 'indirect'
                options.GLfun2opt = @Frobenius_f;
            case 'inv_indirect'
                options.GLfun2opt = @Frobenius_i;
        end
    end
    % get man_opt from input and default
    man_opt         = struct('verbosity',2,'statsfun',@statsfun,'stopfun',@stopfun,'tolgradnorm',1e-6);
    man_opt         = gp_getOpt(options.man_opt,man_opt);
    man_opt.maxiter = options.maxiter;  
    options.man_opt = man_opt;
end


function [B,info] = optim_direct(C,options)
    % extract variables from options struct
    w         = options.weights;
    M         = options.manifold;
    B0        = options.B0;
    if isfield(M,'GL2man'), B0 = M.GL2man(B0); end % convert B0 if needed
    tol       = options.tol;
%     maxiter   = options.maxiter; % already in man_opt
    fun2opt   = options.GLfun2opt;
%     optStrat  = options.optStrategy; % no longer needed at this point
    optSolver = options.optSolver;
    man_opt   = options.man_opt;
    
    % handle fun2opt and convert it if needed
    [cost,grad,hess] = fun2opt(C,w);
    if isfield(M,'convertFun2opt'), [cost,grad,hess] = M.convertFun2opt(cost,grad,hess); end
    
    % initialize problem struct
    problem = struct('M',M,'cost',cost,'egrad',grad,'ehess',hess,'tol',tol);
    
    % perform optimization
    [B,~,info] = optSolver(problem,B0,man_opt);
end


function [B,info] = optim_indirect(C,options)
    % extract variables from options struct
    w         = options.weights;
    M         = options.manifold;
    B0        = options.B0;
    if isfield(M,'GL2man'), B0 = M.GL2man(B0); end % convert B0 if needed
    tol       = options.tol;
    fun2opt   = options.GLfun2opt;
    optSolver = options.optSolver;
    diagmat   = options.diagmat;
    man_opt   = options.man_opt;
    % extract needed variables in man_opt
    maxiter   = man_opt.maxiter;
    verbosity = man_opt.verbosity;
    % modify man_opt 
    man_opt.maxiter   = 1;
    man_opt.verbosity = 0;
    
    % sizes
    [n,~,K] = size(C);
    
    % main loop
    it      = 0;
    stopnow = false;
    B       = B0;
    while ~stopnow
        % get target diagonal matrices
        if isfield(M,'man2GL')
            tmp = M.man2GL(B);
        else
            tmp = B;
        end
        D = zeros(n,n,K);
        for k=1:K
            D(:,:,k) = diagmat(tmp*C(:,:,k)*tmp');
        end
        % handle fun2opt and convert if needed
        [cost, grad, hess] = fun2opt(C,D,w);
        if isfield(M,'convertFun2opt'), [cost,grad,hess] = M.convertFun2opt(cost,grad,hess); end
        % initialize problem struct
        problem = struct('M',M,'cost',cost,'egrad',grad,'ehess',hess,'tol',tol);
        % perform one step of optimization
        [B,~,tmp] = optSolver(problem,B,man_opt);
        % update some variables 
        it = it+1;
        if isfield(tmp(end),'Delta')
            man_opt.Delta0 = tmp(end).Delta;
        end
        % deal with info struct
        if it==1 % first iterate, initialize info
            info = tmp;
        else
            tmp        = tmp(end);
            tmp.iter   = it;
            tmp.cost   = cost(B);
            tmp.x      = B;
            info(it+1) = tmp;
        end
        % stop criterion
        stopnow = man_opt.stopfun(problem,B,info,it); % check that last=it ok
        stopnow = stopnow || (it>maxiter);
        %
        if verbosity
%             it
%             info
            disp(['it: ' int2str(it-1) '       f: ' num2str(info(it+1).cost,'%10.6e') '      |grad|: ' num2str(info(it+1).gradnorm,'%10.6e')]);
        end
    end
end

function [B,info] = optim_inv_indirect(C,options)

error('optim_inv_indirect not implemented yet');

end







% function D = aux_diagmat(C,B,diagmat)
%     [n,~,K] = size(C);
%     D = zeros(n,n,K);
%     for k=1:K
%         D(:,:,k) = diagmat(B*C(:,:,k)*B');
%     end
% end






% check if a variable is a function
% isfun = @(f) isa(f, 'function_handle');

% function to get some data on optimization
function stats = statsfun(problem, x, stats)
    stats.x = x;
end
% stopping criterion function -> should be ok
function stopnow = stopfun(problem,x,info,last)
    M = problem.M;
    if isfield(M,'man2GL')
        tmp = M.man2GL(x);
        n   = size(tmp,1);
        if isfield(info,'accepted')
            stopnow = (last>=2 && info(last).accepted && norm(M.man2GL(info(last-1).x)/(tmp)-eye(n),'fro')^2/n < problem.tol);
        else
            stopnow = (last>=2 && norm(M.man2GL(info(last-1).x)/(tmp)-eye(n),'fro')^2/n < problem.tol);
        end
    else
        n = size(x,1);
        if isfield(info,'accepted')
            stopnow = (last>=2 && info(last).accepted && norm(info(last-1).x/(x)-eye(n),'fro')^2/n < problem.tol);
        else
            stopnow = (last>=2 && norm(info(last-1).x/(x)-eye(n),'fro')^2/n < problem.tol);
        end
    end
end