function [GLcost, GLgrad, GLhess] = Frobenius_c(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    K = size(C,3);
    off = @(x) x - diag(diag(x));
    cost = 0;
    parfor k=1:K
        cost = cost + w(k)*norm(off(B*C(:,:,k)*B'),'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    K = size(C,3);
    off = @(x) x - diag(diag(x));
    grad = 0;
    parfor k=1:K
        grad = grad + 4*w(k)*off(B*C(:,:,k)*B')*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w)
    K = size(C,3);
    off = @(x) x - diag(diag(x));
    hess = 0;
    parfor k=1:K
        hess = hess + 4*w(k)*(off(Z*C(:,:,k)*B' + B*C(:,:,k)*Z')*B + off(B*C(:,:,k)*B')*Z)*C(:,:,k);
    end
    hess = hess/K;
end