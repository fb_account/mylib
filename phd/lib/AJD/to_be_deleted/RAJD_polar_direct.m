function [B,C,info,options] = RAJD_polar_direct(C,options)

% get some dimensions
n = size(C,1); % matrices size
K = size(C,3); % number of matrices
% default options parameters
B0       = eye(n);
manifold = polarfactory(n);
opt      = struct('B0',B0,'tol',1e-6,'maxiter',100,'weights',ones(1,K),'manifold',manifold,'optmethod',@rlbfgs,'GLfun2opt',@lKL_direct,'man_opt',[]);
% get options from input and default
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
% get man_opt from input and default
man_opt         = struct('verbosity',2,'statsfun',@statsfun,'stopfun',@stopfun);
man_opt         = getOptions(options.man_opt,man_opt);
man_opt.maxiter = options.maxiter;
options.man_opt = man_opt; % for output purposes
% initialize parameters
man       = options.manifold;
if isfield(man,'projonman')
    B0    = man.projonman(GL2polar(options.B0));
else
    B0    = GL2polar(options.B0);
end
weights   = options.weights;
optmethod = options.optmethod;
GLfun2opt = options.GLfun2opt;
% deal with cost function, gradient and Hessian
[GLcost, GLgrad, GLhess] = GLfun2opt(C,weights);
[cost,egrad,ehess]       = fun2opt_GL2polar(GLcost,GLgrad,GLhess);
% initialize problem struct
problem        = [];
problem.M      = man;
problem.cost   = cost;
problem.egrad  = egrad;
problem.ehess  = ehess;
problem.tol    = options.tol;
% perform optimization
[B,~,info] = optmethod(problem,B0,man_opt);
% get some outputs
B = B.S*B.U;
for k=1:K
    C(:,:,k) = B*C(:,:,k)*B';
end

end

% function to get some data on optimization
function stats = statsfun(problem, x, stats)
    stats.x = x;
    stats.B = x.S*x.U;
end
% stopping criterion function
function stopnow = stopfun(problem,x,info,last)
    n = size(x.U,1);
    if isfield(info,'accepted')
        stopnow = (last>=2 && info(last).accepted && norm(info(last-1).B/(x.S*x.U)-eye(n),'fro')^2/n < problem.tol);
    else
        stopnow = (last>=2 && norm(info(last-1).B/(x.S*x.U)-eye(n),'fro')^2/n < problem.tol);
    end
end