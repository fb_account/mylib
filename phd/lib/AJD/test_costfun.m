n = 8;
K = 10;
% generate dumb SPD matrix set
C = zeros(n,n,K);
for k=1:K
    tmp      = randn(n);
    C(:,:,k) = tmp*tmp';
end
% define manifold to check
man = euclideanfactory(n,n);
% get cost function with grad and hess
[GLcost, GLgrad, GLhess] = KLl_direct(C,ones(1,K));
% [GLcost, GLgrad, GLhess] = alphaLD_ind(C,ones(1,K),@(C) closestdiag_alphaLD(C,0,[]),0);
% D = zeros(n,n,K);
% for k=1:K
%     D(:,:,k) = ddiag(C(:,:,k));
% end
% [GLcost, GLgrad, GLhess] = alphaLD_inv_ind(C,D,ones(1,K),0.25);
% define problem
problem.M    = man;
problem.cost = GLcost;
problem.grad = GLgrad;
problem.hess = GLhess;

checkgradient(problem);
checkhessian(problem);