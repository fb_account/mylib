function [B,C,info,options] = RAJD(C,options)

% get some dimensions
n = size(C,1); % matrices size
K = size(C,3); % number of matrices
% default options parameters
B0       = eye(n);
manifold = polarfactory(n);
opt      = struct('B0',B0,'tol',1e-6,'maxiter',100,'weights',ones(1,K),'manifold',manifold,'optmethod',@rlbfgs,'optStrategy','direct','GLfun2opt',[],'diagmat',[],'man_opt',[]);
% get options from input and default
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
% get man_opt from input and default
man_opt         = struct('verbosity',0,'statsfun',@statsfun,'stopfun',@stopfun);
man_opt         = getOptions(options.man_opt,man_opt);
man_opt.maxiter = options.maxiter;
options.man_opt = man_opt; % for output purposes
% initialize parameters
man       = options.manifold;
if isfield(man,'GL2man')
    B0    = man.GL2man(options.B0); % ### ok
else
    B0    = options.B0;             % ### ok
end
weights   = options.weights;
optmethod = options.optmethod;
GLfun2opt = options.GLfun2opt;
diagmat   = options.diagmat;
% deal with cost function, gradient and Hessian
switch options.optStrategy
    case 'direct'
        if isempty(GLfun2opt), GLfun2opt = @KLl_direct;        end
        [cost, egrad, ehess]             = GLfun2opt(C,weights);
    case 'indirect'
        if isempty(GLfun2opt), GLfun2opt = @F_ind;             end
        if isempty(diagmat),   diagmat   = @(x) diag(diag(x)); end
        [cost, egrad, ehess]             = GLfun2opt(C,weights,diagmat);
    otherwise
        error('RAJD: unrecognized optStrategy');
end
if isfield(man,'fun2opt_GL2man')
    [cost,egrad,ehess] = man.fun2opt_GL2man(cost,egrad,ehess); % ### ok 
end
% initialize problem struct
problem        = [];
problem.M      = man;
problem.cost   = cost;
problem.egrad  = egrad;
problem.ehess  = ehess;
problem.tol    = options.tol;
% perform optimization
[B,~,info] = optmethod(problem,B0,man_opt);
% get some outputs
if isfield(man,'man2GL')
    B = man.man2GL(B);            % ### ok
end
for k=1:K
    C(:,:,k) = B*C(:,:,k)*B';
end

end

% function to get some data on optimization
function stats = statsfun(problem, x, stats)
    stats.x = x;
    if isfield(problem.M,'man2GL')
        stats.B = problem.M.man2GL(x); % ### ok
    else
        stats.B = x;
    end
end
% stopping criterion function
function stopnow = stopfun(problem,x,info,last)
    if isfield(problem.M,'man2GL')
        B = problem.M.man2GL(x);
    else
        B = x;
    end
    n = size(B,1); % ### ok
    if isfield(info,'accepted')
        stopnow = (last>=2 && info(last).accepted && norm(info(last-1).B\B-eye(n),'fro')^2/n < problem.tol); % ### ok
    else
        stopnow = (last>=2 && norm(info(last-1).B\B-eye(n),'fro')^2/n < problem.tol); % ### ok
    end
end