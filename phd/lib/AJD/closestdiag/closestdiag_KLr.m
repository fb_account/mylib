function D = closestdiag_KLr(C)


D = diag(diag(inv(C)).^-1);