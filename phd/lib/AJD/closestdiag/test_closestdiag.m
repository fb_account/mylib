n = 8;
% generate random SPD matrix
C = randn(n);
C = C'*C;
% options for optimization
D0  = ddiag(C);
opt = struct('D0',D0,'tol',1e-9,'maxiter',50,'optmethod',@trustregions,'man_opt',[]);
% % closest diag according to riemannian distance
% D   = closestdiag_R(C,opt);
% disp(['R       : ' num2str(sum(diag(logm(diag(diag(D).^-1)*C)).^2)/n)])
% closest diag according to alpha LD divergence
alpha = 0;
D     = closestdiag_alphaLD(C,alpha,opt);
disp(['alphaLD : ' num2str(sum((diag(inv(.5*(1-alpha)*C + .5*(1+alpha)*D)) - diag(D).^-1).^2)/n)])
% % closest diag according to Wasserstein distance
% D   = closestdiag_W(C,opt);
% disp(['W       : ' num2str(sum((diag(sqrtm(diag(diag(D).^.5)*C*diag(diag(D).^.5))) - diag(D)).^2)/n)])