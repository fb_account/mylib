function [D,info,options] = closestdiag_alphaLD(C,alpha,options)


n   = size(C,1); % matrix size
man = diagposdeffactory(n);

% default options parameters
D0  = (1+alpha)/2*ddiag(C) + (1-alpha)/2*diag(1./diag(inv(C)));
opt = struct('D0',D0,'tol',1e-6,'maxiter',15,'optmethod',@conjugategradient,'man_opt',[]);
% get options from input and default
if nargin<3
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
% get man_opt from input and default
man_opt         = struct('verbosity',0,'statsfun',@statsfun,'stopfun',@stopfun);
man_opt         = getOptions(options.man_opt,man_opt);
man_opt.maxiter = options.maxiter;
options.man_opt = man_opt; % for output purposes
% initialize parameters
D0              = options.D0;
optmethod       = options.optmethod;
% initialize problem
problem.M       = man;
problem.tol     = options.tol;
problem.cost    = @(D) aux_cost(D,C,alpha);
problem.grad    = @(D) aux_grad(D,C,alpha);
problem.hess    = @(D,Z) aux_hess(D,Z,C,alpha);

% checkgradient(problem);
% figure;
% checkhessian(problem);

% perform optimization
[D,~,info] = optmethod(problem,D0,man_opt);

end

function cost = aux_cost(D,C,alpha)
    Q    = (1-alpha)/2*C + (1+alpha)/2*D;
    tmp1 = det(Q);
    tmp2 = det(C)^((1-alpha)/2)*prod(diag(D))^((1+alpha)/2);
    cost = 4/(1-alpha^2)*log(tmp1/tmp2);
end

function rgrad = aux_grad(D,C,alpha)
    n     = size(C,1);
    sig   = diag(diag(inv((1-alpha)/2*C+(1+alpha)/2*D)));
    rgrad = 2/(1-alpha)*D*(sig*D-eye(n));
end

function rhess = aux_hess(D,Z,C,alpha)
    Q     = (1-alpha)/2*C+(1+alpha)/2*D;
    sig   = diag(diag(inv(Q)));
    dsig  = -(1+alpha)/2*diag(diag(Q\Z/Q));
    rhess = 2/(1-alpha)*D*(sig*Z + dsig*D);
end


% function to get some data on optimization
function stats = statsfun(problem, x, stats)
    stats.x = x;
end
% stopping criterion function
function stopnow = stopfun(problem,x,info,last)
    if isfield(info,'accepted')
        stopnow = (last>=2 && info(last).accepted && sum((log(diag(info(last-1).x)./diag(x))).^2)/size(x,1) < problem.tol);
    else
        stopnow = (last>=2 && sum((log(diag(info(last-1).x)./diag(x))).^2)/size(x,1) < problem.tol);
    end
end