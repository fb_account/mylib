function D = closestdiag_KLs(C)

D = diag((diag(C).^.5) .* (diag(inv(C)).^-.5));