function D = ddiag(C)

D = diag(diag(C));