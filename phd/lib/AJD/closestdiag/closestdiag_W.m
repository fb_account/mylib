function [D,info,options] = closestdiag_W(C,options)


n   = size(C,1); % matrix size
man = diagposdeffactory(n);

% default options parameters
D0  = ddiag(C);
opt = struct('D0',D0,'tol',1e-6,'maxiter',15,'optmethod',@conjugategradient,'man_opt',[]);
% get options from input and default
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
% get man_opt from input and default
man_opt         = struct('verbosity',0,'statsfun',@statsfun,'stopfun',@stopfun);
man_opt         = getOptions(options.man_opt,man_opt);
man_opt.maxiter = options.maxiter;
options.man_opt = man_opt; % for output purposes
% initialize parameters
D0              = options.D0;
optmethod       = options.optmethod;
% initialize problem
problem.M       = man;
problem.tol     = options.tol;
problem.cost    = @(D) aux_cost(D,C);
problem.grad    = @(D) aux_grad(D,C);
problem.hess    = @(D,Z) aux_hess(D,Z,C);

% checkgradient(problem);
% figure;
% checkhessian(problem);


[D,~,info] = optmethod(problem,D0,man_opt);


end

function cost = aux_cost(D,C)
    tmp  = diag(diag(D).^.5);
    Q    = sqrtm(tmp*C*tmp);
    cost = trace(.5*(C+D)-Q);
end

function rgrad = aux_grad(D,C)
    tmp   = diag(diag(D).^.5);
    Q     = sqrtm(tmp*C*tmp);
    rgrad = .5*D*(D-diag(diag(Q)));
end

function rhess = aux_hess(D,Z,C)
    tmp1  = diag(D).^.5;
    Q     = sqrtm(diag(tmp1)*C*diag(tmp1));
    tmp2  = Z*diag(tmp1.^-1)*C*diag(tmp1);
    tmp2  = .5*(tmp2+tmp2');
    dQ    = lyap(Q,-tmp2);
    rhess = .5*D*(Z - diag(diag(dQ)));
end


% function to get some data on optimization
function stats = statsfun(problem, x, stats)
    stats.x = x;
end
% stopping criterion function
function stopnow = stopfun(problem,x,info,last)
    if isfield(info,'accepted')
        stopnow = (last>=2 && info(last).accepted && sum((log(diag(info(last-1).x)./diag(x))).^2)/size(x,1) < problem.tol);
    else
        stopnow = (last>=2 && sum((log(diag(info(last-1).x)./diag(x))).^2)/size(x,1) < problem.tol);
    end
end