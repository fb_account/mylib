function [D,info,options] = closestdiag_R(C,options)


n   = size(C,1); % matrix size
man = diagposdeffactory(n);

% default options parameters
D0  = ddiag(C);
opt = struct('D0',D0,'tol',1e-6,'maxiter',15,'optmethod',@conjugategradient,'man_opt',[]);
% get options from input and default
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
% get man_opt from input and default
man_opt         = struct('verbosity',0,'statsfun',@statsfun,'stopfun',@stopfun);
man_opt         = getOptions(options.man_opt,man_opt);
man_opt.maxiter = options.maxiter;
options.man_opt = man_opt; % for output purposes
% initialize parameters
D0              = options.D0;
optmethod       = options.optmethod;
% initialize problem
problem.M       = man;
problem.tol     = options.tol;
problem.cost    = @(D) aux_cost(D,C);
problem.grad    = @(D) aux_grad(D,C);
problem.hess    = @(D,Z) aux_hess(D,Z,C);

% checkgradient(problem);
% figure;
% checkhessian(problem);

% perform optimization
[D,~,info] = optmethod(problem,D0,man_opt);

end

function cost = aux_cost(D,C)
    tmp  = eig(diag(diag(D).^-.5)*C*diag(diag(D).^-.5));
    cost = sum(log(tmp).^2);
end

function rgrad = aux_grad(D,C)
    rgrad = -2*D*diag(diag(logm(diag(diag(D).^-1)*C)));
end

function rhess = aux_hess(D,Z,C)
    dL    = logm_frechet(C\D,C\Z); 
    rhess = 2*D*diag(diag(dL));
end


% function to get some data on optimization
function stats = statsfun(problem, x, stats)
    stats.x = x;
end
% stopping criterion function
function stopnow = stopfun(problem,x,info,last)
    if isfield(info,'accepted')
        stopnow = (last>=2 && info(last).accepted && sum((log(diag(info(last-1).x)./diag(x))).^2)/size(x,1) < problem.tol);
    else
        stopnow = (last>=2 && sum((log(diag(info(last-1).x)./diag(x))).^2)/size(x,1) < problem.tol);
    end
end