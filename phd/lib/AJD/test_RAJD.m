%% set of matrices to diagonalize
n   = 8;
K   = 32;
snr = 1000;

A = randn(n);
while cond(A)>10
    A = randn(n);
end

d = randn(n,K).^2;


Cs = zeros(n,n,K);
N  = zeros(n,n,K);
for k=1:K
    tmp       = randn(n);
    N(:,:,k)  = tmp*tmp';
    N(:,:,k)  = N(:,:,k)/norm(N(:,:,k),'fro');
    Cs(:,:,k) = A*diag(d(:,k))*A';
    Cs(:,:,k) = Cs(:,:,k)/norm(Cs(:,:,k),'fro');
end
C = Cs + N/snr;



%%
B0 = eye(n);
manifold = obliquepolarfactory(n);
GLfun2opt = @F_direct;

opt = struct('B0',B0,'tol',1e-6,'maxiter',100,'weights',ones(1,K),'manifold',manifold,'optmethod',@trustregions,'optStrategy','direct','GLfun2opt',GLfun2opt,'man_opt',[]);

[B,C_out,info,options] = RAJD_polar(C,opt);



%%

B0       = eye(n);
manifold = intrinsicpolarfactory(n,C);
ddiag    = @(x) diag(diag(x));
% GLfun2opt = @(C,w) alphaLD_direct(C,w,0.75);
GLfun2opt = @R_ind;
opt      = struct('B0',B0,'tol',1e-6,'maxiter',100,'weights',ones(1,K),'manifold',manifold,'optmethod',@trustregions,'optStrategy','indirect','GLfun2opt',GLfun2opt,'diagmat',ddiag,'man_opt',[]);

[B,C_out,info,options] = RAJD(C,opt);


%%

B0       = eye(n);
manifold = nonholonomicGLleftfactory(n);
ddiag    = @(x) diag(diag(x));
opt      = struct('B0',B0,'tol',1e-6,'maxiter',100,'weights',ones(1,K),'manifold',manifold,'optmethod',@rlbfgs,'optStrategy','direct','GLfun2opt',@lKL_direct,'diagmat',ddiag,'man_opt',[]);

[B,C_out,info,options] = RAJD(C,opt);

%%

B0       = eye(n);
manifold = GLleftfactory(n);
ddiag    = @(x) diag(diag(inv(x)).^-1);
opt      = struct('B0',B0,'tol',1e-6,'maxiter',100,'weights',ones(1,K),'manifold',manifold,'optmethod',@trustregions,'optStrategy','inv_ind','GLfun2opt',@rKL_inv_ind,'diagmat',ddiag,'man_opt',[]);

[B,C_out,info,options] = RAJD_inv(C,opt);

