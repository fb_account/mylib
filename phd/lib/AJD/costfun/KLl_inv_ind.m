function [GLcost, GLgrad, GLhess] = KLl_inv_ind(M,D,w)
    GLcost = @(A) aux_cost(A,M,D,w);
    GLgrad = @(A) aux_grad(A,M,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w);
end

function cost=aux_cost(A,M,D,w)
    n = size(M,1);
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        tmp  = M(:,:,k)/(A*D(:,:,k)*A');
        cost = cost + w(k)*(trace(tmp)-n - log(det(tmp)));
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w)
    n = size(M,1);
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        grad = grad + 2*w(k)*(eye(n) - N\M(:,:,k))/A';
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w)
    n = size(M,1);
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        dN   = A*D(:,:,k)*Z' + Z*D(:,:,k)*A';
        hess = hess + 2*w(k)*(N\((M(:,:,k)-N)*(A'\Z') + (dN/N)*M(:,:,k)))/A'; % it seems that the order of the parentheses is numerically important !!
    end
    hess = hess/K;
end