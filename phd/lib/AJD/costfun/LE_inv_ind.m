function [GLcost, GLgrad, GLhess] = LE_inv_ind(M,D,w)
    GLcost = @(A) aux_cost(A,M,D,w);
    GLgrad = @(A) aux_grad(A,M,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w);
end

function cost=aux_cost(A,M,D,w)
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        cost = cost + w(k)*norm(logm(M(:,:,k)) - logm(N),'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w)
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        N = A*D(:,:,k)*A';
        O = logm(N) - logm(M(:,:,k));
        L = logm_frechet_real(N,O);
        grad = grad + 4*w(k)*L*A*D(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w)
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        N        = A*D(:,:,k)*A';
        dN       = Z*D(:,:,k)*A' + A*D(:,:,k)*Z';
        O        = logm(N) - logm(M(:,:,k));
        dO       = logm_frechet_real(N,dN);
        [~,L,dL] = logm_frechet_real_2(N,O,dN,dO);
        hess     = hess + 4*w(k)*(dL*A+L*Z)*D(:,:,k);
    end
    hess = hess/K;
end