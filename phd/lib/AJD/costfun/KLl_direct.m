function [GLcost, GLgrad, GLhess] = KLl_direct(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M = B*C(:,:,k)*B';
        cost = cost + w(k)*(log(prod(diag(M)))-log(det(M)));
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        grad = grad + 2*w(k)*(diag(diag(B*C(:,:,k)*B').^-1)*B*C(:,:,k)-inv(B'));
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        d  = diag(B*C(:,:,k)*B');
        dd = diag(Z*C(:,:,k)*B' + B*C(:,:,k)*Z');
        hess = hess + 2*w(k)*(diag(d.^-1)*(Z - diag((d.^-1).*dd)*B)*C(:,:,k) + B'\Z'/B');
    end
    hess = hess/K;
end