function [GLcost, GLgrad, GLhess] = R_ind(C,w,diagmat)
    GLcost = @(B) aux_cost(B,C,w,diagmat);
    GLgrad = @(B) aux_grad(B,C,w,diagmat);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat);
end

function cost=aux_cost(B,C,w,diagmat)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        N    = diag(diag(D).^-.5);
        cost = cost + w(k)*norm(logm(N*M*N),'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        grad = grad + 4*w(k)*logm(D\M)/B';
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        dM   = Z*C(:,:,k)*B' + B*C(:,:,k)*Z';
        D    = diagmat(M);
        dL   = logm_frechet_real(D\M,D\dM);
        hess = hess + 4*w(k)*(dL - logm(D\M)*(B'\Z'))/B';
    end
    hess = hess/K;
end