function [GLcost, GLgrad, GLhess] = alphaLD_inv_ind(M,D,w,alpha)
    GLcost = @(A) aux_cost(A,M,D,w,alpha);
    GLgrad = @(A) aux_grad(A,M,D,w,alpha);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w,alpha);
end

function cost=aux_cost(A,M,D,w,alpha)
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        Q    = (1-alpha)/2*M(:,:,k) + (1+alpha)/2*N;
        tmp1 = det(Q);
        tmp2 = det(M(:,:,k))^((1-alpha)/2)*det(N)^((1+alpha)/2);
        cost = cost + w(k)*4/(1-alpha^2)*log(tmp1/tmp2);
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w,alpha)
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        Q    = (1-alpha)/2*M(:,:,k) + (1+alpha)/2*N;
        grad = grad + w(k)*4/(1-alpha)*(inv(Q) - inv(N))*A*D(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w,alpha)
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        Q    = (1-alpha)/2*M(:,:,k) + (1+alpha)/2*N;
        dQ   = (1+alpha)/2*(A*D(:,:,k)*Z' + Z*D(:,:,k)*A');
        hess = hess + w(k)*4/(1-alpha)*(Q\(Z - (dQ/Q)*A)*D(:,:,k) + A'\Z'/A');
    end
    hess = hess/K;
end