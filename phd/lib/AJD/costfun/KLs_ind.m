function [GLcost, GLgrad, GLhess] = KLs_ind(C,w,diagmat)
    GLcost = @(B) aux_cost(B,C,w,diagmat);
    GLgrad = @(B) aux_grad(B,C,w,diagmat);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat);
end

function cost=aux_cost(B,C,w,diagmat)
    n = size(C,1);
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        cost = cost + w(k)*(.5*trace(M\D + diag(diag(D).^-1)*M)-n);
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        grad = grad + w(k)*(diag(diag(D).^-1)*M - M\D)/B';
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        dM   = Z*C(:,:,k)*B' + B*C(:,:,k)*Z';
        D    = diagmat(M);
        hess = hess + w(k)*(diag(diag(D).^-1)*Z*C(:,:,k) + M\((dM/M)*D + D*(B'\Z'))/B');
    end
    hess = hess/K;
end