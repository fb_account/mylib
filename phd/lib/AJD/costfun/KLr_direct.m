function [GLcost, GLgrad, GLhess] = KLr_direct(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    n = size(C,1);
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diag(diag(inv(M)).^-1);
        tmp  = D/M;
        cost = cost + w(k)*(trace(tmp)-n - log(det(tmp)));
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    n = size(C,1);
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diag(diag(inv(M)).^-1);
        grad = grad + 2*w(k)*(eye(n) - M\D)/B';
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        dM   = B*C(:,:,k)*Z' + Z*C(:,:,k)*B';
        M_   = inv(M);
        D    = diag(diag(M_).^-1);
        dD   = diag((diag(D).^2).*diag(M_*dM*M_));
        hess = hess + 2*w(k)*M_*((D-M)*(B'\Z') + dM*M_*D - dD)/B';
    end
    hess = hess/K;
end