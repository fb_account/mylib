function [GLcost, GLgrad, GLhess] = W_inv_ind(M,D,w)
    GLcost = @(A) aux_cost(A,M,D,w);
    GLgrad = @(A) aux_grad(A,M,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w);
end

function cost=aux_cost(A,M,D,w)
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        tmp  = sqrtm(M(:,:,k));
        Q_   = sqrtm(tmp*N*tmp);
        cost = cost + w(k)*trace((M(:,:,k)+N)/2 - Q_);
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w)
    n = size(M,1);
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        tmp  = sqrtm(M(:,:,k));
        Q    = sqrtm(inv(tmp*N*tmp));
        grad = grad + w(k)*(eye(n) - tmp*Q*tmp)*A*D(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w)
    n = size(M,1);
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        dN   = Z*D(:,:,k)*A' + A*D(:,:,k)*Z';
        tmp  = sqrtm(M(:,:,k));
        Q    = sqrtm(inv(tmp*N*tmp));
        dQ   = lyap(Q,(N*tmp)\dN/(tmp*N));
        hess = hess + w(k)*((eye(n) - tmp*Q*tmp)*Z - tmp*dQ*tmp*A)*D(:,:,k);
    end
    hess = hess/K;
end