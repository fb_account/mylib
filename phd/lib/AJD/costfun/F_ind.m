function [GLcost, GLgrad, GLhess] = F_ind(C,w,diagmat)
    GLcost = @(B) aux_cost(B,C,w,diagmat);
    GLgrad = @(B) aux_grad(B,C,w,diagmat);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat);
end

function cost=aux_cost(B,C,w,diagmat)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M = B*C(:,:,k)*B';
        D = diagmat(M);
        cost = cost + w(k)*norm(M-D,'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M = B*C(:,:,k)*B';
        D = diagmat(M);
        grad = grad + 4*w(k)*(M-D)*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M = B*C(:,:,k)*B';
        D = diagmat(M);
        dM = Z*C(:,:,k)*B' + B*C(:,:,k)*Z';
        hess = hess + 4*w(k)*(dM*B + (M - D)*Z)*C(:,:,k)
    end
    hess = hess/K;
end