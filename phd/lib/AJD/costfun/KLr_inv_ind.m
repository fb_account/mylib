function [GLcost, GLgrad, GLhess] = KLr_inv_ind(M,D,w)
    GLcost = @(A) aux_cost(A,M,D,w);
    GLgrad = @(A) aux_grad(A,M,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w);
end

function cost=aux_cost(A,M,D,w)
    n = size(M,1);
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        tmp  = (A*D(:,:,k)*A')/M(:,:,k);
        cost = cost + w(k)*(trace(tmp)-n - log(det(tmp)));
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w)
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        grad = grad + 2*w(k)*(M(:,:,k)\(A*D(:,:,k)) - inv(A'));
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w)
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        hess = hess + 2*w(k)*(M(:,:,k)\(Z*D(:,:,k)) + A'\Z'/A');
    end
    hess = hess/K;
end