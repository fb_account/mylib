function [GLcost, GLgrad, GLhess] = alphaLD_direct(C,w,alpha)
    GLcost = @(B) aux_cost(B,C,w,alpha);
    GLgrad = @(B) aux_grad(B,C,w,alpha);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,alpha);
end

function cost=aux_cost(B,C,w,alpha)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = closestdiag_alphaLD(M,alpha);
        Q    = (1-alpha)/2*M + (1+alpha)/2*D;
        tmp1 = det(Q);
        tmp2 = det(M)^((1-alpha)/2)*prod(diag(D))^((1+alpha)/2);
        cost = cost + w(k)*4/(1-alpha^2)*log(tmp1/tmp2);
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,alpha)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = closestdiag_alphaLD(M,alpha);
        Q    = (1-alpha)/2*M + (1+alpha)/2*D;
        grad = grad + w(k)*4/(1+alpha)*(inv(Q) - inv(M))*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,alpha)
    hess = [];
    error('alphaLD_direct: hessian not implemented yet, please use first order algorithm');
end