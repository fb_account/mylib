function [GLcost, GLgrad, GLhess] = KLs_inv_ind(M,D,w)
    GLcost = @(A) aux_cost(A,M,D,w);
    GLgrad = @(A) aux_grad(A,M,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w);
end

function cost=aux_cost(A,M,D,w)
    n = size(M,1);
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        cost = cost + w(k)*(.5*trace(M(:,:,k)\N +N\M(:,:,k))-n);
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w)
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        grad = grad + w(k)*(M(:,:,k)\N - N\M(:,:,k))/A';
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w)
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        dN   = A*D(:,:,k)*Z' + Z*D(:,:,k)*A';
        hess = hess + w(k)*(M(:,:,k)\(Z*D(:,:,k)) + N\(M(:,:,k)*(A'\Z') + (dN/N)*M(:,:,k))/A');
    end
    hess = hess/K;
end