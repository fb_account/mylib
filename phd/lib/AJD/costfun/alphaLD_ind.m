function [GLcost, GLgrad, GLhess] = alphaLD_ind(C,w,diagmat,alpha)
    GLcost = @(B) aux_cost(B,C,w,diagmat,alpha);
    GLgrad = @(B) aux_grad(B,C,w,diagmat,alpha);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat,alpha);
end

function cost=aux_cost(B,C,w,diagmat,alpha)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        Q    = (1-alpha)/2*M + (1+alpha)/2*D;
        tmp1 = det(Q);
        tmp2 = det(M)^((1-alpha)/2)*prod(diag(D))^((1+alpha)/2);
        cost = cost + w(k)*4/(1-alpha^2)*log(tmp1/tmp2);
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat,alpha)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        Q    = (1-alpha)/2*M + (1+alpha)/2*D;
        grad = grad + w(k)*4/(1+alpha)*(inv(Q) - inv(M))*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat,alpha)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        Q    = (1-alpha)/2*M + (1+alpha)/2*D;
        dQ   = (1-alpha)/2*(B*C(:,:,k)*Z' + Z*C(:,:,k)*B');
        hess = hess + w(k)*4/(1+alpha)*(Q\(Z - (dQ/Q)*B)*C(:,:,k) + B'\Z'/B');
    end
    hess = hess/K;
end