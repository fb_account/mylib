function [GLcost, GLgrad, GLhess] = KLs_direct(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    n = size(C,1);
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diag((diag(M).^.5).*(diag(inv(M)).^-.5));
        cost = cost + w(k)*(.5*trace(M\D + diag(diag(D).^-1)*M)-n);
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        d    = (diag(M).^.5).*(diag(inv(M)).^-.5);
        grad = grad + w(k)*(diag(1./d)*M - M\diag(d))/B';
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        dM   = B*C(:,:,k)*Z' + Z*C(:,:,k)*B';
        M_   = inv(M);
        d    = (diag(M).^.5).*(diag(M_).^-.5);
        dd   = .5*d.*((1./diag(M)).*diag(dM) + (1./diag(M_)).*diag(M_*dM*M_));
        hess = hess + w(k)*(diag(1./d)*(Z - diag(dd./d)*B)*C(:,:,k) + M_*(diag(d)*(B'\Z') + dM*M_*diag(d) - diag(dd))/B');
    end
    hess = hess/K;
end