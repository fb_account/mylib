function [GLcost, GLgrad, GLhess] = R_inv_ind(M,D,w)
    GLcost = @(A) aux_cost(A,M,D,w);
    GLgrad = @(A) aux_grad(A,M,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w);
end

function cost=aux_cost(A,M,D,w)
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        N    = sqrtm(A*D(:,:,k)*A');
        cost = cost + w(k)*norm(logm(N\M(:,:,k)/N),'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w)
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        grad = grad + 4*w(k)*logm(M(:,:,k)\N)/A';
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w)
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        N    = A*D(:,:,k)*A';
        dN   = A*D(:,:,k)*Z' + Z*D(:,:,k)*A';
        dL   = logm_frechet_real(M(:,:,k)\N,M(:,:,k)\dN);
        hess = hess + 4*w(k)*(dL - logm(M(:,:,k)\N)*(A'\Z'))/A';
    end
    hess = hess/K;
end