function [GLcost, GLgrad, GLhess] = LE_direct(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        tmp  = logm(M);
        cost = cost + w(k)*norm(tmp - diag(diag(tmp)),'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        O    = logm(M);
        O    = O - diag(diag(O));
        L    = logm_frechet_real(M,O);
        grad = grad + 4*w(k)*L*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M        = B*C(:,:,k)*B';
        dM       = Z*C(:,:,k)*B' + B*C(:,:,k)*Z';
        O        = logm(M);
        O        = O - diag(diag(O));
        dO       = logm_frechet_real(M,dM);
        dO       = dO - diag(diag(dO));
        [~,L,dL] = logm_frechet_real_2(M,O,dM,dO);
        hess     = hess + 4*w(k)*(dL*B+L*Z)*C(:,:,k);
    end
    hess = hess/K;
end