function [GLcost, GLgrad, GLhess] = LE_ind(C,w,diagmat)
    GLcost = @(B) aux_cost(B,C,w,diagmat);
    GLgrad = @(B) aux_grad(B,C,w,diagmat);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat);
end

function cost=aux_cost(B,C,w,diagmat)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        cost = cost + w(k)*norm(logm(M) - logm(D),'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        O    = logm(M) - diag(log(diag(D)));
        L    = logm_frechet_real(M,O);
        grad = grad + 4*w(k)*L*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M        = B*C(:,:,k)*B';
        D        = diagmat(M);
        dM       = Z*C(:,:,k)*B' + B*C(:,:,k)*Z';
        O        = logm(M) - diag(log(diag(D)));
        dO       = logm_frechet_real(M,dM);
        [~,L,dL] = logm_frechet_real_2(M,O,dM,dO);
        hess     = hess + 4*w(k)*(dL*B+L*Z)*C(:,:,k);
    end
    hess = hess/K;
end