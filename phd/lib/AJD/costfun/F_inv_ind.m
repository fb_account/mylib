function [GLcost, GLgrad, GLhess] = F_inv_ind(M,D,w)
    GLcost = @(A) aux_cost(A,M,D,w);
    GLgrad = @(A) aux_grad(A,M,D,w);
    GLhess = @(A,Z) aux_hess(A,Z,M,D,w);
end

function cost=aux_cost(A,M,D,w)
    K = size(M,3);
    cost = 0;
    parfor k=1:K
        cost = cost + w(k)*norm(M(:,:,k)-A*D(:,:,k)*A','fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(A,M,D,w)
    K = size(M,3);
    grad = 0;
    parfor k=1:K
        grad = grad + 4*w(k)*(A*D(:,:,k)*A'-M(:,:,k))*A*D(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(A,Z,M,D,w)
    K = size(M,3);
    hess = 0;
    parfor k=1:K
        hess = hess + 4*w(k)*((Z*D(:,:,k)*A' + A*D(:,:,k)*Z')*A + (A*D(:,:,k)*A'-M(:,:,k))*Z)*D(:,:,k);
    end
    hess = hess/K;
end