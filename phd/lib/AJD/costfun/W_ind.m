function [GLcost, GLgrad, GLhess] = W_ind(C,w,diagmat)
    GLcost = @(B) aux_cost(B,C,w,diagmat);
    GLgrad = @(B) aux_grad(B,C,w,diagmat);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat);
end

function cost=aux_cost(B,C,w,diagmat)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        Sig  = diag(diag(D).^.5);
        Q_   = sqrtm(Sig*M*Sig);
        cost = cost + w(k)*trace((M+D)/2 - Q_);
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat)
    n = size(C,1);
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        Sig  = diag(diag(D).^.5);
        Q    = sqrtm(inv(Sig*M*Sig));
        grad = grad + w(k)*(eye(n) - Sig*Q*Sig)*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat)
    n = size(C,1);
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        dM   = Z*C(:,:,k)*B' + B*C(:,:,k)*Z';
        D    = diagmat(M);
        Sig  = diag(diag(D).^.5);
        Q    = sqrtm(inv(Sig*M*Sig));
        dQ   = lyap(Q,(M*Sig)\dM/(Sig*M));
        hess = hess + w(k)*((eye(n) - Sig*Q*Sig)*Z - Sig*dQ*Sig*B)*C(:,:,k);
    end
    hess = hess/K;
end