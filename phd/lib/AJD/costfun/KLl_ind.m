function [GLcost, GLgrad, GLhess] = KLl_ind(C,w,diagmat)
    GLcost = @(B) aux_cost(B,C,w,diagmat);
    GLgrad = @(B) aux_grad(B,C,w,diagmat);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat);
end

function cost=aux_cost(B,C,w,diagmat)
    n = size(C,1);
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        tmp  = M/D;
        cost = cost + w(k)*(trace(tmp)-n - log(det(tmp)));
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        D    = diagmat(B*C(:,:,k)*B');
        grad = grad + 2*w(k)*(diag(diag(D).^-1)*B*C(:,:,k) - inv(B'));
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        D    = diagmat(B*C(:,:,k)*B');
        hess = hess + 2*w(k)*(diag(diag(D).^-1)*Z*C(:,:,k) + B'\Z'/B');
    end
    hess = hess/K;
end