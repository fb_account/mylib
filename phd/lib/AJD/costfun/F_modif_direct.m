function [GLcost, GLgrad, GLhess] = F_modif_direct(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    K = size(C,3);
    ddiag = @(x) diag(diag(x));
    cost = 0;
    parfor k=1:K
        cost = cost + w(k)*norm(C(:,:,k) - B\ddiag(B*C(:,:,k)*B')/B','fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    K = size(C,3);
    ddiag = @(x) diag(diag(x));
    grad = 0;
    parfor k=1:K
        G = B*B';
        M = B*C(:,:,k)*B';
        D = ddiag(M);
        Q = G\(M-D)/G;
        grad = grad + 4*w(k)*(Q*D - ddiag(Q)*M);
    end
    grad = grad/B'/K;
end

function hess=aux_hess(B,Z,C,w)
    hess = [];
    error('hessian not implemented, use only first order methods for now');
%     K = size(C,3);
%     ddiag = @(x) diag(diag(x));
%     hess = 0;
%     parfor k=1:K
%         Q  = B'\(B\ddiag(B*C(:,:,k)*B')/B' - C(:,:,k))/B;
%         dQ = (B*B')\(ddiag(Z*C(:,:,k)*B'+B*C(:,:,k)*Z') - (Z/B)*ddiag(B*C(:,:,k)*B') - ddiag(B*C(:,:,k)*B')*(B'\Z'))/(B*B') - (B'\Z')*Q - Q*(Z/B);
%         hess = hess + 4*w(k)*(ddiag(dQ)*B*C(:,:,k)*B' - dQ*ddiag(B*C(:,:,k)*B') - Q*ddiag(Z*C(:,:,k)*B'+B*C(:,:,k)*Z'))/B' ...
%                     + 4*w(k)*(ddiag(Q)*Z*C(:,:,k) + Q*ddiag(B*C(:,:,k)*B')*(B'\Z/B'));
%     end
end