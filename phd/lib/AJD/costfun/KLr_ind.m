function [GLcost, GLgrad, GLhess] = KLr_ind(C,w,diagmat)
    GLcost = @(B) aux_cost(B,C,w,diagmat);
    GLgrad = @(B) aux_grad(B,C,w,diagmat);
    GLhess = @(B,Z) aux_hess(B,Z,C,w,diagmat);
end

function cost=aux_cost(B,C,w,diagmat)
    n = size(C,1);
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        tmp  = D/M;
        cost = cost + w(k)*(trace(tmp)-n - log(det(tmp)));
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w,diagmat)
    n = size(C,1);
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = diagmat(M);
        grad = grad + 2*w(k)*(eye(n) - M\D)/B';
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w,diagmat)
    K = size(C,3);
    hess = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        dM   = B*C(:,:,k)*Z' + Z*C(:,:,k)*B';
        D    = diagmat(M);
        hess = hess + 2*w(k)*M\((D-M)*(B'\Z') + (dM/M)*D)/B';
    end
    hess = hess/K;
end