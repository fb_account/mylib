function [GLcost, GLgrad, GLhess] = R_direct(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = closestdiag_R(M);
        N    = diag(diag(D).^-.5);
        cost = cost + w(k)*norm(logm(N*M*N),'fro')^2;
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = closestdiag_R(M);
        grad = grad + 4*w(k)*logm(D\M)/B';
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w)
    hess = [];
    error('R_direct: hessian not implemented yet, please use first order algorithm');
end