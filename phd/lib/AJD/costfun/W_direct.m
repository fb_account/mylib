function [GLcost, GLgrad, GLhess] = W_direct(C,w)
    GLcost = @(B) aux_cost(B,C,w);
    GLgrad = @(B) aux_grad(B,C,w);
    GLhess = @(B,Z) aux_hess(B,Z,C,w);
end

function cost=aux_cost(B,C,w)
    K = size(C,3);
    cost = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = closestdiag_W(M);
        Sig  = diag(diag(D).^.5);
        Q_   = sqrtm(Sig*M*Sig);
        cost = cost + w(k)*trace((M+D)/2 - Q_);
    end
    cost = cost/K;
end

function grad=aux_grad(B,C,w)
    n = size(C,1);
    K = size(C,3);
    grad = 0;
    parfor k=1:K
        M    = B*C(:,:,k)*B';
        D    = closestdiag_W(M);
        Sig  = diag(diag(D).^.5);
        Q    = sqrtm(inv(Sig*M*Sig));
        grad = grad + w(k)*(eye(n) - Sig*Q*Sig)*B*C(:,:,k);
    end
    grad = grad/K;
end

function hess=aux_hess(B,Z,C,w)
    hess = [];
    error('W_direct: hessian not implemented yet, please use first order algorithm');
end