function [B,C,info,options] = m_NOJoB(C,options)

% function [B,C,info,options] = gp_NOJoB(C,options)
%
% function performing Joint Blind Source Separation over a set of
% covariance matrices. The unmixing matrix which is sought is non
% orthogonal and updated with power iterations.
%
% Inputs:
%   - C: NxNxKxMxM array containing covariance matrices. N corresponds
%        to the number of channels, K to the number of target matrices and
%        M to the number of datasets.
%
%   - options: struct containing fields
%       - B0:      NxNxM array containing the initial unmixing matrices
%                  (default is identity)
%       - tol:     stopping criteria (default is 10^-12)
%       - maxiter: the maximal number of iterations (default is 1000)
%
% Outputs:
%   - B: NxNxM array containing the final unmixing matrices
%
%   - C: NxNxKxMxM array containing the diagonalized covariance matrices
%
%   - info: 1xnIter struct containing fields
%       - it:    iteration number
%       - cost:  value of the cost function at iteration it
%       - dcost: difference of the cost function between iteration it-1 
%                and it (stopping criteria)
%       - B:     unmixing matrix at iteration it
%
% reference: ORTHOGONAL AND NON-ORTHOGONAL JOINT BLIND SOURCE SEPARATION 
%            IN THE LEAST-SQUARES SENSE. 
%            M. Congedo, R. Phlypo, J. Chatel-Goldman
%            20th European Signal Processing Conference (EUSIPCO 2012)
%
% see also: gp_OJoB
%
% *** History: 18-Feb-2015
% *** Author: Florent BOUCHARD, GIPSA-Lab, 2015



% get variables from C
N = size(C,1); % number of channels
K = size(C,3); % number of matrices
M = size(C,4); % number of datasets

% initialize parameters according to options input
%%% default
B0 = zeros(N,N,M);
for m=1:M
    B0(:,:,m) = eye(N);
end
opt = struct('B0',B0,'weights',ones(1,K),'tol',1e-6,'maxiter',100);
%%% get final options
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
%%% get parameters
B0      = options.B0;
w       = options.weights;
tol     = options.tol;
maxiter = options.maxiter;

% initialize B
B = B0;

% initialize info struct
info = struct('it',0,'cost',cost_fun(C,B,w),'stopcrit',nan,'B',B);

% Main Loop
cost   = cost_fun(C,B,w);
stopcrit   = 1;
it     = 0;
while it<maxiter && stopcrit>tol
    it = it+1;
    % update B with power iterations
    B = update(B,C,w);
    B = update(B,C,w);
    % get cost and dcost
    cost_tmp = cost_fun(C,B,w);
    stopcrit = norm(info(it).B/B'-eye(N),'fro')^2/N;
    cost     = cost_tmp;
    % fill info struct
    info(it+1).it        = it;
    info(it+1).cost      = cost;
    info(it+1).stopcrit  = stopcrit;
    info(it+1).B         = multitransp(B);

    % ** Display
%     disp(['it:  ' int2str(it) '    cost:  ' num2str(cost,'%10.5e\n') '    dcost:  ' num2str(dcost,'%10.5e\n')]);
end

% compute final C
C = getC(C,B);

% get final B by transposing current one -> should be done properly !!
B = multitransp(B);
end

function cost = cost_fun(C,B,w)
    K = size(C,3);
    M = size(C,4);
    cost=0;
    for m1=1:M
        for m2=m1:M
            for k=1:K
                tmp  = B(:,:,m1)'*C(:,:,k,m1,m2)*B(:,:,m2);
                cost = cost + w(k)*trace(tmp*(tmp' - diag(diag(tmp'))));
            end
        end
    end
end

function C = getC(C,B)
    K = size(C,3); % number of matrices
    M = size(C,4); % number of datasets
    C_tmp = zeros(size(C));
    for k=1:K
        for m1=1:M
            for m2=1:M
                C_tmp(:,:,k,m1,m2) = B(:,:,m1)'*C(:,:,k,m1,m2)*B(:,:,m2);
            end
        end
    end
    C = C_tmp;
end

function B = update(B,C,w)
    N = size(C,1); % number of channels
    K = size(C,3); % number of matrices
    M = size(C,4); % number of datasets
    for m1=1:M
        B_tmp = B(:,:,m1);
        Mtot  = zeros(N);
        Mn    = zeros(N,N,N);
        for n=1:N
            for k=1:K
                for m2=1:M
                    v = w(k)*C(:,:,k,m1,m2)*B(:,n,m2);
                    Mn(:,:,n) = Mn(:,:,n) + v*v';
                end
            end
            Mtot = Mtot + Mn(:,:,n);
        end
        L = chol(Mtot,'lower');
        for n=1:N
            x = linsolve(L, Mn(:,:,n)*B(:,n,m1));
            y = linsolve(L', x);
            B_tmp(:,n) = y*(sqrt(pinv(y'*Mn(:,:,n)*y)));
        end
        B(:,:,m1) = B_tmp;      
    end
end