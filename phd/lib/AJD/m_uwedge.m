function [B,C,info,options] = m_uwedge(C,options)

% This function has been re-coded for input/output format compatibility
% reasons.
% It is also to be able to have the same stopping criterion for all our
% algorithms (to be added).
%
% The original code can be found at : *add*
% All credit goes to Petr Tichavsky.
% Please cite:
%
%  P. Tichavsky, A. Yeredor and J. Nielsen,
%     "A Fast Approximate Joint Diagonalization Algorithm
%      Using a Criterion with a Block Diagonal Weight Matrix",
%      ICASSP 2008, Las Vegas
% or                                                                            
%  P. Tichavsky and A. Yeredor,                                                
%     "Fast Approximate Joint Diagonalization Incorporating Weight Matrices"   
%     IEEE Transactions of Signal Processing, 2009. 
%
% B such that B*C(:,:,k)*B' as much diagonal as possible for all k
%
% WARNING: normalization of B depends only on the first matrix of the set
% so this matrix should be chosen wisely ! 

% variables:
nChan = size(C,1);
nMat  = size(C,3);

% initialize parameters according to options input
%%% default

B0 = eye(nChan); % maybe we can put a better initialization as the one in the original function

opt = struct('B0',B0,'weights',ones(1,nMat),'tol',1e-6,'maxiter',100);
%%% get final options
if nargin<2
    options = getOptions([],opt);
else
    options = getOptions(options,opt);
end
%%% get parameters
B0      = options.B0;
w       = options.weights;
tol     = options.tol;
maxiter = options.maxiter;


% initialize B
B = B0;

% initialize some variables
diagC = zeros(nChan, nMat); % diag part of the transformed matrices
C_tmp = zeros(nChan,nChan,nMat); % tmp transformed matrices

% symmetrize input matrices and initialize C_tmp and diagC
for k=1:nMat
    C(:,:,k)     = .5*(C(:,:,k)+C(:,:,k)');
    C_tmp(:,:,k) = sqrt(w(k))*B*C(:,:,k)*B';
    diagC(:,k)   = sqrt(w(k))*diag(C_tmp(:,:,k));
end

% initialize cost and dcost
cost  = sum(C_tmp(:).^2)-sum(diagC(:).^2);
stopcrit = tol+1; % dumb initialization to enter loop

% initialize info struct
info = struct('it',0,'cost',cost,'stopcrit',nan,'B',B);


% Main Loop
it = 0;
while it<maxiter && stopcrit>tol
    it = it + 1;
    W  = diagC*diagC';
    C1 = zeros(nChan);
    for n=1:nChan
        C1(:,n)=sum(squeeze(C_tmp(:,n,:)).*diagC,2);
    end
    D0=W.*W'-diag(W)*diag(W)';
    A0=eye(nChan)+(C1.*W-diag(diag(W))*C1')./(D0+eye(nChan));
    B=A0\B;
    % normalize the result according to first matrix of the set
%     Waux=B*C(:,:,1)*B';
    Waux = B*B'; % modif to have unit line norm instead
    aux=1./sqrt(abs(diag(Waux)));
    B=diag(aux)*B;
    % update transformed matrices
    for k=1:nMat
        C_tmp(:,:,k) = sqrt(w(k))*B*C(:,:,k)*B';
        diagC(:,k)   = sqrt(w(k))*diag(C_tmp(:,:,k));
    end
    % get cost, dcost
    cost      = sum(C_tmp(:).^2)-sum(diagC(:).^2);
    stopcrit = norm(info(it).B/B-eye(n),'fro')^2/n;
    % fill info struct
    info(it+1) = struct('it',it,'cost',cost,'stopcrit',stopcrit,'B',B);

    % print some info
%     disp(['it: ' int2str(it) '     cost: ' num2str(cost) '     dcost: ' num2str(dcost)]);
end

% fill out C
C = C_tmp;