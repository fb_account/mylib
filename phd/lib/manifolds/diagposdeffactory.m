function M = diagposdeffactory(n)
% Returns a manifold structure to optimize over the manifold of diagonal matrices 
% with strictly positive elements.
%
% function M = diagposdeffactory(n)
%
% This manifold is a closed submanifold of the manifold of symmetric positive
% definite matrices.
% details to be added.
%
% author: Florent BOUCHARD, GIPSA-Lab, 2018

M.name = @() sprintf('diagonal positive definite manifold D(%d)', n);

M.dim = @() n;

M.inner = @(X, eta, zeta) sum((diag(eta)./diag(X)) .* (diag(zeta)./diag(X)));
    
M.norm = @(X, eta) sqrt(M.inner(X, eta, eta));

M.dist = @(x, y) error('diagposdeffactory.dist not implemented yet.');

M.typicaldist = @() sqrt(n);


M.proj = @projection;
function eta = projection(X, Z)
    eta = diag(diag(Z));
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    rgrad = diag((diag(X).^2).*diag(egrad));
end

M.ehess2rhess = @ehess2rhess;
function hess = ehess2rhess(X, egrad, ehess, eta)
    hess = diag((diag(X).^2).*ddiag(ehess) + diag(eta).*diag(egrad).*diag(X));
end

M.retr = @retraction;
function Y = retraction(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    Y = exponential(X,eta,t);
end

M.exp = @exponential;
function Y = exponential(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    Y = real(diag(diag(X).*real(exp(t*diag(eta)./diag(X)))));
    % some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(Y)<1e-10 || isnan(rcond(Y))
        t = t/2;
        Y = real(diag(diag(X).*real(exp(t*diag(eta)./diag(X)))));
    end 
end


M.hash = @(X) ['z' hashmd5(X(:))];

M.rand = @random;
function X = random()
    X = diag(rand(n,1));
end

M.randvec = @randomvec;
function eta = randomvec(X)
    % random vector on the tangent space
    eta = diag(randn(n,1));
end

M.lincomb = @matrixlincomb;

M.zerovec = @(X) zeros(n);

M.transp = @(x1, x2, d) projection(x2, d);

% vec and mat are not isometries, because of the scaled inner metric
M.vec = @(X, Z) Z(:);
M.mat = @(X, z) reshape(z,n,n);
M.vecmatareisometries = @() false;

end