% some precision parameter
eps = 1e-6;
% size of manifolds
n = 8;

% dumb SPD matrices for testing
K = 5;
C = zeros(n,n,K);
for k=1:K
    tmp      = randn(n);
    C(:,:,k) = tmp*tmp';
end

% dumb cost function for testing
[cost, egrad, ehess] = lKL_direct(C,ones(1,K));

%% (A) GL left

GLl = GLleftfactory(n);

% (0.1) generate some point on GLl : GLl.rand
x  = GLl.rand();
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
% (0.2) generate some tangent vector : GLl.randvec
% always ok since no condition
xi = GLl.randvec(x);
%
% (1) GLl.proj -> always ok since identity function
z  = randn(n);
xi = GLl.proj(x,z);
%
% (2) GLl.retr
x  = GLl.rand();
xi = GLl.randvec(x);
t  = rand;
y  = GLl.retr(x,zeros(n));
if norm(x-y,'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = GLl.retr(x,xi,t);
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
%
% (3) gradient and hessian
problem       = [];
problem.M     = GLl;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);

%% (B) oblique GL left

OGLl = obliqueGLleftfactory(n);

% (0.1) generate some point on OGLl : OGLl.rand
x  = OGLl.rand();
% (i) check x in GL
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
% (ii) check x has the correct diagonal scaling
if norm(diag(x*x') - ones(n,1),'fro')>eps, disp('M.rand: norm lines of x different from one'); end
%
% (0.2) generate some tangent vector : OGLl.randvec
xi = OGLl.randvec(x);
if norm(diag(xi*x'),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) OGLl.proj
z   = randn(n);
xi  = OGLl.proj(x,z);
if abs(OGLl.inner(x,xi,z-xi))>eps, disp('M.proj: orthogonality issue'); end
tmp = OGLl.proj(x,xi) - xi;
if OGLl.norm(x,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) OGLl.retr
x  = OGLl.rand();
xi = OGLl.randvec(x);
t  = rand;
y  = OGLl.retr(x,zeros(n));
if norm((x-y),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = OGLl.retr(x,xi,t);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
% (ii) check norm of the lines of y
if norm(diag(y*y') - ones(n,1),'fro')>eps, disp('M.retr: norm lines of x different from one'); end
%
% (3) OP.projonman
x = GLl.rand();
y = OGLl.projonman(x);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.projonman: y singular'); end
% (ii) check norm of the lines of y
if norm(diag(y*y') - ones(n,1),'fro')>eps, disp('M.projonman: norm lines of x different from one'); end
%
% (4) gradient and hessian
problem       = [];
problem.M     = OGLl;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);


%% (C) intrinsic GL left 

IGLl = intrinsicGLleftfactory(n,C);

% (0.1) generate some point on IGLl : IGLl.rand
x  = IGLl.rand();
% (i) check x in GL
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
% (ii) check x has the correct diagonal scaling
tmp   = 0;
for k=1:K
    tmp = tmp + diag(x*C(:,:,k)*x').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.rand: norm lines of x different from expected'); end
%
% (0.2) generate some tangent vector : IGLl.randvec
xi = IGLl.randvec(x);
Q    = 0;
for k=1:K
    Q = Q + C(:,:,k)*x'*diag(diag(x*C(:,:,k)*x'));
end
if norm(diag(xi*Q),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) IGLl.proj
z   = randn(n);
xi  = IGLl.proj(x,z);
if abs(IGLl.inner(x,xi,z-xi))>eps, disp('M.proj: orthogonality issue'); end
tmp = IGLl.proj(x,xi) - xi;
if IGLl.norm(x,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) IGLl.retr
x  = IGLl.rand();
xi = IGLl.randvec(x);
t  = rand;
y  = IGLl.retr(x,zeros(n));
if norm((x-y),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = IGLl.retr(x,xi,t);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
% (ii) check norm of the lines of y
tmp   = 0;
for k=1:K
    tmp = tmp + diag(y*C(:,:,k)*y').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.retr: norm lines of x different from expected'); end
%
% (3) IGLl.projonman
x = GLl.rand();
y = IGLl.projonman(x);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.projonman: y singular'); end
% (ii) check norm of the lines of y
tmp   = 0;
for k=1:K
    tmp = tmp + diag(y*C(:,:,k)*y').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.projonman: norm lines of x different from expected'); end
%
% (4) gradient and hessian
problem       = [];
problem.M     = IGLl;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);

%% (D) non holonomic GL left

nhGLl = nonholonomicGLleftfactory(n);

% (0.1) generate some point on nhGLl : nhGLl.rand
x  = nhGLl.rand();
% check x in GL (no other conditions)
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
%
% (0.2) generate some horizontal lift : nhGLl.randvec
xi = nhGLl.randvec(x);
if norm(diag((x*x')\xi*x'),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) nhGLl.proj
z   = randn(n);
xi  = nhGLl.proj(x,z);
if abs(nhGLl.inner(x,xi,z-xi))>eps, disp('M.proj: orthogonality issue'); end
tmp = nhGLl.proj(x,xi) - xi;
if nhGLl.norm(x,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) nhGLl.retr
x  = nhGLl.rand();
x  = OGLl.projonman(x); % normalize x for the case retr_norm = true.
xi = nhGLl.randvec(x);
t  = rand;
y  = nhGLl.retr(x,zeros(n));
if norm((x-y),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = nhGLl.retr(x,xi,t);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
%
% (3) gradient and hessian
problem       = [];
problem.M     = nhGLl;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);
















