function M = intrinsicGLleftfactory(n,C)


% define GL left manifold
M_parent = GLleftfactory(n);



M.name = @() sprintf('Intrinsic GL manifold endowed with the left-invariant metric int-GL(%d)', n);

M.dim = @() (n-1)*n;

M.inner = M_parent.inner;

M.norm = M_parent.norm;

M.dist = @(x, y) error('intrinsicGLleftfactory.dist not implemented yet.');

M.typicaldist = @() sqrt(n*n-n);


% symm = @(z) .5*(z+z');
function eta = symm(z)
    eta = .5*(z+z');
end


function Q = getQ(x,C)
    K = size(C,3);
    Q = 0;
    for k=1:K
        Q = Q + C(:,:,k)*x'*diag(diag(x*C(:,:,k)*x'));
    end
end
function dQ = getdQ(x,eta,C)
    K  = size(C,3);
    dQ = 0;
    for k=1:K
        dQ = dQ + C(:,:,k)*(eta'*diag(diag(x*C(:,:,k)*x'))...
            + 2*x'*(diag(diag(eta*C(:,:,k)*x'))));
    end
end

function D = solveDiagEq(x,z,Q)
    D = diag(((x*x').*(Q'*Q))\diag(z*Q));
end


M.proj = @projection;
function eta = projection(x, z)
    Q   = getQ(x,C);
    D   = solveDiagEq(x,z,Q);
    eta = z -(x*x')*D*Q';
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(x, egrad)
    % get gradient on parent
    rgrad = M_parent.egrad2rgrad(x,egrad);
    % project it on tangent
    rgrad = projection(x,rgrad);
end

M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(x, egrad, ehess, eta)
    % get gradient and hessian on parent
    rgrad = M_parent.egrad2rgrad(x,egrad);
    rhess = M_parent.ehess2rhess(x,egrad,ehess,eta);
    % correct hessian
    Q     = getQ(x,C);
    dQ    = getdQ(x,eta,C);
    D     = solveDiagEq(x,rgrad,Q);
    tmp   = x\eta;
    rhess = rhess + x*(symm(Q*D*x)*tmp - symm(tmp*Q*D*x)) - symm(eta*x')*D*Q' - (x*x')*D*dQ';
    % project it on tangent space
    rhess = projection(x,rhess);
end

M.retr = @retraction;
function y = retraction(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % retraction on parent
    y = M_parent.retr(x,eta,t);
    % project on manifold
    if ~(rcond(y)<1e-10 || isnan(rcond(y)))
        y = projonman(y);
    end
    
    % some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(y)<1e-10 || isnan(rcond(y))
        t = t/2;
        y = M_parent.retr(x,eta,t);
        if ~(rcond(y)<1e-10 || isnan(rcond(y)))
            y = projonman(y);
        end
    end 
end

M.exp = @exponential;
function y = exponential(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    y = retraction(x, eta, t);
    warning('intrinsicGLleftfactory:exp', ...
        ['Exponential for intrinsic GL ' ...
        'manifold not implemented yet. Used retraction instead.']);
end

M.log = @(x, y) error('intrinsicGLleftfactory.log not implemented yet.');

M.hash = @(x) ['z' hashmd5(x(:))];

M.rand = @random;
function x = random()
    % get random matrix on parent
    x = M_parent.rand();
    % project on manifold
    x = projonman(x);
end

M.randvec = @randomvec;
function eta = randomvec(x)
    % random vector on the tangent space
    eta = randn(n);
    eta = projection(x, eta);
    nrm = M_parent.norm(x, eta);
    eta = eta / nrm;
end

M.lincomb = @matrixlincomb;

M.zerovec = @(x) zeros(n);

M.transp = @(x1, x2, d) projection(x2, d);

M.vec = @(x, u_mat) u_mat(:);

M.mat = @(x, u_vec) reshape(u_vec, [n,n]);

M.vecmatareisometries = @() false;



M.GL2man = @projonman;
function y = projonman(x)
    K = size(C,3);
    % set the correct diagonal scaling
    tmp = 0;
    for k=1:K
        tmp = tmp + diag(x*C(:,:,k)*x').^2;
    end
    D = diag(tmp.^(-1/4));
    y = D*x;
end

end