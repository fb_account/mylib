function M = GLleftfactory(n)


M.name = @() sprintf('GL endowed with the left-invariant metric GL(%d)', n);

M.dim = @() n*n;

M.inner = @(x, eta, zeta) trace((x\eta)*(x\zeta)');

M.norm = @(x, eta) sqrt(M.inner(x, eta, eta));

M.dist = @(x, y) error('GLleftfactory.dist not implemented yet.');

M.typicaldist = @() n;

M.proj = @(x, eta) eta;

M.tangent = M.proj;


% symm      = @(Z) .5*(Z+Z');
function eta_S = symm(Z)
    eta_S = .5*(Z+Z');
end


M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(x, egrad)
    rgrad = (x*x')*egrad;
end

M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(x, egrad, ehess, eta)
    tmp1 = x*x';
    tmp2 = x\eta;
    rhess = tmp1*ehess + symm(x*eta')*egrad + x*(symm(tmp2*egrad'*x) - symm(x'*egrad)*tmp2);
end

M.retr = @retraction;
function Y = retraction(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % we use the Riemannian exponential here
    Y = exponential(X, eta, t);
end

M.exp = @exponential;
function y = exponential(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    tmp1 = x\eta;
    tmp2 = tmp1 - tmp1';
    y = x*expm(t*tmp1')*expm(t*tmp2);
%     % some fix to correct eventual numerical instability
%     % (may occur during linesearch)
%     while rcond(y)<1e-10 || isnan(rcond(y))
%         t = t/2;
%         y = x*expm(t*tmp1')*expm(t*tmp2);
%     end 
end

M.log = @(x, y) error('GLleftfactory.log not implemented yet.');

M.hash = @(x) ['z' hashmd5(x(:))];

M.rand = @random;
function x = random()
    x = randn(n);
    while abs(det(x))<1e-3 % ensure that x is not too close to singularity
        x = randn(n);
    end
    % ensure that det(x)>0
    if det(x)<0
        d = eye(n);
        d(1,1) =-1;
        x = d*x;
    end
end

M.randvec = @(x) randomvec(x, M.norm);
function u = randomvec(x, normfun) 
    u = randn(n);
    u = u / normfun(x,u);
end

M.lincomb = @matrixlincomb;

M.zerovec = @(x) zeros(n);

M.transp = @(x1, x2, d) (x2/x1)*d;

M.vec = @(x, u_mat) u_mat(:);

M.mat = @(x, u_vec) reshape(u_vec, [n,n]);

M.vecmatareisometries = @() false;


end