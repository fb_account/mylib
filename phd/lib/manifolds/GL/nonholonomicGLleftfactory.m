function M = nonholonomicGLleftfactory(n,retr_norm)

if nargin<2, retr_norm=true; end


% warning('if you use an unproperly defined criterion on the quotient, only use first order optimization methods');

% define GL left manifold
M_parent = GLleftfactory(n);



M.name = @() sprintf('Non-holonomic GL manifold endowed with the left-invariant metric nh-GL(%d)', n);

M.dim = @() (n-1)*n;

M.inner = M_parent.inner;

M.norm = M_parent.norm;

M.dist = @(x, y) error('nonholonomicGLleftfactory.dist not implemented yet.');

M.typicaldist = @() sqrt(n*n-n);

function D = solveDiagEq(x,z)
    tmp1 = x*x';
    tmp2 = inv(x*x');
    D    = diag((tmp2.*tmp1)\diag(tmp2*z*x'));
end


M.proj = @projection;
function eta = projection(x, z)
    D   = solveDiagEq(x,z);
    eta = z - D*x;
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(x, egrad)
    % get gradient from parent
    rgrad = M_parent.egrad2rgrad(x,egrad);
    % project it on horizontal space
    % (not needed for properly defined criteria on quotient)
    rgrad = projection(x,rgrad);
end

% this is only valid for properly defined criteria on the quotient !
M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(x, egrad, ehess, eta)
    % get hessian from parent
    rhess = M_parent.ehess2rhess(x,egrad,ehess,eta);
    % project it on horizontal space
    rhess = projection(x,rhess);
end

M.retr = @retraction;
function y = retraction(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % exponential on parent
    % (be careful, if alternative retraction used, it needs to be invariant
    % along equivalence classes)
    y = M_parent.exp(x,eta,t);
    % rescaling to avoid bad scaling issues
    if retr_norm
        if ~(rcond(y)<1e-10 || isnan(rcond(y)))
        	y = normalization(y);
        end
    end

    % some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(y)<1e-10 || isnan(rcond(y))
        t = t/2;
        y = M_parent.exp(x,eta,t);
        % rescaling to avoid bad scaling issues
        if retr_norm
            if ~(rcond(y)<1e-10 || isnan(rcond(y)))
                y = normalization(y);
            end
        end
    end
end

function y = normalization(x)
    D = diag(diag(x*x').^(-.5));
    y = D*x;
end


M.exp  = M_parent.exp;

M.log = @(x, y) error('nonholonomicGLleftfactory.log not implemented yet.');

M.hash = @(x) ['z' hashmd5(x(:))];

if retr_norm
    M.rand = @() normalization(M_parent.rand());
else
   M.rand = M_parent.rand();
end

M.randvec = @randomvec;
function u = randomvec(x) 
    u = randn(n);
    u = projection(x,u);
    u = u / M_parent.norm(x,u);
end

M.lincomb = @matrixlincomb;

M.zerovec = @(x) zeros(n);

M.transp = @(x1, x2, d) projection(x2, (x2/x1)*d);

M.vec = @(x, u_mat) u_mat(:);

M.mat = @(x, u_vec) reshape(u_vec, [n,n]);

M.vecmatareisometries = @() false;

end