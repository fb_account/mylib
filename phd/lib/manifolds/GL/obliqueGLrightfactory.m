function M = obliqueGLrightfactory(n)

% define GL right manifold
M_parent = GLrightfactory(n);


M.name = @() sprintf('Oblique GL manifold endowed with the right-invariant metric O-GL(%d)', n);

M.dim = @() (n-1)*n;

M.inner = M_parent.inner;

M.norm = M_parent.norm;

M.dist = @(x, y) error('obliqueGLrightfactory.dist not implemented yet.');

M.typicaldist = @() sqrt(n*n-n);


% symm = @(z) .5*(z+z');
function eta = symm(z)
    eta = .5*(z+z');
end


M.proj = @projection;
function eta = projection(x, z)
    tmp = x*x';
    eta = z - diag(diag(z*x'))*diag(diag(tmp*tmp).^-1)*tmp*x;
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(x, egrad)
    % get gradient on parent
    rgrad = M_parent.egrad2rgrad(x,egrad);
    % project it on tangent
    rgrad = projection(x,rgrad);
end

M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(x, egrad, ehess, eta)
    % get gradient and hessian on parent
    rgrad = M_parent.egrad2rgrad(x,egrad);
    rhess = M_parent.ehess2rhess(x,egrad,ehess,eta);
    % correct hessian
    tmp1  = x*x';
    tmp2  = eta/x;
    D     = diag(diag(rgrad*x'))*diag(diag(tmp1*tmp1).^-1);
    rhess = rhess + (tmp2*symm(tmp1*D) - symm(tmp1*D*tmp2))*x - D*(eta*(x'*x) +x*symm(eta'*x));
    % project it on tangent
    rhess = projection(x,rhess);
end

M.retr = @retraction;
function y = retraction(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % retraction on parent
    y = M_parent.retr(x,eta,t);
    % project on manifold
    if ~(rcond(y)<1e-10 || isnan(rcond(y)))
        y = projonman(y);
    end

    % some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(y)<1e-10 || isnan(rcond(y))
        t = t/2;
        y = M_parent.retr(x,eta,t);
        if ~(rcond(y)<1e-10 || isnan(rcond(y)))
            y = projonman(y);
        end
    end 
end

M.exp = @exponential;
function y = exponential(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    y = retraction(x, eta, t);
    warning('obliqueGLrightfactory:exp', ...
        ['Exponential for oblique GL ' ...
        'manifold not implemented yet. Used retraction instead.']);
end

M.log = @(x, y) error('obliqueGLrightfactory.log not implemented yet.');

M.hash = @(x) ['z' hashmd5(x(:))];

M.rand = @random;
function x = random()
    % get random matrix on parent
    x = M_parent.rand();
    % project on manifold
    x = projonman(x);
end

M.randvec = @randomvec;
function eta = randomvec(x)
    % random vector on the tangent space
    eta = randn(n);
    eta = projection(x, eta);
    nrm = M_parent.norm(x, eta);
    eta = eta / nrm;
end

M.lincomb = @matrixlincomb;

M.zerovec = @(x) zeros(n);

M.transp = @(x1, x2, d) projection(x2, d*(x1\x2));
% M.transp = @(x1, x2, d) projection(x2, d);

M.vec = @(x, u_mat) u_mat(:);

M.mat = @(x, u_vec) reshape(u_vec, [n,n]);

M.vecmatareisometries = @() false;



M.GL2man = @projonman;
function y = projonman(x)
    % normalize norm of lines of x
    D = diag(diag(x*x').^(-.5));
    y = D*x;
end


end