function M = nonholonomicGLrightfactory_unproper(n,retr_norm)

if nargin<2
    retr_norm   = true;
end


% define GL right manifold
M_parent = GLrightfactory(n);



M.name = @() sprintf('Non-holonomic GL manifold endowed with the right-invariant metric nh-GL(%d)', n);

M.dim = @() (n-1)*n;

M.inner = M_parent.inner;

M.norm = M_parent.norm;

M.dist = @(x, y) error('nonholonomicGLrightfactory_unproper.dist not implemented yet.');

M.typicaldist = @() sqrt(n*n-n);


M.proj = @projection;
function eta = projection(x, z)
    eta = z - diag(diag(z/x))*x;
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(x, egrad)
    % get gradient from parent
    rgrad = M_parent.egrad2rgrad(x,egrad);
    % project it on horizontal space
    % (not needed for properly defined criteria on quotient)
    rgrad = projection(x,rgrad);
end

M.ehess2rhess = @(x, egrad, ehess, eta) error('nonholonomicGLrightfactory_unproper.ehess2rhess not implemented, only use first order optimization methods');


M.retr = @retr_GLr;

function y = retr_GLr(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    y = M_parent.exp(x,eta,t);
    % rescaling to avoid bad scaling issues
    if retr_norm
        y = normalization(y);
    end
    
    % some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(y)<1e-10 || isnan(rcond(y))
        t = t/2;
        y = M_parent.exp(x,eta,t);
        % rescaling to avoid bad scaling issues
        if retr_norm
            y = normalization(y);
        end
    end 
end

function y = normalization(x)
    D = diag(diag(x*x').^(-.5));
    y = D*x;
end

M.exp = @exponential;
function y = exponential(x, eta, t)
    if nargin < 3
        t = 1.0;
    end
    y = retr_GLr(x, eta, t);

    warning('nonholonomicGLrightfactory_unproper:exp', ...
        ['Exponential for nonholonomic GL' ...
        'manifold not implemented yet (and does not exist). Used M.retr instead.']);
end

M.log = @(x, y) error('nonholonomicGLrightfactory_unproper.log not implemented yet.');

M.hash = @(x) ['z' hashmd5(x(:))];


if retr_norm
    M.rand = @() normalization(M_parent.rand());
else
   M.rand = M_parent.rand();
end



M.randvec = @randomvec;
function u = randomvec(x) 
    u = randn(n);
    u = projection(x,u);
    u = u / M_parent.norm(x,u);
end

M.lincomb = @matrixlincomb;

M.zerovec = @(x) zeros(n);

% modified version of transport to fit with needs of modified retraction
% and equivalence classes for first order algorithms.
% (still defines proper transport with other retractions) 
M.transp = @(x1, x2, d) projection(x2, d*(x1\x2));

M.vec = @(x, u_mat) u_mat(:);

M.mat = @(x, u_vec) reshape(u_vec, [n,n]);

M.vecmatareisometries = @() false;

end