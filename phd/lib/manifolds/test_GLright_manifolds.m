% some precision parameter
eps = 1e-6;
% size of manifolds
n = 8;

% dumb SPD matrices for testing
K = 5;
C = zeros(n,n,K);
for k=1:K
    tmp      = randn(n);
    C(:,:,k) = tmp*tmp';
end

% dumb cost function for testing
[cost, egrad, ehess] = lKL_direct(C,ones(1,K));

%% (A) GL right

GLr = GLrightfactory(n);

% (0.1) generate some point on GLr : GLr.rand
x  = GLr.rand();
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
% (0.2) generate some tangent vector : GLr.randvec
% always ok since no condition
xi = GLr.randvec(x);
%
% (1) GLr.proj -> always ok since identity function
z  = randn(n);
xi = GLr.proj(x,z);
%
% (2) GLr.retr
x  = GLr.rand();
xi = GLr.randvec(x);
t  = rand;
y  = GLr.retr(x,zeros(n));
if norm(x-y,'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = GLr.retr(x,xi,t);
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
%
% (3) gradient and hessian
problem       = [];
problem.M     = GLr;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);

%% (B) oblique GL right

OGLr = obliqueGLrightfactory(n);

% (0.1) generate some point on OGLr : OGLr.rand
x  = OGLr.rand();
% (i) check x in GL
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
% (ii) check x has the correct diagonal scaling
if norm(diag(x*x') - ones(n,1),'fro')>eps, disp('M.rand: norm lines of x different from one'); end
%
% (0.2) generate some tangent vector : OGLr.randvec
xi = OGLr.randvec(x);
if norm(diag(xi*x'),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) OGLr.proj
z   = randn(n);
xi  = OGLr.proj(x,z);
if abs(OGLr.inner(x,xi,z-xi))>eps, disp('M.proj: orthogonality issue'); end
tmp = OGLr.proj(x,xi) - xi;
if OGLr.norm(x,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) OGLr.retr
x  = OGLr.rand();
xi = OGLr.randvec(x);
t  = rand;
y  = OGLr.retr(x,zeros(n));
if norm((x-y),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = OGLr.retr(x,xi,t);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
% (ii) check norm of the lines of y
if norm(diag(y*y') - ones(n,1),'fro')>eps, disp('M.retr: norm lines of x different from one'); end
%
% (3) OP.projonman
x = GLr.rand();
y = OGLr.projonman(x);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.projonman: y singular'); end
% (ii) check norm of the lines of y
if norm(diag(y*y') - ones(n,1),'fro')>eps, disp('M.projonman: norm lines of x different from one'); end
%
% (4) gradient and hessian
problem       = [];
problem.M     = OGLr;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);


%% (C) intrinsic GL left 

IGLr = intrinsicGLrightfactory(n,C);

% (0.1) generate some point on IGLr : IGLr.rand
x  = IGLr.rand();
% (i) check x in GL
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
% (ii) check x has the correct diagonal scaling
tmp   = 0;
for k=1:K
    tmp = tmp + diag(x*C(:,:,k)*x').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.rand: norm lines of x different from expected'); end
%
% (0.2) generate some tangent vector : IGLr.randvec
xi = IGLr.randvec(x);
Q    = 0;
for k=1:K
    Q = Q + C(:,:,k)*x'*diag(diag(x*C(:,:,k)*x'));
end
if norm(diag(xi*Q),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) IGLr.proj
z   = randn(n);
xi  = IGLr.proj(x,z);
if abs(IGLr.inner(x,xi,z-xi))>eps, disp('M.proj: orthogonality issue'); end
tmp = IGLr.proj(x,xi) - xi;
if IGLr.norm(x,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) IGLr.retr
x  = IGLr.rand();
xi = IGLr.randvec(x);
t  = rand;
y  = IGLr.retr(x,zeros(n));
if norm((x-y),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = IGLr.retr(x,xi,t);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
% (ii) check norm of the lines of y
tmp   = 0;
for k=1:K
    tmp = tmp + diag(y*C(:,:,k)*y').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.retr: norm lines of x different from expected'); end
%
% (3) IGLr.projonman
x = GLr.rand();
y = IGLr.projonman(x);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.projonman: y singular'); end
% (ii) check norm of the lines of y
tmp   = 0;
for k=1:K
    tmp = tmp + diag(y*C(:,:,k)*y').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.projonman: norm lines of x different from expected'); end
%
% (4) gradient and hessian
problem       = [];
problem.M     = IGLr;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);



%% (D) non holonomic GL right

nhGLr = nonholonomicGLrightfactory(n,'retr_modif',false);

% (0.1) generate some point on nhGLr : nhGLr.rand
x  = nhGLr.rand();
% check x in GL (no other conditions)
if rcond(x)<eps || isnan(rcond(x)), disp('M.rand: singular'); end 
%
% (0.2) generate some "horizontal lift" : nhGLr.randvec
xi = nhGLr.randvec(x);
if norm(diag(xi/x),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) nhGLr.proj
z   = randn(n);
xi  = nhGLr.proj(x,z);
if abs(nhGLr.inner(x,xi,z-xi))>eps, disp('M.proj: orthogonality issue'); end
tmp = nhGLr.proj(x,xi) - xi;
if nhGLr.norm(x,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) nhGLr.retr
x  = nhGLr.rand();
x  = OGLr.projonman(x); % normalize x for the case retr_norm = true.
xi = nhGLr.randvec(x);
t  = rand;
y  = nhGLr.retr(x,zeros(n));
if norm((x-y),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
y  = nhGLr.retr(x,xi,t);
% (i) check y in GL 
if rcond(y)<eps || isnan(rcond(y)), disp('M.retr: y singular'); end
%
% (3) gradient and hessian
problem       = [];
problem.M     = nhGLr;
problem.cost  = cost;
problem.egrad = egrad;

figure;
checkgradient(problem);














