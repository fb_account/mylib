function X = GL2polar(x)
% performs the polar decomposition of an invertible matrix.
% input  : n x n matrix x.
% output : struct X such that
%            - X.S : n x n symmetric positive definite matrix
%            - X.U : n x n orthogonal matrix
%          and we have x = X.S * X.U

% less stability with this !! (it happened that X.U not very orthogonal)
% X.S = sqrtm(x*x');
% X.U = X.S\x;

% note : also possible to compute
% best for numerical efficiency ? -> not always consistent
[L,D,R] = svd(x, 0); % x=L*D*R'
X.S = L*D*L';
X.U = L*R';

end