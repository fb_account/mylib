function [cost,egrad,ehess] = fun2opt_GL2polar(GLcost,GLgrad,GLhess)
    % objective function
    cost  = @(X) GLcost(X.S*X.U);
    % gradient
    egrad = @(X) gradGL2polar(X,GLgrad);
    function egrad = gradGL2polar(X,GLgrad)
        tmp=GLgrad(X.S*X.U);
        egrad = struct('S',tmp*X.U','U',X.S*tmp);
    end
    % hessian
    ehess = @(X,Z) hessGL2Polar(X,Z,GLgrad,GLhess);
    function ehess = hessGL2Polar(X,Z,GLgrad,GLhess)
        tmp1=GLgrad(X.S*X.U);
        tmp2=GLhess(X.S*X.U,Z.S*X.U+X.S*Z.U);
        ehess = struct('S',tmp2*X.U' + tmp1*Z.U','U',X.S*tmp2 + Z.S*tmp1);
    end
end