function M = intrinsicpolarfactory(n,C)


% define polar manifold
M_parent = polarfactory(n);

M.name = @() sprintf('Intrinsic polar manifold IP(%d) SU', n);


M.dim = @() (n-1)*n;

M.inner = M_parent.inner;
    
M.norm = M_parent.norm;

M.dist = @(x, y) error('intrinsicpolarfactory.dist not implemented yet.');

M.typicaldist = @() sqrt(n*n-n);



% symm      = @(Z) .5*(Z+Z');
% orth_proj = @(U, Z) Z - U*symm(U'*Z);
function eta_S = symm(Z)
    eta_S = .5*(Z+Z');
end
function eta_U = orth_proj(U,Z)
    eta_U = Z - U*symm(U'*Z);
end

function Q = getQ(X,C)
    K = size(C,3);
    Q = 0;
    B = X.S*X.U;
    for k=1:K
        Q = Q + C(:,:,k)*B'*diag(diag(B*C(:,:,k)*B'));
    end
end
function dQ = getdQ(X,eta,C)
    K = size(C,3);
    dQ = 0;
    B  = X.S*X.U;
    dB = eta.S*X.U + X.S*eta.U;
    for k=1:K
        dQ = dQ + C(:,:,k)*(dB'*diag(diag(B*C(:,:,k)*B'))...
            + 2*B'*(diag(diag(dB*C(:,:,k)*B'))));
    end
end

function D = solveDiagEq(X,Z,Q)
    Q1 = Q'*X.U'*X.S*X.U*Q;
    Q2 = Q'*Q;
    D  = 2*diag((X.S.*Q1+(X.S*X.S).*Q2)\diag((Z.S*X.U + X.S*Z.U)*Q));
end



M.proj = @projection;
function eta = projection(X, Z)
    % project on parent first
    eta = M_parent.proj(X,Z);
    % project on intrinsic tangent
    Q = getQ(X,C);
    D = solveDiagEq(X,eta,Q);
    eta.S = eta.S - X.S*symm(X.U*Q*D)*X.S;
    eta.U = eta.U - orth_proj(X.U,X.S*D*Q');
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    % get gradient on parent first
    rgrad = M_parent.egrad2rgrad(X,egrad);
    % project on tangent
    rgrad = projection(X, rgrad);
end

M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(X, egrad, ehess, eta)
    % get gradient and hessian on parent first
    rgrad = M_parent.egrad2rgrad(X,egrad);
    rhess = M_parent.ehess2rhess(X,egrad,ehess,eta);
    % correct hessian
    Q  = getQ(X,C);
    dQ = getdQ(X,eta,C);
    D  = solveDiagEq(X,rgrad,Q);
    rhess.S = rhess.S - X.S*symm((eta.U*Q + X.U*dQ)*D)*X.S - symm(eta.S*symm(X.U*Q*D)*X.S);
    rhess.U = rhess.U - orth_proj(X.U,eta.S*D*Q' + X.S*D*dQ' - eta.U*symm(X.U'*X.S*D*Q'));
    % project on tangent
    rhess   = projection(X, rhess);
end

M.retr = @retraction;
function Y = retraction(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % retraction on parent
    Y = M_parent.retr(X,eta,t);
    % project on manifold
    if ~(rcond(Y.S)<1e-10 || isnan(rcond(Y.S)))
        Y = projonman(Y);
    end
    
    % some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(Y.S)<1e-10 || isnan(rcond(Y.S))
        t = t/2;
        Y = M_parent.retr(X,eta,t);
        if ~(rcond(Y.S)<1e-10 || isnan(rcond(Y.S)))
            Y = projonman(Y);
        end
    end 
end

M.exp = @exponential;
function Y = exponential(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    Y = retraction(X, eta, t);
    warning('intrinsicpolarfactory:exp', ...
        ['Exponential for intrinsic polar ' ...
        'manifold not implemented yet. Used retraction instead.']);
end

M.hash = M_parent.hash;

M.rand = @random;
function X = random()
    % get random matrix on parent
    X = M_parent.rand();
    % project on manifold
    X = projonman(X);
end

M.randvec = @randomvec;
function eta = randomvec(X)
    % random vector on the tangent space
    eta.S = randn(n);
    eta.U = randn(n);
    eta = projection(X, eta);
    nrm = M_parent.norm(X, eta);
    eta.S = eta.S / nrm;
    eta.U = eta.U / nrm;
end

M.lincomb = M_parent.lincomb;

M.zerovec = M_parent.zerovec;

M.transp = @(x1, x2, d) projection(x2, d);

% vec and mat are not isometries, because of the scaled inner metric
M.vec = M_parent.vec;
M.mat = M_parent.mat;
M.vecmatareisometries = @() false;



M.GL2man         = @(x) projonman(GL2polar(x));
M.man2GL         = @(X) X.S*X.U;
M.fun2opt_GL2man = @fun2opt_GL2polar;


function Y = projonman(X)
    K = size(C,3);
    B = X.S*X.U;
    tmp = 0;
    for k=1:K
        tmp = tmp + diag(B*C(:,:,k)*B').^2;
    end
    Y = GL2polar(diag(tmp.^(-1/4))*B);
end

end