function M = polarfactory(n)


M.name = @() sprintf('Polar manifold P(%d)', n);

M.dim = @() n*n;

M.inner = @(X, eta, zeta) trace( (X.S\eta.S) * (X.S\zeta.S) )...
    +  eta.U(:).'*zeta.U(:);

M.norm = @(X, eta) sqrt(M.inner(X, eta, eta));

M.dist = @(x, y) error('polarfactory.dist not implemented yet.');

M.typicaldist = @() n;


% symm      = @(Z) .5*(Z+Z');
% orth_proj = @(U, Z) Z - U*symm(U'*Z);
function eta_S = symm(Z)
    eta_S = .5*(Z+Z');
end
function eta_U = orth_proj(U,Z)
    eta_U = Z - U*symm(U'*Z);
end
% orthogonalization
function A = uf(A)
    [L,~,R] = svd(A, 0);
    A = L*R';
end

M.proj = @projection;
function eta = projection(X, Z)
    eta.S = symm(Z.S);
    eta.U = orth_proj(X.U, Z.U);
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    rgrad.S = X.S*symm(egrad.S)*X.S;
    rgrad.U = orth_proj(X.U, egrad.U);
end

M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(X, egrad, ehess, eta)
    rhess.S = X.S*symm(ehess.S)*X.S  + symm(eta.S*symm(egrad.S)*X.S);
    rhess.U = ehess.U - eta.U*symm(egrad.U'*X.U);
    rhess.U = orth_proj(X.U,rhess.U);
end

M.retr = @retraction;
function Y = retraction(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % we choose to use the exponential here, for alternative retractions,
    % see for example sympositivedefinitefactory.m and stiefelfactory.m
    Y = exponential(X, eta, t);
end

M.exp = @exponential;
function Y = exponential(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % The symm() and real() calls are mathematically not necessary but
    % are numerically necessary.
    Y.S = symm(X.S*real(expm(X.S\(t*eta.S))));
    % From a formula by Ross Lippert, Example 5.4.2 in AMS08.
    tetaU = t*eta.U;
    Y.U = real([X.U tetaU] * ...
          expm([X.U'*tetaU , -tetaU'*tetaU ; eye(n) , X.U'*tetaU]) * ...
          [ expm(-X.U'*tetaU) ; zeros(n) ]);
%    Y.U = uf(Y.U); % numerical security, needed ? to test
end

M.log = @(x, y) error('polarfactory.log not implemented yet.');

M.hash = @(X) ['z' hashmd5([X.S(:) ; X.U(:)])];

M.rand = @random;
function X = random()
    X = GL2polar(randn(n));
end

M.randvec = @(X) randomvec(X,M.norm);
function eta = randomvec(X,normfun)
    % random vector on the tangent space
    eta.S = randn(n);
    eta.U = randn(n);
    eta = projection(X, eta);
    nrm = normfun(X, eta);
    eta.S = eta.S / nrm;
    eta.U = eta.U / nrm;
end

M.lincomb = @lincomb;
function d = lincomb(x, a1, d1, a2, d2)
    
    if nargin == 3
        d.S = a1*d1.S;
        d.U = a1*d1.U;
    elseif nargin == 5
        d.S = a1*d1.S + a2*d2.S;
        d.U = a1*d1.U + a2*d2.U;
    else
        error('Bad use of polarfactory.lincomb.');
    end
    
end

M.zerovec = @(X) struct('S', zeros(n), 'U', zeros(n));

M.transp = @(x1, x2, d) projection(x2, d);

% vec and mat are not isometries, because of the scaled inner metric
M.vec = @(X, Z) [Z.S(:); Z.U(:)];
M.mat = @(X, z) struct('S', reshape(z(1:(n*n)), n, n),...
                       'U', reshape(z((n*n+1):(2*n*n)), n, n));
M.vecmatareisometries = @() false;


M.GL2man         = @GL2polar;
M.man2GL         = @(X) X.S*X.U;
M.fun2opt_GL2man = @fun2opt_GL2polar;

end