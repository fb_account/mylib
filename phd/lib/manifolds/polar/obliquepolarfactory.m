function M = obliquepolarfactory(n)


% define polar manifold
M_parent = polarfactory(n);

M.name = @() sprintf('Oblique polar manifold OP(%d) SU', n);

M.dim = @() (n-1)*n;

M.inner = M_parent.inner;
    
M.norm = M_parent.norm;

M.dist = @(x, y) error('obliquepolarfactory.dist not implemented yet.');

M.typicaldist = @() sqrt(n*n-n);



% symm = @(Z) .5*(Z+Z');
function eta_S = symm(Z)
    eta_S = .5*(Z+Z');
end
% solveDiagEq = @(S,Q) 2*diag((S^3.*S+S^2.*S^2)\diag(Q));
function D = solveDiagEq(S,Z)
    D = 2*diag((S^3.*S+S^2.*S^2)\diag(Z*S));
end
% oblSPD_proj = @(S, Z) Z - symm(S^2*solveDiagEq(S,Z*S)*S);
function eta_S = oblSPD_proj(S,Z)
    eta_S = Z - S*symm(S*solveDiagEq(S,Z))*S;
end

M.proj = @projection;
function eta = projection(X, Z)
    % project on parent
    eta = M_parent.proj(X,Z);
    % correct the component S
    eta.S = oblSPD_proj(X.S,eta.S);
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    % get gradient on parent
    rgrad = M_parent.egrad2rgrad(X,egrad);
    % correct the component S
    rgrad.S = oblSPD_proj(X.S,rgrad.S);
end

M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(X, egrad, ehess, eta)
    % get gradient and hessian on parent
    rgrad = M_parent.egrad2rgrad(X,egrad);
    rhess = M_parent.ehess2rhess(X,egrad,ehess,eta);       
    % correct the component S
    D       = solveDiagEq(X.S,rgrad.S);
    tmp     = rhess.S - X.S*symm(eta.S*D)*X.S - symm(eta.S*symm(X.S*D)*X.S);
    rhess.S = oblSPD_proj(X.S,tmp);
end

M.retr = @retraction;
function Y = retraction(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    % retraction on parent
    Y = M_parent.retr(X,eta,t);
    % project on manifold
    if ~(rcond(Y.S)<1e-10 || isnan(rcond(Y.S)))
        Y = projonman(Y);
    end
    
    % some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(Y.S)<1e-10 || isnan(rcond(Y.S))
        t = t/2;
        Y = M_parent.retr(X,eta,t);
        if ~(rcond(Y.S)<1e-10 || isnan(rcond(Y.S)))
            Y = projonman(Y);
        end
    end
end

M.exp = @exponential;
function Y = exponential(X, eta, t)
    if nargin < 3
        t = 1.0;
    end
    Y = retraction(X, eta, t);
    warning('obliquepolarfactory:exp', ...
        ['Exponential for oblique polar ' ...
        'manifold not implemented yet. Used retraction instead.']);
end

M.hash = M_parent.hash;

M.rand = @random;
function X = random()
    % get random matrix on parent
    X = M_parent.rand();
    % project on manifold
    X = projonman(X);
end

M.randvec = @randomvec;
function eta = randomvec(X)
    % random vector on the tangent space
    eta.S = randn(n);
    eta.U = randn(n);
    eta = projection(X, eta);
    nrm = M_parent.norm(X, eta);
    eta.S = eta.S / nrm;
    eta.U = eta.U / nrm;
end

M.lincomb = M_parent.lincomb;

M.zerovec = M_parent.zerovec;

M.transp = @(x1, x2, d) projection(x2, d);

% vec and mat are not isometries, because of the scaled inner metric
M.vec = M_parent.vec;
M.mat = M_parent.mat;
M.vecmatareisometries = @() false;


M.GL2man         = @(x) projonman(GL2polar(x));
M.man2GL         = @(X) X.S*X.U;
M.fun2opt_GL2man = @fun2opt_GL2polar;


function Y = projonman(X)
    Y.S = sqrtm(diag(diag(X.S^2).^-.5)*X.S^2*diag(diag(X.S^2).^-.5));
    Y.U = X.U;
end

end