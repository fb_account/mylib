% some precision parameter
eps = 1e-6;
% size of manifolds
n = 8;

% dumb SPD matrices for testing
K = 5;
C = zeros(n,n,K);
for k=1:K
    tmp      = randn(n);
    C(:,:,k) = tmp*tmp';
end

% dumb cost function for testing
[GLcost, GLgrad, GLhess] = F_direct(C,ones(1,K));
[cost,egrad,ehess]       = fun2opt_GL2polar(GLcost,GLgrad,GLhess);

%% (A) polar manifold

P = polarfactory(n);

% (0.1) generate some point on P : P.rand
X     = P.rand();
[~,p] = chol(X.S);
if p~=0, disp('M.rand: X.S not SPD'); end
if norm((X.U*X.U'-eye(n)),'fro')>eps, disp('M.rand: X.U not orthogonal'); end
%
% (0.2) generate some tangent vector : P.randvec
Xi   = P.randvec(X);
% % generate point in normal space
% tmp1 = randn(n);
% tmp1 = .5*(tmp1-tmp1'); % take skew-symmetric part
% tmp2 = randn(n);
% tmp2 = .5*(tmp2+tmp2'); % take symmetric part
% tmp  = struct('S', tmp1, 'U', X.U*tmp2);
% if abs(P.inner(X,Xi,tmp))>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
if norm((Xi.S'-Xi.S),'fro')>eps, disp('M.randvec: S component of vector not symmetric'); end
tmp = X.U'*Xi.U; % should be skew-symmetric
if norm((tmp+tmp'),'fro')>eps, disp('M.randvec: U component of vector not X.U * skew-symmetric matrix'); end
%
% (1) P.proj
Z   = struct('S', randn(n), 'U', randn(n));
Xi  = P.proj(X,Z);
if abs(P.inner(X,Xi,struct('S', Z.S-Xi.S, 'U', Z.U-Xi.U)))>eps, disp(['M.proj: orthogonality issue' num2str(abs(P.inner(X,Xi,struct('S', Z.S-Xi.S, 'U', Z.U-Xi.U))))]); end
tmp = P.proj(X,Xi);
tmp = struct('S',tmp.S-Xi.S,'U',tmp.U-Xi.U);
if P.norm(X,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) P.retr
X     = P.rand();
Eta   = P.randvec(X);
t     = rand;
Y     = P.retr(X,struct('S',zeros(n),'U',zeros(n)));
if norm((X.S-Y.S),'fro')>eps || norm((X.U-Y.U),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
Y     = P.retr(X,Eta,t);
[~,p] = chol(Y.S);
if p~=0, disp('M.retr: Y.S not SPD'); end
if norm((Y.U*Y.U'-eye(n)),'fro')>eps, disp('M.retr: Y.U not orthogonal'); end
%
% (3) gradient and hessian
problem       = [];
problem.M     = P;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);

%% (B) oblique polar manifold


OP = obliquepolarfactory(n);

% (0.1) generate some point on OP : OP.rand
X     = OP.rand();
% (i) check X in P
[~,p] = chol(X.S);
if p~=0, disp('M.rand: X.S not SPD'); end
if norm((X.U*X.U'-eye(n)),'fro')>eps, disp('M.rand: X.U not orthogonal'); end
% (ii) check norm of the line of X.S
if norm(diag(X.S*X.S) - ones(n,1),'fro')>eps, disp('M.rand: norm lines of X.S different from one'); end
%
% (0.2) generate some tangent vector : OP.randvec
Xi   = OP.randvec(X);
% generate point in normal space
% tmp1 = X.S*diag(randn(n,1));
% tmp1 = .5*X.S*(tmp1+tmp1')*X.S;
% tmp2 = randn(n);
% tmp2 = .5*(tmp2+tmp2'); % symmetric part
% tmp  = struct('S', tmp1, 'U', X.U*tmp2);
% if abs(OP.inner(X,Xi,tmp))>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
if norm(diag(Xi.S*X.S),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) OP.proj
Z   = P.proj(X,struct('S', randn(n), 'U', randn(n)));
Xi  = OP.proj(X,Z);
if abs(OP.inner(X,Xi,struct('S', Z.S-Xi.S, 'U', Z.U-Xi.U)))>eps, disp(['M.proj: orthogonality issue ' num2str(abs(OP.inner(X,Xi,struct('S', Z.S-Xi.S, 'U', Z.U-Xi.U))))]); end
tmp = OP.proj(X,Xi);
tmp = struct('S',tmp.S-Xi.S,'U',tmp.U-Xi.U);
if OP.norm(X,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) OP.retr
X     = OP.rand();
Eta   = OP.randvec(X);
t     = rand;
Y     = OP.retr(X,struct('S',zeros(n),'U',zeros(n)));
if norm((X.S-Y.S),'fro')>eps || norm((X.U-Y.U),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
Y     = OP.retr(X,Eta,t);
% (i) check Y in P 
[~,p] = chol(Y.S);
if p~=0, disp('M.retr: Y.S not SPD'); end
if norm((Y.U*Y.U'-eye(n)),'fro')>eps, disp('M.retr: Y.U not orthogonal'); end
% (ii) check norm of the line of Y.S
if norm(diag(Y.S*Y.S) - ones(n,1),'fro')>eps, disp('M.retr: norm lines of Y.S different from one'); end
%
% (3) OP.projonman
X = P.rand();
Y = OP.projonman(X);
% (i) check Y in P 
[~,p] = chol(Y.S);
if p~=0, disp('M.projonman: Y.S not SPD'); end
if norm((Y.U*Y.U'-eye(n)),'fro')>eps, disp('M.projonman: Y.U not orthogonal'); end
% (ii) check norm of the line of Y.S
if norm(diag(Y.S*Y.S) - ones(n,1),'fro')>eps, disp('M.projonman: norm lines of Y.S different from one'); end
%
% (4) gradient and hessian
problem       = [];
problem.M     = OP;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);

%% (C) intrinsic polar manifold

IP = intrinsicpolarfactory(n,C);

% (0.1) generate some point on IP : IP.rand
X     = IP.rand();
% (i) check X in P
[~,p] = chol(X.S);
if p~=0, disp('M.rand: X.S not SPD'); end
if norm((X.U*X.U'-eye(n)),'fro')>eps, disp(['M.rand: X.U not orthogonal ' num2str(norm((X.U*X.U'-eye(n)),'fro'))]); disp(X.U*X.U'); end
% (ii) check norm of the line of X.S
B     = X.S*X.U;
tmp   = 0;
for k=1:K
    tmp = tmp + diag(B*C(:,:,k)*B').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.rand: norm lines of X.S different from expected'); end
%
% (0.2) generate some tangent vector : OP.randvec
Xi   = IP.randvec(X);
xi   = Xi.S*X.U + X.S*Xi.U;
Q    = 0;
for k=1:K
    Q = Q + C(:,:,k)*B'*diag(diag(B*C(:,:,k)*B'));
end
if norm(diag(xi*Q),'fro')>eps, disp('M.randvec: vector does not seem to be in the tangent space'); end
%
% (1) IP.proj
Z   = P.proj(X,struct('S', randn(n), 'U', randn(n)));
Xi  = IP.proj(X,Z);
if abs(IP.inner(X,Xi,struct('S', Z.S-Xi.S, 'U', Z.U-Xi.U)))>eps, disp(['M.proj: orthogonality issue ' num2str(abs(IP.inner(X,Xi,struct('S', Z.S-Xi.S, 'U', Z.U-Xi.U))))]); end
tmp = IP.proj(X,Xi);
tmp = struct('S',tmp.S-Xi.S,'U',tmp.U-Xi.U);
if IP.norm(X,tmp)>eps, disp('M.proj: does not appear to be a projector'); end
%
% (2) IP.retr
X     = IP.rand();
Eta   = IP.randvec(X);
t     = rand;
Y     = IP.retr(X,struct('S',zeros(n),'U',zeros(n)));
if norm((X.S-Y.S),'fro')>eps || norm((X.U-Y.U),'fro')>eps, disp('M.retr: retraction of null vector induces a movement on M'); end
Y     = IP.retr(X,Eta,t);
% (i) check Y in P 
[~,p] = chol(Y.S);
if p~=0, disp('M.retr: Y.S not SPD'); end
if norm((Y.U*Y.U'-eye(n)),'fro')>eps, disp('M.retr: Y.U not orthogonal'); end
% (ii) check norm of the line of Y.S
B     = Y.S*Y.U;
tmp   = 0;
for k=1:K
    tmp = tmp + diag(B*C(:,:,k)*B').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.retr: norm lines of Y.S*Y.U different from one'); end
%
% (3) IP.projonman
X = P.rand();
Y = IP.projonman(X);
% (i) check Y in P 
[~,p] = chol(Y.S);
if p~=0, disp('M.projonman: Y.S not SPD'); end
if norm((Y.U*Y.U'-eye(n)),'fro')>eps, disp('M.projonman: Y.U not orthogonal'); end
% (ii) check norm of the line of Y.S
B     = Y.S*Y.U;
tmp   = 0;
for k=1:K
    tmp = tmp + diag(B*C(:,:,k)*B').^2;
end
if norm(tmp - ones(n,1),'fro')>eps, disp('M.projonman: norm lines of Y.S*Y.U different from one'); end
%
% (4) gradient and hessian
problem       = [];
problem.M     = IP;
problem.cost  = cost;
problem.egrad = egrad;
problem.ehess = ehess;

figure;
checkgradient(problem);
figure;
checkhessian(problem);
