% ************************************************************************
% FB-EEG toolbox. Private release, do not share without the explicit agreement of the authors.
%
%
% *** History:         2016-12
% *** Contributors:    Florent Bouchard
% *** Contact:         florentbouchard@ymail.com

 p=mfilename('fullpath'); %find the path from this script
 p=p(1:end-10);%remove 'installer'
 addpath( genpath(p));
 disp('GP TOOLBOX successfully activated')
help installer
clear p

