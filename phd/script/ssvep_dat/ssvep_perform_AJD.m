folder     = '~/results_thesis/ssvep_dat/';
folder_mat = 'ssvep_dat_mat/';
folder_cos = 'data_for_AJD/';
%
subj       = ["01","02","03","04","05","06","07","08","09","10","11","12"];
nSubj      = length(subj);
sess       = {
    ["01","02"];                % 01
    ["01","02"];                % 02
    ["01","02"];                % 03
    ["01","02"];                % 04
    ["01","02"];                % 05
    ["01","02"];                % 06
    ["01","02","03"];           % 07
    ["01","02"];                % 08
    ["01","02"];                % 09
    ["01","02","03","04"];      % 10
    ["01","02"];                % 11
    ["01","02","03","04","05"]; % 12
    };


%% AJD algorithms

% common options to all algorithms
opt_common = @(C,w,manifold,GLfun2opt,optStrategy,optmethod) struct('B0',eye(size(C,1)), 'tol',1e-9, 'maxiter',1000, 'man_opt',struct('verbosity',0), ...
                                                                    'weights',w, 'manifold',manifold, 'GLfun2opt',GLfun2opt, 'optStrategy',optStrategy, 'optmethod',optmethod ...
                                                                   );
                                                                    
                                                                     
                                                                     
% all divergence AJD functions with direct strategy
identifier = 'all_div';
algo_names = { 'FiPa', 'KLliPa', ...
               'KLriPa', 'KLsiPa', 'alphaLDiPa', ...
               'RiPa', 'LEiPa', 'WiPa' ...
             };
nAlgo      = length(algo_names);
%
optStrategy = 'direct';
optmethod   = @rlbfgs;
%
algos    = cell(1,nAlgo);
algos{1} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@F_direct                    ,optStrategy,optmethod));
algos{2} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@KLl_direct                  ,optStrategy,optmethod));
algos{3} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@KLr_direct                  ,optStrategy,optmethod));
algos{4} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@KLs_direct                  ,optStrategy,optmethod));
algos{5} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@(C,w) alphaLD_direct(C,w,0) ,optStrategy,optmethod));
algos{6} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@R_direct                    ,optStrategy,optmethod));
algos{7} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@LE_direct                   ,optStrategy,optmethod));
algos{8} = @(C,w) RAJD(C,opt_common(C, w, intrinsicpolarfactory(size(C,1),C) ,@W_direct                    ,optStrategy,optmethod));


%% load data and perform AJD


for subjix=1:nSubj
    disp(['subj: ' int2str(subjix)]);
    subj_cur     = subj(subjix);
    sess_cur     = sess{subjix};
    nSess        = length(sess_cur);
    for sessix=1:nSess
        disp(['sess: ' int2str(sessix)]);
        % load data and put them in adequate structure for preprocessing
        % subject and session of interest
        load([folder 'data_for_AJD/C_subject' char(subj_cur) '_session' char(sess_cur(sessix))])
        % matrices and weights
        C = cat(3,C13,C26,C17,C34,C21,C42,C_b);
        n = size(C,1);
        K = size(C,3);
        w = cat(2,1/6,1/6,1/6,1/6,1/6,1/6,nd)*K/2;
        % whitening
        G = zeros(n);
        for k=1:K
            G = G + w(k)*C(:,:,k)/K;
        end
        L  = chol(G,'lower');
        Cw = zeros(n,n,K);
        for k=1:K
            Cw(:,:,k) = L\C(:,:,k)/L';
        end
        % AJD
        B_all = zeros(n,n,nAlgo);
        tic;
        parfor algoix=1:nAlgo
%         disp(['algo: ' algo_names{algoix}]);
            [Bw,info,options] = algos{algoix}(Cw,w);
            B = Bw/L;
            %
            B_all(:,:,algoix) = B;
            disp(['algo: ' algo_names{algoix}]);
        end
        toc;
        save([folder 'unmix/unmix_subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_' identifier],'B_all', 'algo_names', 'algos', 'opt_common');
    end
end