%% (0) parameters

folder     = '~/results_thesis/ssvep_dat/';
folder_mat = 'ssvep_dat_mat/';
%
subj       = ["01","02","03","04","05","06","07","08","09","10","11","12"];
nSubj      = length(subj);
sess       = {
    ["01","02"];                % 01
    ["01","02"];                % 02
    ["01","02"];                % 03
    ["01","02"];                % 04
    ["01","02"];                % 05
    ["01","02"];                % 06
    ["01","02","03"];           % 07
    ["01","02"];                % 08
    ["01","02"];                % 09
    ["01","02","03","04"];      % 10
    ["01","02"];                % 11
    ["01","02","03","04","05"]; % 12
    };


identifier = 'all_div';

%% selected subj/sess

%
subj       = ["03","12"];
nSubj      = length(subj);
sess       = {
    ["02"]; % 03
    ["01","05"]; % 12
    };


%%
ssvep_freqs = [13,17,21];
fmax        = 256;                              % max frequency (Hz)
fres        = 0.5;                              % frequency resolution (Hz)
LB          = 1;                                % low cut-off freq (Hz)
HB          = 43;                               % hight cut-off freq (Hz)
FILT_param  = [LB,HB, 4, NaN];                  % [LB, HB, order, decimation(not yet computed)]
event_code  = [33024,33025,33027,33026];        % resting state,  13Hz, 17Hz, 21Hz
event_table = {'13 Hz','17 Hz','21 Hz'};
L_epoch     = 3;                                % length windows in seconds
L_welch     = 2;                                % length welch estimator windows in seconds
l_welch     = 1.75;                             % length welch estimator step in seconds
freqs       = [LB:fres:HB];                     % freqs for welch estimator
offset      = 2.5;                              % WARNING : using 2.5seconds for experiment specific sync for event_pos


%% (1) figure :  spectrum of sources for the 3 conditions (13Hz, 17Hz, 21Hz) at the electrode level

for subjix=1:nSubj
    subj_cur     = subj(subjix);
    sess_cur     = sess{subjix};
    nSess        = length(sess_cur);
    for sessix=1:nSess
        % load data
        load([folder folder_mat 'subject' char(subj_cur) '_session' char(sess_cur(sessix))]);
        % build triggers
        event_pos           = event_pos(ismember(event_type,event_code));    % keep only useful event_pos
        event_type          = event_type(ismember(event_type,event_code));   % keep only useful event_type
        triggers            = zeros(size(raw,2),1);
        triggers(event_pos) = 1;
        % filter data
        FILT_param(4)       = round(fs/fmax);                                % compute decimation factor
        [xfilt, xfs, xtrig] = preprocessingEEG(raw',fs,FILT_param,triggers); % band-pass filt
        X                   = xfilt';
        % load unmixing matrices
        load([folder 'unmix/unmix_subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_' identifier]);
        nAlgo = length(algo_names);
        for aix=1:nAlgo
            B   = B_all(:,:,aix);
            % compute sources and epoch them (WARNING : OFFSET IS SET TO 2.5 seconds due to experiment-specific triggers' sync)
            S   = B*X;
            Sk  = epoch_p300(S,xtrig,L_epoch*xfs,xfs*offset);
            % compute power spectral density with welch-estimator 
            Sfk = zeros(size(Sk,1),length(freqs),size(Sk,3));
            for k=1:size(Sk,3)
                Sfk(:,:,k) = pwelch(Sk(:,:,k)',L_welch*xfs,l_welch*xfs,freqs,xfs)';
            end
            % figure !
            figure;
            for six=1:size(Sfk,1)
                subplot(4,2,six);
                hold all;
                plot(freqs, mean(Sfk(six,:,event_type==33025),3)', 'linewidth',2);
                plot(freqs, mean(Sfk(six,:,event_type==33027),3)', 'linewidth',2);
                plot(freqs, mean(Sfk(six,:,event_type==33026),3)', 'linewidth',2);
%                 legend('13','17','21');
                max_13 = max(mean(Sfk(six,:,event_type==33025),3));
                max_17 = max(mean(Sfk(six,:,event_type==33027),3));
                max_21 = max(mean(Sfk(six,:,event_type==33026),3));
                max_val = max([max_13,max_17,max_21]);
                line([13 13], [0 max_val],'Color','b','LineStyle','-');
                line([26 26], [0 max_val],'Color','b','LineStyle','--');
                line([17 17], [0 max_val],'Color','r','LineStyle','-');
                line([34 34], [0 max_val],'Color','r','LineStyle','--');
                line([21 21], [0 max_val],'Color','y','LineStyle','-');
                line([42 42], [0 max_val],'Color','y','LineStyle','--');
            end
            %
            print(gcf,[folder 'figures/sources/psd/' 'subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_algo_' algo_names{aix}],'-dpng','-r300')
            close all;
        end
    end
end

%% (2) retroprojections and stuffs

for subjix=3%:nSubj
    subj_cur     = subj(subjix);
    sess_cur     = sess{subjix};
    nSess        = length(sess_cur);
    for sessix=3%:nSess
        % load data
        load([folder folder_mat 'subject' char(subj_cur) '_session' char(sess_cur(sessix))]);
        % build triggers
        event_pos           = event_pos(ismember(event_type,event_code));    % keep only useful event_pos
        event_type          = event_type(ismember(event_type,event_code));   % keep only useful event_type
        triggers            = zeros(size(raw,2),1);
        triggers(event_pos) = 1;
        % filter data
        FILT_param(4)       = round(fs/fmax);                                % compute decimation factor
        [xfilt, xfs, xtrig] = preprocessingEEG(raw',fs,FILT_param,triggers); % band-pass filt
        X                   = xfilt';
        % load unmixing matrices
        load([folder 'unmix/unmix_subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_' identifier]);
        nAlgo = length(algo_names);
        for aix=1:nAlgo
            B  = B_all(:,:,aix);
            % compute sources and epoch them (WARNING : OFFSET IS SET TO 2.5 seconds due to experiment-specific triggers' sync)
            S  = B*X;
            Sk = epoch_p300(S,xtrig,L_epoch*xfs,xfs*offset);
            % Retroprojection of sources in electrodes domain (to avoid scaling issue)
            A         = pinv(B); %estimated mixing matrix
            Xtilde    = zeros([size(X),size(A,2)]); % backprojected sources [N x Ttot x P] (P: nb sources)
            Xtilde_k  = zeros(size(X,1), size(Sk,2), size(Sk,3), size(A,2)); % backprojected epochs [N x T x K x P]
            Xtilde_fk = zeros(size(X,1),length(freqs),size(Sk,3),size(A,2)); % backprojected spectrum [N x F x K x P]
            for six=1:size(A,2)
                Atilde                 = A;
                Atilde(:,1:end ~= six) = 0; % six-th source only
                Xtilde(:,:,six)        = Atilde*S;
                Xtilde_k(:,:,:,six)    = epoch_p300(Xtilde(:,:,six),xtrig,L_epoch*xfs,xfs*offset);
                for k=1:size(Xtilde_k,3)
                    Xtilde_fk(:,:,k,six) = pwelch(Xtilde_k(:,:,k,six)',L_welch*xfs,l_welch*xfs,freqs,xfs)';
                end
            end
%             % figure !
%             figure;
%             for six=1:size(A,2)
%                 subplot(4,2,six);
%                 hold all;
%                 plot(freqs, mean(mean(Xtilde_fk(:,:,event_type==33025,six),3),1)', 'linewidth',2);
%                 plot(freqs, mean(mean(Xtilde_fk(:,:,event_type==33027,six),3),1)', 'linewidth',2);
%                 plot(freqs, mean(mean(Xtilde_fk(:,:,event_type==33026,six),3),1)', 'linewidth',2);
% %                 legend('13','17','21');
%                 max_13 = max(mean(mean(Xtilde_fk(:,:,event_type==33025,six),3),1));
%                 max_17 = max(mean(mean(Xtilde_fk(:,:,event_type==33027,six),3),1));
%                 max_21 = max(mean(mean(Xtilde_fk(:,:,event_type==33026,six),3),1));
%                 max_val = max([max_13,max_17,max_21]);
%                 line([13 13], [0 max_val],'Color','b','LineStyle','-');
%                 line([26 26], [0 max_val],'Color','b','LineStyle','--');
%                 line([17 17], [0 max_val],'Color','r','LineStyle','-');
%                 line([34 34], [0 max_val],'Color','r','LineStyle','--');
%                 line([21 21], [0 max_val],'Color','y','LineStyle','-');
%                 line([42 42], [0 max_val],'Color','y','LineStyle','--');
%             end
%             %
%             print(gcf,[folder 'figures/sources/psd_retroproj/' 'subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_algo_' algo_names{aix}],'-dpng','-r300')
%             close all;
            
            % some score
            for i=1:size(Xtilde_fk,4)
                [scoreA(:,i),scoreB(:,:,i)]=frequency_ratio(Xtilde_fk(:,:,:,i));
                scoreB_mean(:,i)=mean(scoreB(:,:,i),2);
            end
%             % figure !
%             figure;
%             for six=1:size(A,2)
%                 subplot(4,2,six);
%                 hold all;
%                 plot(freqs, mean(scoreB(:,event_type==33025,six),2), 'linewidth',2);
%                 plot(freqs, mean(scoreB(:,event_type==33027,six),2), 'linewidth',2);
%                 plot(freqs, mean(scoreB(:,event_type==33026,six),2), 'linewidth',2);
%                 %
%                 max_13 = max(mean(scoreB(:,event_type==33025,six),2));
%                 max_17 = max(mean(scoreB(:,event_type==33027,six),2));
%                 max_21 = max(mean(scoreB(:,event_type==33026,six),2));
%                 max_val = max([max_13,max_17,max_21]);
%                 line([13 13], [0 max_val],'Color','b','LineStyle','-');
%                 line([26 26], [0 max_val],'Color','b','LineStyle','--');
%                 line([17 17], [0 max_val],'Color','r','LineStyle','-');
%                 line([34 34], [0 max_val],'Color','r','LineStyle','--');
%                 line([21 21], [0 max_val],'Color','y','LineStyle','-');
%                 line([42 42], [0 max_val],'Color','y','LineStyle','--');
%             end
%             print(gcf,[folder 'figures/sources/ratio_freq/' 'subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_algo_' algo_names{aix}],'-dpng','-r300')
%             close all;

            figure;
            for indA=1:size(A,2)
                ratio13(indA)= sum(mean(scoreB(ismember(freqs, [13,26]),event_type==33025,indA),2));%13 Hz
                ratio17(indA)= sum(mean(scoreB(ismember(freqs, [17,34]),event_type==33027,indA),2)); %17 Hz
                ratio21(indA)= sum(mean(scoreB(ismember(freqs, [21,42]),event_type==33026,indA),2)); %21 Hz
            end
            ratio13_best(aix) = max(ratio13);
            ratio17_best(aix) = max(ratio17);
            ratio21_best(aix) = max(ratio21);
            subplot(131);plot(sort(ratio13,'descend'),'b*-');title(event_table(1));ylim([0 1])
            subplot(132);plot(sort(ratio17,'descend'),'r*-');title(event_table(2));ylim([0 1])
            subplot(133);plot(sort(ratio21,'descend'),'g*-');title(event_table(3));ylim([0 1])
%             print(gcf,[folder 'figures/sources/ratio_order/' 'subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_algo_' algo_names{aix}],'-dpng','-r300')
%             close all;
        end
    end
end

%% figures for latex

for subjix=1:nSubj
    subj_cur     = subj(subjix);
    sess_cur     = sess{subjix};
    nSess        = length(sess_cur);
    for sessix=1:nSess
        % load data
        load([folder folder_mat 'subject' char(subj_cur) '_session' char(sess_cur(sessix))]);
        % build triggers
        event_pos           = event_pos(ismember(event_type,event_code));    % keep only useful event_pos
        event_type          = event_type(ismember(event_type,event_code));   % keep only useful event_type
        triggers            = zeros(size(raw,2),1);
        triggers(event_pos) = 1;
        % filter data
        FILT_param(4)       = round(fs/fmax);                                % compute decimation factor
        [xfilt, xfs, xtrig] = preprocessingEEG(raw',fs,FILT_param,triggers); % band-pass filt
        X                   = xfilt';
        % load unmixing matrices
        load([folder 'unmix/unmix_subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_' identifier]);
        nAlgo = length(algo_names);
        for aix=1:nAlgo
            B  = B_all(:,:,aix);
            % compute sources and epoch them (WARNING : OFFSET IS SET TO 2.5 seconds due to experiment-specific triggers' sync)
            S  = B*X;
            Sk = epoch_p300(S,xtrig,L_epoch*xfs,xfs*offset);
            % Retroprojection of sources in electrodes domain (to avoid scaling issue)
            A         = pinv(B); %estimated mixing matrix
            Xtilde    = zeros([size(X),size(A,2)]); % backprojected sources [N x Ttot x P] (P: nb sources)
            Xtilde_k  = zeros(size(X,1), size(Sk,2), size(Sk,3), size(A,2)); % backprojected epochs [N x T x K x P]
            Xtilde_fk = zeros(size(X,1),length(freqs),size(Sk,3),size(A,2)); % backprojected spectrum [N x F x K x P]
%             % mean spectrum for each source for target frequencies
%             Sfrest_mean = zeros(size(A,2),length(freqs));
%             Sf13_mean   = zeros(size(A,2),length(freqs));
%             Sf17_mean   = zeros(size(A,2),length(freqs));
%             Sf21_mean   = zeros(size(A,2),length(freqs));
            for six=1:size(A,2)
                Atilde                 = A;
                Atilde(:,1:end ~= six) = 0; % six-th source only
                Xtilde(:,:,six)        = Atilde*S;
                Xtilde_k(:,:,:,six)    = epoch_p300(Xtilde(:,:,six),xtrig,L_epoch*xfs,xfs*offset);
                for k=1:size(Xtilde_k,3)
                    Xtilde_fk(:,:,k,six) = pwelch(Xtilde_k(:,:,k,six)',L_welch*xfs,l_welch*xfs,freqs,xfs)';
                end
%                 % mean for each source
%                 Sfrest_mean(six,:) = mean(mean(Xtilde_fk(:,:,event_type==33024,six),3),1);
%                 Sf13_mean(six,:)   = mean(mean(Xtilde_fk(:,:,event_type==33025,six),3),1);
%                 Sf17_mean(six,:)   = mean(mean(Xtilde_fk(:,:,event_type==33027,six),3),1);
%                 Sf21_mean(six,:)   = mean(mean(Xtilde_fk(:,:,event_type==33026,six),3),1);
            end
%             % sort sources
%             tmp = Sf13_mean(:,ismember(freqs,13)) + Sf13_mean(:,ismember(freqs,26)) ...
%                 + Sf17_mean(:,ismember(freqs,17)) + Sf17_mean(:,ismember(freqs,34)) ...
%                 + Sf21_mean(:,ismember(freqs,21)) + Sf21_mean(:,ismember(freqs,42)) ...
%                 + (max(Sf13_mean,[],2) + max(Sf17_mean,[],2) + max(Sf21_mean,[],2))/3;
% %             tmp = max([mean(Sf13_mean,2), mean(Sf17_mean,2), mean(Sf21_mean,2)],[],2); 
% 
%             [~,i] = sort(tmp,'descend');
%             Sfrest_mean = Sfrest_mean(i,:);
%             Sf13_mean   = Sf13_mean(i,:);
%             Sf17_mean   = Sf17_mean(i,:);
%             Sf21_mean   = Sf21_mean(i,:);
%             % parameters for text file
%             nChan      = size(S,1);
%             nF         = length(freqs);
%             all_s      = strings(1+nF,1+4*nChan);
%             all_s(1,1) = "f";
%             tmp        = ["s1","s2","s3","s4","s5","s6","s7","s8"];
%             for cix=1:nChan
%                 all_s(1,1+cix)         = join([tmp(cix),"rest"],"_");
%                 all_s(1,1+nChan+cix)   = join([tmp(cix),"13"],"_");
%                 all_s(1,1+2*nChan+cix) = join([tmp(cix),"17"],"_");
%                 all_s(1,1+3*nChan+cix) = join([tmp(cix),"21"],"_");
%             end
%             all_s(2:1+nF,1)                   = string(freqs);
%             all_s(2:1+nF,2:1+nChan)           = string(Sfrest_mean');
%             all_s(2:1+nF,2+nChan:1+2*nChan)   = string(Sf13_mean');
%             all_s(2:1+nF,2+2*nChan:1+3*nChan) = string(Sf17_mean');
%             all_s(2:1+nF,2+3*nChan:1+4*nChan) = string(Sf21_mean');
%             % write in file
%             filename = ['psd_src_' 'subject' char(subj_cur) '_session' char(sess_cur(sessix)) '_' algo_names{aix} '.txt'];
%             fileID   = fopen([folder 'fig_txt/src/' filename],'w','native','UTF-8');
%             %
%             [nLine,nCol] = size(all_s);
%             for lix=1:nLine
%                 for cix=1:nCol-1
%                     fprintf(fileID,'%s,',all_s(lix,cix));
%                 end
%                 % last column, no separator
%                 cix = nCol;
%                 fprintf(fileID,'%s',all_s(lix,cix));
%                 fprintf(fileID,'\n');
%             end
%             fclose(fileID);
            
            % some score
            for i=1:size(Xtilde_fk,4)
                [scoreA(:,i),scoreB(:,:,i)]=frequency_ratio(Xtilde_fk(:,:,:,i));
                scoreB_mean(:,i)=mean(scoreB(:,:,i),2);
            end
            for indA=1:size(A,2)
                score13(indA)= sum(mean(scoreB(ismember(freqs, [13,26]),event_type==33025,indA),2));%13 Hz
                score17(indA)= sum(mean(scoreB(ismember(freqs, [17,34]),event_type==33027,indA),2)); %17 Hz
                score21(indA)= sum(mean(scoreB(ismember(freqs, [21,42]),event_type==33026,indA),2)); %21 Hz
            end
            score13_best(aix) = max(score13);
            score17_best(aix) = max(score17);
            score21_best(aix) = max(score21);
        end
        % parameters for text file with scores
        all_s = strings(1+3,1+nAlgo);
        all_s(1,1) = "cond";
        all_s(1,2:end) = string(algo_names);
        all_s(2:1+3,1) = string(1:3);
        all_s(2,2:1+nAlgo) = string(score13_best);
        all_s(3,2:1+nAlgo) = string(score17_best);
        all_s(4,2:1+nAlgo) = string(score21_best);
        % write in file
        filename = ['scores_src_' 'subject' char(subj_cur) '_session' char(sess_cur(sessix)) '.txt'];
        fileID   = fopen([folder 'fig_txt/scores/' filename],'w','native','UTF-8');
        %
        [nLine,nCol] = size(all_s);
        for lix=1:nLine
            for cix=1:nCol-1
                fprintf(fileID,'%s,',all_s(lix,cix));
            end
            % last column, no separator
            cix = nCol;
            fprintf(fileID,'%s',all_s(lix,cix));
            fprintf(fileID,'\n');
        end
        fclose(fileID);
    end
end
