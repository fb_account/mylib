%% parameters

folder     = '~/results_thesis/ssvep_dat/';
folder_mat = 'ssvep_dat_mat/';
folder_cos = 'data_for_AJD/';
%
subj       = ["01","02","03","04","05","06","07","08","09","10","11","12"];
nSubj      = length(subj);
sess       = {
    ["01","02"];                % 01
    ["01","02"];                % 02
    ["01","02"];                % 03
    ["01","02"];                % 04
    ["01","02"];                % 05
    ["01","02"];                % 06
    ["01","02","03"];           % 07
    ["01","02"];                % 08
    ["01","02"];                % 09
    ["01","02","03","04"];      % 10
    ["01","02"];                % 11
    ["01","02","03","04","05"]; % 12
    };

%% main stuffs

for subjix=1:nSubj
    subj_cur     = subj(subjix);
    sess_cur     = sess{subjix};
    nSess        = length(sess_cur);
    for sessix=1:nSess
        %% (0) load data and put them in adequate structure for preprocessing
        load([folder folder_mat 'subject' char(subj_cur) '_session' char(sess_cur(sessix))]);
        nsample = size(raw,2);
        nchan   = size(label,1);
        % some more parameters for preprocessing
        fmax        = 256;                        % max frequency (Hz)
        fres        = 1;                          % frequency resolution (Hz)
        LB          = 1;                          % low cut-off freq (Hz)
        HB          = 43;                         % hight cut-off freq (Hz)
        FILT_param  = [LB,HB, 4, round(fs/fmax)]; % [LB, HB, order, decimation factor]
        event_code  = [33024,33025,33027,33026];  % resting state,  13Hz, 17Hz, 21Hz
        % build triggers
        event_pos           = event_pos(ismember(event_type,event_code));  % keep only useful event_pos
        event_type          = event_type(ismember(event_type,event_code)); % keep only useful event_type
        triggers            = zeros(size(raw,2),1);
        triggers(event_pos) = 1;
        % filter data
        [xfilt, xfs, xtrig] = preprocessingEEG(raw',fs,FILT_param,triggers); % band-pass filt
        X                   = xfilt';
        % epoching (WARNING : OFFSET IS SET TO 2.5 seconds due to experiment-specific triggers' sync)
        L_epoch = 3; % length windows in seconds
        offset  = 2.5;
        Xk      = epoch_p300(X,xtrig,L_epoch*xfs,xfs*offset);
        nEpoch  = size(Xk,3);


        %% (2) compute matrices to diagonalize + weights

        % "background" eeg cospectra: cospectra between 1-43 Hz, 1 Hz resolution computed over all epochs
        % - for (13,26), (17,34) and (21,42) Hz, we don't take the epochs from the corresponding classes 13, 17 and 21
        %
        % for each trial, cospectra computed with sliding window technique + mean over every trials considered
        %
        % "target" eeg cospectra: cospectra at (13,26), (17,34) and (21,42) Hz computed with the epochs of the corresponding classes

        win_size = L_epoch*xfs;
        nwindow  = xfs; % number of sample for a window -> 1 s
        noverlap = round(0.75*xfs);  % overlapping -> 75%
        nbwin    = floor((win_size - noverlap)/(nwindow - noverlap)); % number of windows in a trial

        nF    = HB-LB+1;
        C_b   = zeros(nchan,nchan,nF);
        C13   = zeros(nchan);
        C26   = zeros(nchan);
        C17   = zeros(nchan);
        C34   = zeros(nchan);
        C21   = zeros(nchan);
        C42   = zeros(nchan);

        for eix=1:nEpoch
            % get epoch and class
            X_tmp  = Xk(:,:,eix)';
            eclass = event_type(eix);
            data_e = zeros(nwindow,nchan,nbwin);
            for w=1:nbwin
                e             = win_size - (w-1)*nwindow + (w-1)*noverlap; % we fill X_tmp_e from the end to the start
                s             = e - nwindow + 1;
                data_e(:,:,w) = X_tmp(s:e,:);
            end
            % apply hanning appodizing windows
            data_w = zeros(size(data_e));
            win    = repmat(hanning(nwindow),[1,nchan]);
            for w=1:nbwin
                data_w(:,:,w) = win.*data_e(:,:,w);
            end
            % compute fft
            data_ft = fft(data_w,nwindow);
            % select frequencies of interest -> 1-43Hz, 1Hz resolution
            data_ft = data_ft(1:43,:,:); % hardcoded -> ugly, change !

            % compute cospectra
            for w=1:nbwin
                for f=1:nF
                    if f==13 && eclass==33025
                        C13 = C13 + real(data_ft(f,:,w)'*data_ft(f,:,w));
                    elseif f==26 && eclass==33025
                        C26 = C26 + real(data_ft(f,:,w)'*data_ft(f,:,w));
                    elseif f==17 && eclass==33027
                        C17 = C17 + real(data_ft(f,:,w)'*data_ft(f,:,w));
                    elseif f==34 && eclass==33027
                        C34 = C34 + real(data_ft(f,:,w)'*data_ft(f,:,w));
                    elseif f==21 && eclass==33026
                        C21 = C21 + real(data_ft(f,:,w)'*data_ft(f,:,w));
                    elseif f==42 && eclass==33026
                        C42 = C42 + real(data_ft(f,:,w)'*data_ft(f,:,w));
                    else
                        C_b(:,:,f) = C_b(:,:,f) + real(data_ft(f,:,w)'*data_ft(f,:,w));
                    end
                end
            end
        end

        % normalize the trace and get non-diag measure for
        nd = ones(1,nF);
        for f=1:nF
            C_b(:,:,f) = C_b(:,:,f)/trace(C_b(:,:,f));
            tmp        = C_b(:,:,f)-diag(diag(C_b(:,:,f)));
            nd(f)      = trace(tmp*tmp')/(diag(C_b(:,:,f))'*diag(C_b(:,:,f)))/(nchan-1);
        end
        nd  = nd/sum(nd);
        C13 = C13/trace(C13);
        C26 = C26/trace(C26);
        C17 = C17/trace(C17);
        C34 = C34/trace(C34);
        C21 = C21/trace(C21);
        C42 = C42/trace(C42);

        %% (3) save that shit !!
        save([folder 'data_for_AJD/C_subject' char(subj_cur) '_session' char(sess_cur(sessix))],'C_b','nd','C13','C26','C17','C34','C21','C42');
        
    end
end