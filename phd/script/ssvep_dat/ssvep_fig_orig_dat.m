%% (0) parameters

folder     = '~/results_thesis/ssvep_dat/';
folder_mat = 'ssvep_dat_mat/';
%
subj       = ["01","02","03","04","05","06","07","08","09","10","11","12"];
nSubj      = length(subj);
sess       = {
    ["01","02"];                % 01
    ["01","02"];                % 02
    ["01","02"];                % 03
    ["01","02"];                % 04
    ["01","02"];                % 05
    ["01","02"];                % 06
    ["01","02","03"];           % 07
    ["01","02"];                % 08
    ["01","02"];                % 09
    ["01","02","03","04"];      % 10
    ["01","02"];                % 11
    ["01","02","03","04","05"]; % 12
    };


%% selected subj/sess

%
subj       = ["03","12"];
nSubj      = length(subj);
sess       = {
    ["02"]; % 03
    ["01","05"]; % 12
    };


%%
ssvep_freqs = [13,17,21];
fmax        = 256;                              % max frequency (Hz)
fres        = 0.5;                              % frequency resolution (Hz)
LB          = 1;                                % low cut-off freq (Hz)
HB          = 43;                               % hight cut-off freq (Hz)
FILT_param  = [LB,HB, 4, NaN];                  % [LB, HB, order, decimation(not yet computed)]
event_code  = [33024,33025,33027,33026];        % resting state,  13Hz, 17Hz, 21Hz
event_table = {'13 Hz','17 Hz','21 Hz'};
L_epoch     = 3;                                % length windows in seconds
L_welch     = 2;                                % length welch estimator windows in seconds
l_welch     = 1.75;                             % length welch estimator step in seconds
freqs       = [LB:fres:HB];                     % freqs for welch estimator
offset      = 2.5;                              % WARNING : using 2.5seconds for experiment specific sync for event_pos


%% (1) figure :  spectrum of data for the 3 conditions (13Hz, 17Hz, 21Hz) at the electrode level

for subjix=1:nSubj
    subj_cur     = subj(subjix);
    sess_cur     = sess{subjix};
    nSess        = length(sess_cur);
    for sessix=1:nSess
        % load data
        load([folder folder_mat 'subject' char(subj_cur) '_session' char(sess_cur(sessix))]);
        % build triggers
        event_pos           = event_pos(ismember(event_type,event_code));    % keep only useful event_pos
        event_type          = event_type(ismember(event_type,event_code));   % keep only useful event_type
        triggers            = zeros(size(raw,2),1);
        triggers(event_pos) = 1;
        % filter data
        FILT_param(4)       = round(fs/fmax);                                % compute decimation factor
        [xfilt, xfs, xtrig] = preprocessingEEG(raw',fs,FILT_param,triggers); % band-pass filt
        X                   = xfilt';
        % epoching (WARNING : OFFSET IS SET TO 2.5 seconds due to experiment-specific triggers' sync)
        Xk = epoch_p300(X,xtrig,L_epoch*xfs,xfs*offset);
        % compute power spectral density with welch-estimator
        Xfk=zeros(size(Xk,1),length(freqs),size(Xk,3));
        for k=1:size(Xk,3)
            Xfk(:,:,k)=pwelch(Xk(:,:,k)',L_welch*xfs,l_welch*xfs,freqs,xfs)';
        end
%         f=figure;
%         hold all;
%         plot(freqs, mean(mean(Xfk(:,:,event_type==33025),3),1)', 'linewidth',2, 'DisplayName','13Hz');
%         plot(freqs, mean(mean(Xfk(:,:,event_type==33027),3),1)', 'linewidth',2, 'DisplayName','17Hz');
%         plot(freqs, mean(mean(Xfk(:,:,event_type==33026),3),1)', 'linewidth',2, 'DisplayName','21Hz');
%         max_13 = max(mean(mean(Xfk(:,:,event_type==33025),3),1));
%         max_17 = max(mean(mean(Xfk(:,:,event_type==33027),3),1));
%         max_21 = max(mean(mean(Xfk(:,:,event_type==33026),3),1));
%         max_val = max([max_13,max_17,max_21]);
%         line([13 13], [0 max_val],'Color','b','LineStyle','-');
%         line([26 26], [0 max_val],'Color','b','LineStyle','--');
%         line([17 17], [0 max_val],'Color','r','LineStyle','-');
%         line([34 34], [0 max_val],'Color','r','LineStyle','--');
%         line([21 21], [0 max_val],'Color','y','LineStyle','-');
%         line([42 42], [0 max_val],'Color','y','LineStyle','--');
%         %
%         print(gcf,[folder 'figures/original/psd_grand_avg/' 'subject' char(subj_cur) '_session' char(sess_cur(sessix))],'-dpng','-r300')
%         close all;
        
        figure;
        for xix=1:size(Xfk,1)
            subplot(4,2,xix);
            hold all;
            plot(freqs, mean(Xfk(xix,:,event_type==33025),3)', 'linewidth',2);
            plot(freqs, mean(Xfk(xix,:,event_type==33027),3)', 'linewidth',2);
            plot(freqs, mean(Xfk(xix,:,event_type==33026),3)', 'linewidth',2);
%                 legend('13','17','21');
            max_13 = max(mean(Xfk(xix,:,event_type==33025),3));
            max_17 = max(mean(Xfk(xix,:,event_type==33027),3));
            max_21 = max(mean(Xfk(xix,:,event_type==33026),3));
            max_val = max([max_13,max_17,max_21]);
            line([13 13], [0 max_val],'Color','b','LineStyle','-');
            line([26 26], [0 max_val],'Color','b','LineStyle','--');
            line([17 17], [0 max_val],'Color','r','LineStyle','-');
            line([34 34], [0 max_val],'Color','r','LineStyle','--');
            line([21 21], [0 max_val],'Color','y','LineStyle','-');
            line([42 42], [0 max_val],'Color','y','LineStyle','--');
            ylim([0 3e-3])
            title(label{xix});
        end
        %
%         print(gcf,[folder 'figures/original/psd_elec/' 'subject' char(subj_cur) '_session' char(sess_cur(sessix))],'-dpng','-r300')
%         close all;
    end
end

%% text file for figure in latex


for subjix=1:nSubj
    subj_cur     = subj(subjix);
    sess_cur     = sess{subjix};
    nSess        = length(sess_cur);
    for sessix=1:nSess
        % load data
        load([folder folder_mat 'subject' char(subj_cur) '_session' char(sess_cur(sessix))]);
        % build triggers
        event_pos           = event_pos(ismember(event_type,event_code));    % keep only useful event_pos
        event_type          = event_type(ismember(event_type,event_code));   % keep only useful event_type
        triggers            = zeros(size(raw,2),1);
        triggers(event_pos) = 1;
        % filter data
        FILT_param(4)       = round(fs/fmax);                                % compute decimation factor
        [xfilt, xfs, xtrig] = preprocessingEEG(raw',fs,FILT_param,triggers); % band-pass filt
        X                   = xfilt';
        % epoching (WARNING : OFFSET IS SET TO 2.5 seconds due to experiment-specific triggers' sync)
        Xk = epoch_p300(X,xtrig,L_epoch*xfs,xfs*offset);
        % compute power spectral density with welch-estimator
        Xfk=zeros(size(Xk,1),length(freqs),size(Xk,3));
        for k=1:size(Xk,3)
            Xfk(:,:,k)=pwelch(Xk(:,:,k)',L_welch*xfs,l_welch*xfs,freqs,xfs)';
        end
        % mean psd in the three conditions
        Xfrest_mean = mean(Xfk(:,:,event_type==33024),3);
        Xf13_mean   = mean(Xfk(:,:,event_type==33025),3);
        Xf17_mean   = mean(Xfk(:,:,event_type==33027),3);
        Xf21_mean   = mean(Xfk(:,:,event_type==33026),3);
        
        % parameters for text file
        nChan      = size(raw,1);
        nF         = length(freqs);
        all_s      = strings(1+nF,1+4*nChan);
        all_s(1,1) = "f";
        tmp        = string(label);
        for cix=1:nChan
            all_s(1,1+cix)         = join([tmp(cix),"rest"],"_");
            all_s(1,1+nChan+cix)   = join([tmp(cix),"13"],"_");
            all_s(1,1+2*nChan+cix) = join([tmp(cix),"17"],"_");
            all_s(1,1+3*nChan+cix) = join([tmp(cix),"21"],"_");
        end
        all_s(2:1+nF,1)                   = string(freqs);
        all_s(2:1+nF,2:1+nChan)           = string(Xfrest_mean');
        all_s(2:1+nF,2+nChan:1+2*nChan)   = string(Xf13_mean');
        all_s(2:1+nF,2+2*nChan:1+3*nChan) = string(Xf17_mean');
        all_s(2:1+nF,2+3*nChan:1+4*nChan) = string(Xf21_mean');
        % write in file
        filename = ['psd_elec_' 'subject' char(subj_cur) '_session' char(sess_cur(sessix)) '.txt'];
        fileID   = fopen([folder 'fig_txt/elec/' filename],'w','native','UTF-8');
        %
        [nLine,nCol] = size(all_s);
        for lix=1:nLine
            for cix=1:nCol-1
                fprintf(fileID,'%s,',all_s(lix,cix));
            end
            % last column, no separator
            cix = nCol;
            fprintf(fileID,'%s',all_s(lix,cix));
            fprintf(fileID,'\n');
        end
        fclose(fileID);
    end
end