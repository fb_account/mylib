%% (0) Parameters

folder     = '~/results_thesis/ssvep_dat/';
folder_gdf = 'ssvep_dat_gdf/';
folder_mat = 'ssvep_dat_mat/';
%
subj       = ["01","02","03","04","05","06","07","08","09","10","11","12"];
nSubj      = length(subj);
sess_gdf   = {
    ["2012.07.06-19.02.16","2012.07.06-19.06.14"];                                                                   % 01
    ["2012.07.19-17.36.23","2012.07.19-17.41.14"];                                                                   % 02
    ["2012.07.11-15.25.23","2012.07.11-15.33.08"];                                                                   % 03
    ["2012.07.18-17.52.30","2012.07.18-17.56.53"];                                                                   % 04
    ["2012.07.19-11.24.02","2012.07.19-11.28.18"];                                                                   % 05
    ["2012.07.20-12.20.55","2012.07.20-12.26.47"];                                                                   % 06
    ["2012.07.11-15.33.08","2012.07.18-09.15.30","2012.07.18-09.21.13"];                                             % 07
    ["2013.04.06-16.29.18","2013.04.06-16.35.05"];                                                                   % 08
    ["2013.04.09-17.32.29","2013.04.09-17.39.37"];                                                                   % 09
    ["2014.02.26-15.32.36","2014.02.26-15.40.22","2014.02.26-16.18.11","2014.02.26-16.25.45"];                       % 10
    ["2014.02.24-18.15.11","2014.02.24-18.23.37"];                                                                   % 11
    ["2014.03.10-19.17.37","2014.03.10-19.47.49","2014.03.10-20.11.55","2014.03.10-20.26.46","2014.03.10-20.41.35"]; % 12
    };
sess_mat   = {
    ["01","02"];                % 01
    ["01","02"];                % 02
    ["01","02"];                % 03
    ["01","02"];                % 04
    ["01","02"];                % 05
    ["01","02"];                % 06
    ["01","02","03"];           % 07
    ["01","02"];                % 08
    ["01","02"];                % 09
    ["01","02","03","04"];      % 10
    ["01","02"];                % 11
    ["01","02","03","04","05"]; % 12
    };


%% (1) Load data with fieldtrip, convert it to mat and save them


for subjix=1:nSubj
    subj_cur     = subj(subjix);
    sess_gdf_cur = sess_gdf{subjix};
    sess_mat_cur = sess_mat{subjix};
    nSess        = length(sess_gdf_cur);
    for sessix=1:nSess
        hdr        = ft_read_header([folder folder_gdf 'subject' char(subj_cur) '/record-[' char(sess_gdf_cur(sessix)) '].gdf']);
        raw        = ft_read_data([folder folder_gdf 'subject' char(subj_cur) '/record-[' char(sess_gdf_cur(sessix)) '].gdf']);
        event_pos  = hdr.orig.EVENT.POS';
        event_type = hdr.orig.EVENT.TYP';
        fs         = hdr.Fs;
        label      = hdr.label;
        save([folder folder_mat 'subject' char(subj_cur) '_session' char(sess_mat_cur(sessix))], 'raw','event_pos','event_type','fs','label');
    end
end



