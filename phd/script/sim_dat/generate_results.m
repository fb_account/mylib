%% (0) some parameters
outFold = '~/results_thesis/sim_dat/';

allSNR   = [10,50,100,500,1000];

nSNR     = length(allSNR);

%% (1) compute and save Moreau-Amari indexes of file of unmixing matrices

% simulated data
load([outFold 'data_sim.mat']);
% load file to deal with
identifier = 'classical_comp';
load([outFold 'unmix/sim_unmix_' identifier '.mat']);
nAlgo = length(algo_names);
% compute cMA
cMA_all = zeros(nAlgo,nSNR,nIt);
for aix=1:nAlgo
    for snrix=1:nSNR
        for it=1:nIt
            cMA_all(aix,snrix,it) = critMoAm(A_all(:,:,it),B_all(:,:,aix,snrix,it));
        end
    end
end
% save file
save([outFold 'cMA/sim_cMA_' identifier],'algo_names','cMA_all','allSNR');


%% (2) get median and quantiles needed for figures

% load Moreau-Amari performance indexes
identifier = 'all_div';
load([outFold 'cMA/sim_cMA_' identifier '.mat']);
% compute median and errors (quantiles)
pval_err   = 0.1;
cMA_median = 10*log10(median((cMA_all),3));   
cMA_err_m  = cMA_median - 10*log10(quantile(cMA_all,pval_err,3));
cMA_err_p  = 10*log10(quantile(cMA_all,1-pval_err,3)) - cMA_median;

cMA_mean   = 10*log10(mean((cMA_all),3));
cMA_std    = std(10*log10(cMA_all),0,3);

%% (3) statistical significance of the results (pair-wise comparisons)

nAlgo = length(algo_names);

% anova to see if there is something significant for a given snr
p_anova = zeros(1,nSNR);
for snrix=1:nSNR
%     p_anova(snrix)=anova1(squeeze(cMA_all(:,snrix,:))',[],'off');
    p_anova(snrix) = friedman(squeeze(cMA_all(:,snrix,:))',1,'off');
end

% pair-wise comparison between algorithms
snrix=5;
% p_ttest = zeros(nAlgo,nAlgo);
p_sgnrnk  = zeros(nAlgo,nAlgo);
for i=1:nAlgo
    for j=1:nAlgo
%         [~,p_ttest(i,j)] = ttest(squeeze((cMA_all(i,snrix,:))),squeeze((cMA_all(j,snrix,:))));
        [p_sgnrnk(i,j),~,stats] = signrank(squeeze(cMA_all(i,snrix,:)),squeeze(cMA_all(j,snrix,:)));
    end
end



%% (4) generate text file for latex figure

% file name identifier
file_str   = 'all_div';
% string array
nAlgo      = length(algo_names);
all_s      = strings(1+nSNR, 1+3*nAlgo);
all_s(1,1) = "snr";
tmp        = string(algo_names);
for aix=1:nAlgo
    all_s(1,1+aix)         = join([tmp(aix),"med"],"_");
    all_s(1,1+nAlgo+aix)   = join([tmp(aix),"err_m"],"_");
    all_s(1,1+2*nAlgo+aix) = join([tmp(aix),"err_p"],"_");
end
all_s(2:1+nSNR,1)                   = string(1:nSNR);
all_s(2:1+nSNR,2:1+nAlgo)           = string(cMA_median');
all_s(2:1+nSNR,2+nAlgo:1+2*nAlgo)   = string(cMA_err_m');
all_s(2:1+nSNR,2+2*nAlgo:1+3*nAlgo) = string(cMA_err_p');
% write in file
filename = ['sim_' file_str '.txt'];
fileID   = fopen([outFold 'fig_txt/' filename],'w','native','UTF-8');
%
[nLine,nCol] = size(all_s);
for lix=1:nLine
    for cix=1:nCol-1
        fprintf(fileID,'%s,',all_s(lix,cix));
    end
    % last column, no separator
    cix = nCol;
    fprintf(fileID,'%s',all_s(lix,cix));
    fprintf(fileID,'\n');
end
fclose(fileID);


