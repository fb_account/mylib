%% (0) load data and define some parameters

outFold = '~/results_thesis/sim_dat/';
load([outFold 'data_sim.mat']);

allSNR   = [10,50,100,500,1000];
nSNR     = length(allSNR);

warning off;

%% (1) prepare AJD algorithms to be used


% common options to all algorithms
opt_common = @(C,manifold,GLfun2opt,optStrategy,optmethod,diagmat) struct('B0',eye(n), 'tol',1e-9, 'maxiter',1000, 'weights',ones(1,K), 'man_opt',struct('verbosity',0), ...
                                                                          'manifold',manifold, 'GLfun2opt',GLfun2opt, 'optStrategy',optStrategy, 'optmethod',optmethod, 'diagmat',diagmat ...
                                                                         );


% (1.0) classical algorithms for comparisons
identifier    = 'classical_comp';
algo_names    = { 'NOJoB', 'uwedge', 'jadiag', ...
                  'FiPa', 'FPc', 'KLliPa'
                };
nAlgo         = length(algo_names);
%
opt_classical = struct('B0',eye(n),'weights',ones(1,K),'tol',1e-9,'maxiter',1000);

%
algos       = cell(1,nAlgo);
algos{1}    = @(C) m_NOJoB(C,opt_classical);
algos{2}    = @(C) m_uwedge(C,opt_classical);
algos{3}    = @(C) m_jadiag(C,opt_classical);
algos{4}    = @(C) RAJD(    C,opt_common(C, intrinsicpolarfactory(n,C) ,@F_direct   ,'direct' ,@rlbfgs,[]));
algos{5}    = @(C) RAJD_inv(C,opt_common(C, polarfactory(n)            ,@F_inv_ind  ,[]       ,@trustregions,@ddiag));
algos{6}    = @(C) RAJD(    C,opt_common(C, intrinsicpolarfactory(n,C) ,@KLl_direct ,'direct' ,@rlbfgs,[]));



% % (1.1) all Frobenius AJD functions with direct strategy
% identifier = 'F_direct';
% algo_names = { 'FiPa', 'FoPa', ...
%                'FiGLla', 'FoGLla', 'FnhGLla', ...
%                'FiGLra', 'FoGLra', 'FnhGLra'
%              };
% nAlgo      = length(algo_names);
% %
% GLfun2opt   = @F_direct;
% optStrategy = 'direct';
% optmethod   = @rlbfgs;
% %
% algos    = cell(1,nAlgo);
% algos{1} = @(C) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C)    ,GLfun2opt,optStrategy,optmethod,[]));
% algos{2} = @(C) RAJD(C,opt_common(C, obliquepolarfactory(n)        ,GLfun2opt,optStrategy,optmethod,[]));
% algos{3} = @(C) RAJD(C,opt_common(C, intrinsicGLleftfactory(n,C)   ,GLfun2opt,optStrategy,optmethod,[]));
% algos{4} = @(C) RAJD(C,opt_common(C, obliqueGLleftfactory(n)       ,GLfun2opt,optStrategy,optmethod,[]));
% algos{5} = @(C) RAJD(C,opt_common(C, nonholonomicGLleftfactory(n)  ,GLfun2opt,optStrategy,optmethod,[]));
% algos{6} = @(C) RAJD(C,opt_common(C, intrinsicGLrightfactory(n,C)  ,GLfun2opt,optStrategy,optmethod,[]));
% algos{7} = @(C) RAJD(C,opt_common(C, obliqueGLrightfactory(n)      ,GLfun2opt,optStrategy,optmethod,[]));
% algos{8} = @(C) RAJD(C,opt_common(C, nonholonomicGLrightfactory(n) ,GLfun2opt,optStrategy,optmethod,[]));

% % (1.2) all KLl AJD functions with direct strategy
% identifier = 'KLl_direct';
% algo_names = { 'KLliPa', 'KLloPa', ...
%                'KLliGLla', 'KLloGLla', 'KLlnhGLla', ...
%                'KLliGLra', 'KLloGLra', 'KLlnhGLra'
%              };
% nAlgo      = length(algo_names);
% %
% GLfun2opt   = @KLl_direct;
% optStrategy = 'direct';
% optmethod   = @rlbfgs;
% %
% algos    = cell(1,nAlgo);
% algos{1} = @(C) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C)    ,GLfun2opt,optStrategy,optmethod,[]));
% algos{2} = @(C) RAJD(C,opt_common(C, obliquepolarfactory(n)        ,GLfun2opt,optStrategy,optmethod,[]));
% algos{3} = @(C) RAJD(C,opt_common(C, intrinsicGLleftfactory(n,C)   ,GLfun2opt,optStrategy,optmethod,[]));
% algos{4} = @(C) RAJD(C,opt_common(C, obliqueGLleftfactory(n)       ,GLfun2opt,optStrategy,optmethod,[]));
% algos{5} = @(C) RAJD(C,opt_common(C, nonholonomicGLleftfactory(n)  ,GLfun2opt,optStrategy,optmethod,[]));
% algos{6} = @(C) RAJD(C,opt_common(C, intrinsicGLrightfactory(n,C)  ,GLfun2opt,optStrategy,optmethod,[]));
% algos{7} = @(C) RAJD(C,opt_common(C, obliqueGLrightfactory(n)      ,GLfun2opt,optStrategy,optmethod,[]));
% algos{8} = @(C) RAJD(C,opt_common(C, nonholonomicGLrightfactory(n) ,GLfun2opt,optStrategy,optmethod,[]));



% % (1.3) F and KLl AJD functions with the different strategies
% identifier = 'strategies';
% algo_names = { 'FiPa', 'FiPb', 'FPc', ...
%                'KLliPa', 'KLliPb', 'KLlPc' ...
%              };
% nAlgo      = length(algo_names);
% %
% optmethod   = @trustregions;
% diagmat     = @ddiag;
% %
% algos    = cell(1,nAlgo);
% algos{1} = @(C) RAJD(    C,opt_common(C, intrinsicpolarfactory(n,C) ,@F_direct    ,'direct'  ,optmethod,[]));
% algos{2} = @(C) RAJD(    C,opt_common(C, intrinsicpolarfactory(n,C) ,@F_ind       ,'indirect',optmethod,diagmat));
% algos{3} = @(C) RAJD_inv(C,opt_common(C, polarfactory(n)            ,@F_inv_ind   ,[]        ,optmethod,diagmat));
% algos{4} = @(C) RAJD(    C,opt_common(C, intrinsicpolarfactory(n,C) ,@KLl_direct  ,'direct'  ,optmethod,[]));
% algos{5} = @(C) RAJD(    C,opt_common(C, intrinsicpolarfactory(n,C) ,@KLl_ind     ,'indirect',optmethod,diagmat));
% algos{6} = @(C) RAJD_inv(C,opt_common(C, polarfactory(n)            ,@KLl_inv_ind ,[]        ,optmethod,diagmat));



% % (1.4) all divergence AJD functions with direct strategy
% identifier = 'all_div';
% algo_names = { 'FiPa', 'KLliPa', ...
%                'KLriPa', 'KLsiPa', 'alphaLDiPa', ...
%                'RiPa', 'LEiPa', 'WiPa' ...
%              };
% % algo_names = { 'FoPa', 'KLloPa', ...
% %                'KLroPa', 'KLsoPa', 'alphaLDoPa', ...
% %                'RoPa', 'LEoPa', 'WoPa' ...
% %              };
% nAlgo      = length(algo_names);
% %
% optStrategy = 'direct';
% optmethod   = @rlbfgs;
% %
% algos    = cell(1,nAlgo);
% algos{1} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@F_direct                    ,optStrategy,optmethod,[]));
% algos{2} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@KLl_direct                  ,optStrategy,optmethod,[]));
% algos{3} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@KLr_direct                  ,optStrategy,optmethod,[]));
% algos{4} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@KLs_direct                  ,optStrategy,optmethod,[]));
% algos{5} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@(C,w) alphaLD_direct(C,w,0) ,optStrategy,optmethod,[]));
% algos{6} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@R_direct                    ,optStrategy,optmethod,[]));
% algos{7} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@LE_direct                   ,optStrategy,optmethod,[]));
% algos{8} = @(C,w) RAJD(C,opt_common(C, intrinsicpolarfactory(n,C) ,@W_direct                    ,optStrategy,optmethod,[]));

% algos{1} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@F_direct                    ,optStrategy,optmethod,[]));
% algos{2} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@KLl_direct                  ,optStrategy,optmethod,[]));
% algos{3} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@KLr_direct                  ,optStrategy,optmethod,[]));
% algos{4} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@KLs_direct                  ,optStrategy,optmethod,[]));
% algos{5} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@(C,w) alphaLD_direct(C,w,0) ,optStrategy,optmethod,[]));
% algos{6} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@R_direct                    ,optStrategy,optmethod,[]));
% algos{7} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@LE_direct                   ,optStrategy,optmethod,[]));
% algos{8} = @(C,w) RAJD(C,opt_common(C, obliquepolarfactory(n) ,@W_direct                    ,optStrategy,optmethod,[]));

%% (2) perform AJD

B_all = zeros(n,n,nAlgo,nSNR,nIt);

for it=1:nIt
disp(['it: ' int2str(it)]);
    % make dataset
    A  = A_all(:,:,it);
    d  = d_all(:,:,it);
    N  = N_all(:,:,:,it);
    Cs = zeros(n,n,K);
    for k=1:K
        Cs(:,:,k) = A*diag(d(:,k))*A';
    end
    for snrix=1:nSNR
        snr = allSNR(snrix);
        disp(['snr: ' int2str(snr)]);
        C   = Cs + N/snr;
        %
        % whitening of data
        L  = chol(mean(C,3),'lower');
        Cw = zeros(n,n,K);
        for k=1:K
            Cw(:,:,k) = L\C(:,:,k)/L';
        end
        %
        % perform AJD
        tic;
        parfor algoix=1:nAlgo
%         disp(['algo: ' algo_names{algoix}]);
            [Bw,info,options] = algos{algoix}(Cw);
            B = Bw/L;
            %
            B_all(:,:,algoix,snrix,it) = B;
            disp(['algo: ' algo_names{algoix}]);
        end
        toc;
    end
    % save results
    save([outFold 'unmix/sim_unmix_' identifier],'B_all', 'algo_names', 'algos', 'opt_common','allSNR');
end

