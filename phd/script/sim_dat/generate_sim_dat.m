%% (0) parameters
nIt      = 50;
n        = 16;
K        = 50;

% not needed at this point
% allSNR   = [100,500,1000,5000];

condA    = [1,10];
powerlaw = 1;

outFold = '~/results_thesis/sim_dat/';

%% (1) generate data

% mixing matrices
A_all = zeros(n,n,nIt);
% true target diagonal matrices
d_all = zeros(n,K,nIt);
% structured noise matrices
N_all = zeros(n,n,K,nIt);

for it=1:nIt
    % random normalized mixing matrix
    A_tmp         = randn(n);
    D_tmp         = diag(diag(A_tmp*A_tmp').^(-.5));
    A_tmp         = D_tmp*A_tmp;
    % ensure conditioning of A
    while cond(A_tmp)<condA(1) || cond(A_tmp)>condA(2)
        A_tmp     = randn(n);
        D_tmp     = diag(diag(A_tmp*A_tmp').^(-.5));
        A_tmp     = D_tmp*A_tmp;
    end
    A_all(:,:,it) = A_tmp;
    %
    % targets
    for k=1:K
        d_all(:,k,it) = (randn(n,1).^2)./((1:n)'.^powerlaw);
    end
    %
    % noise
    for k=1:K
        A_tmp           = randn(n);
        D_tmp           = diag(diag(A_tmp*A_tmp').^(-.5));
        A_tmp           = D_tmp*A_tmp;
        d_tmp           = (randn(n,1).^2)./((1:n)'.^powerlaw);
        N_all(:,:,k,it) = A_tmp*diag(d_tmp)*A_tmp';
    end
end

%% (2) save parameters and data in file

save([outFold 'data_sim.mat'],'nIt','n','K','condA','powerlaw' ...
                             ,'A_all','d_all','N_all');