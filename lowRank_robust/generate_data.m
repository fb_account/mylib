%% parameters
fold = '../results/';

p    = 16;                                 % matrix size
k    = 4;                                  % matrix rank
cSig = 50;                                 % condition number of Sig
SpNR = 100;                                % spike to noise ratio
d    = [3,100];                            % degrees of freedom of the t-distribution
n    = [12,14,15,17,20,40,70,100,200,300]; % number of samples
nIt  = 100;                                % number of repetitions

% Fisher metric parameters
alpha = (d+p)./(d+p+1);
beta  = alpha-1;

%% generate cov mat
U        = orth(randn(p,k) + 1i*randn(p,k));

s        = zeros(k,1);
s(1)     = 1/sqrt(cSig);
s(2)     = sqrt(cSig);
if k>2
    s(3:end) = 1/sqrt(cSig) + (sqrt(cSig)-1/sqrt(cSig)).*rand(k-2,1);
end
Sig      = diag(s(randperm(k)));
Sig      = Sig/trace(Sig)*k*SpNR;

% rho = 0.9 * (1+1i)/sqrt(2) ;
% vec_rho = rho.^( 0:1:(k-1) ) ;
% Sig = toeplitz(vec_rho);
% for i=1:k % just to avoid some numerical issues with cholesky
%     Sig(i,i) = real(Sig(i,i));
% end
% Sig      = Sig/trace(Sig)*k*SpNR;


theta    = struct('U',U,'Sig',Sig);

% construct cov mat
R = eye(p) + U*Sig*U';

%% generate data x of size p*max(n)*nIt from t-distrib with cov I_p + \varphi(theta)
x = zeros(p,max(n),nIt,length(d));
for dix=1:length(d)
    for it=1:nIt
        x(:,:,it,dix) = gene_tdistrib(p,max(n),R,d(dix));
    end
end

%% save these data
save([fold 'data_tyler_hermfixedrank_k' int2str(k) '.mat'],'p','k','d','n','nIt','theta','x','alpha','beta');