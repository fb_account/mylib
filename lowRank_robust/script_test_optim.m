%% some parameters and data to check code is ok
n=50;
p=10;
k=4;
d=3;

alpha = (d + n ) / ( d + n + 1) ;
beta  = alpha - 1 ;

% cov
U   = orth(randn(p,k) + 1i*randn(p,k));
Sig = 50*diag(randn(1,k).^2);
H   = U*Sig*U';
R   = eye(p) + H;

% data
x = gene_tdistrib(p, n, R, d);


%% check that Euclidean gradient, Hessian and vectorized Hessian of tyler_HPD are ok

[costfun_HPD,gradfun_HPD,hessfun_HPD,hessVecfun_HPD]=tyler_HPD(x);

% some random point and tangent vector to check grad, hess and hessVec
R_tmp=randn(p)+1i*randn(p);
R_tmp=R_tmp*R_tmp';
xi_tmp = randn(p)+1i*randn(p);
xi_tmp = .5*(xi_tmp+xi_tmp');
xi_tmp = xi_tmp / norm(xi_tmp(:), 'fro');

problem   = [];
problem.M = hermitianfactory(p);
problem.cost = costfun_HPD;
%
problem.egrad = gradfun_HPD;
figure;checkgradient(problem, R_tmp,xi_tmp);
%
problem.ehess = hessfun_HPD;
figure;checkhessian(problem, R_tmp,xi_tmp);
%
% problem.ehess = @(R,xi) reshape(([eye(p^2),zeros(p^2)]+1i*[zeros(p^2),eye(p^2)])*hessVecfun_HPD(R)*[real(xi(:));imag(xi(:))],[p,p]); % should be done with problem.M.vec and problem.M.mat rather than manually
problem.ehess = @(R,xi) problem.M.mat(R,hessVecfun_HPD(R)*problem.M.vec(R,xi));
figure;checkhessian(problem, R_tmp,xi_tmp);


%% check that Euclidean gradient, Hessian and vectorized Hessian from HPD2HLR are ok

[costfun,gradfun,hessfun,hessVecfun]=HPD2HLR(costfun_HPD,gradfun_HPD,hessfun_HPD,hessVecfun_HPD);

problem      = [];
problem.M    = productmanifold(struct('U',euclideancomplexfactory(p,k),'Sig',hermitianfactory(k)));
problem.cost = costfun;
%
problem.egrad = gradfun;
figure;checkgradient(problem);
%
problem.ehess = hessfun;
figure;checkhessian(problem);
% looks like there is an issue with productmanifold.vec and maybe with
% productmanifold.mat (with the indices) so did it manually for now
problem.ehess = @(theta,xi) struct('U',reshape(([eye(p*k),zeros(p*k),zeros(p*k,2*k^2)]+1i*[zeros(p*k),eye(p*k),zeros(p*k,2*k^2)])*hessVecfun(theta)*[real(xi.U(:));imag(xi.U(:));real(xi.Sig(:));imag(xi.Sig(:))],[p,k]),...
    'Sig',reshape(([zeros(k^2,2*p*k),eye(k^2),zeros(k^2)]+1i*[zeros(k^2,2*p*k+k^2),eye(k^2)])*hessVecfun(theta)*[real(xi.U(:));imag(xi.U(:));real(xi.Sig(:));imag(xi.Sig(:))],[k,k]));
figure;checkhessian(problem);


%% check that Riemannian gradient, Hessian and vectorized Hessian from hermfixedrankfactory are ok

problem = [];
problem.M = hermfixedrankfactory(p,k,alpha,beta);
problem.cost = costfun;
%
problem.egrad = gradfun;
figure;checkgradient(problem);
%
problem.ehess = hessfun;
figure;checkhessian(problem);
%
problem = rmfield(problem,'ehess');
problem.hess = @(theta,xi) problem.M.mat(theta,problem.M.ehessVec2rhessVec(theta,gradfun(theta),hessVecfun(theta))*problem.M.vec(theta,xi));
figure;checkhessian(problem);
% close all