p=15;
k=5;

U     = orth(randn(p,k) + 1i*randn(p,k));
SNR = 50;
Sig   = diag(k:-1:1)/sum(1:k);
H     = SNR* U*Sig*U';
[~,DH,~] = svd(H);
lambda_dh = diag(DH(1:k,1:k));

R     = eye(p) + H;
for r=1:p
    R(r,r) = real(R(r,r));
 end

vec_n = 2*k:k:20*p;
err_SCM = zeros(size(vec_n));
CRB = zeros(size(vec_n));
NMC = 1e3;

for i=1:length(vec_n)
    disp(i);
    n=vec_n(i);
    
    err_SCM_n = 0;
    
    tic
    for w=1:NMC
    %% DATA
     L = chol(R);
     DATA_w=sqrt(1/2)*(randn(p,n) +1i*randn(p,n) ) ;
     DATA = L' * DATA_w;
    
     %% SCM
     SCM = DATA*DATA' / n ;
     [U_SCM,~,~] = svd(SCM);
     U_SCM = U_SCM(:,1:k);
     
     %% ERR
     err_SCM_n = err_SCM_n  + (norm(acos(svd(orth(U_SCM)'*orth(U)))))^2 ;
            
    end
    toc
    
    err_SCM(i) = err_SCM_n / NMC ;
        
    %% CRB
    CRB(i)= ( (p-k)/n * sum( (1+lambda_dh)./(lambda_dh.^2) ) );
    
end

%% 
plot(vec_n./p,err_SCM,'r');
hold on
plot(vec_n./p,CRB,'k');

