%% some parameters and data to check code is ok
n=50;
p=10;
k=4;
d=3;

alpha = (d + n ) / ( d + n + 1) ;
beta  = alpha - 1 ;

% cov
U     = orth(randn(p,k) + 1i*randn(p,k));
tmp   = orth(randn(k) + 1i*randn(k));
Sig   = tmp*50*diag(randn(1,k).^2)*tmp';
theta = struct('U',U,'Sig',Sig);
H     = U*Sig*U';
R     = eye(p) + H;


% data
x = gene_tdistrib(p, n, R, d);

%% check orthonormal basis

M           = hermfixedrankfactory(p,k,alpha,beta);
e_M         = basisHermfixedrank(theta,alpha,beta);
dimBasis    = length(e_M);
%
metricBasis = zeros(dimBasis);
for i=1:dimBasis
    for j=1:dimBasis
        metricBasis(i,j) = M.inner(theta,e_M(i),e_M(j));
    end
end

% this should be very close to zero
disp(norm(metricBasis-eye(dimBasis),'fro')/norm(eye(dimBasis),'fro'));


%% compute FIM
F=fisherInformationMatrix_CESlowrank(theta,alpha,beta);
% check that block corresponding to Grassman is isolated:
% this should be very close to zero
disp(norm(F(2*(p-k)*k+1:end,1:2*(p-k)*k),'fro'))


%% 



