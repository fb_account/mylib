%% Get data and parameters
fold = '../results/';
k    = 4;
load([fold 'data_tyler_hermfixedrank_k' int2str(k) '.mat']);
disp(['k=' int2str(k)]);

nD = length(d);

%% compute Fisher information matrix up to factor n (number of samples)
F       = zeros(2*p*k,2*p*k,nD);
F_Uperp = zeros(2*(p-k)*k,2*(p-k)*k,nD);
F_USig  = zeros(2*k^2,2*k^2,nD);
F_Sig   = zeros(k^2,k^2,nD);
for dix=1:nD
    F(:,:,dix) = fisherInformationMatrix_CESlowrank(theta,alpha(dix),beta(dix));

    % extract components of interest
    F_Uperp(:,:,dix) = F(1:2*(p-k)*k,1:2*(p-k)*k,dix);
    F_USig(:,:,dix)  = F(2*(p-k)*k+1:end,2*(p-k)*k+1:end,dix);
    F_Sig(:,:,dix)   = F(2*(p-k)*k+k^2+1:end,2*(p-k)*k+k^2+1:end,dix);
end
%% compute iCRB coefficients (the CRB is given by coeff_iCRB/n)

coeff_iCRB_hlr     = zeros(1,nD);
coeff_iCRB_hlr_alt = zeros(1,nD);
coeff_iCRB_Gr      = zeros(1,nD);
for dix=1:nD
    % some svds
    L_Uperp = svd(F_Uperp(:,:,dix));
    L_USig  = svd(F_USig(:,:,dix)); % size (2*k^2)*(2*k^2), rank k^2
    L_USig  = L_USig(1:k^2);
    L_Sig   = svd(F_Sig(:,:,dix));

    % compute some coeffs
    c_Uperp = sum(1./L_Uperp);
    c_USig  = sum(1./L_USig);
    c_Sig   = sum(1./L_Sig);

    % compute crb coeffs
    coeff_iCRB_hlr(dix)     = c_Uperp + c_USig; % quotient
    coeff_iCRB_hlr_alt(dix) = c_Uperp + c_Sig;  % alternative one on quotient (adapted to divergence)
    coeff_iCRB_Gr(dix)      = c_Uperp;          % Grassmann
end


% ca marche pas malheureusement ca... J'en suis assez triste, j'aime bien
% Thm 3 dans [S05]
% P = fisherHLR2HPD(theta,alpha,beta);
% coeff_iCRB_HPD = real(trace(P*pinv(F)*P'));

%% iCRB
iCRB_hlr     = zeros(length(n),nD);
iCRB_hlr_alt = zeros(length(n),nD);
iCRB_Gr      = zeros(length(n),nD);
for dix=1:nD
    iCRB_hlr(:,dix)     = coeff_iCRB_hlr(dix)./n;
    iCRB_hlr_alt(:,dix) = coeff_iCRB_hlr_alt(dix)./n;
    iCRB_Gr(:,dix)      = coeff_iCRB_Gr(dix)./n;
end

%% write iCRB in file
all_s      = strings(1+length(n),1+3*nD);
all_s(1,1) = "n";
for dix=1:nD
    all_s(1,1+dix)      = strcat("iCRB_hlr_d",string(int2str(d(dix))));
    all_s(1,1+nD+dix)   = strcat("iCRB_hlr_d",string(int2str(d(dix))));
    all_s(1,1+2*nD+dix) = strcat("iCRB_Gr_d",string(int2str(d(dix))));
end
all_s(2:1+length(n),1)             = string(n);
all_s(2:1+length(n),2:1+nD)        = string(10*log10(iCRB_hlr));
all_s(2:1+length(n),2+nD:1+2*nD)   = string(10*log10(iCRB_hlr_alt));
all_s(2:1+length(n),2+2*nD:1+3*nD) = string(10*log10(iCRB_Gr));


% write in file
filename = [fold 'iCRB_hlr_k' int2str(k) '.txt'];
fileID   = fopen(filename,'w','native','UTF-8');
%
[nLine,nCol] = size(all_s);
for lix=1:nLine
    for cix=1:nCol-1
        if ~ismissing(all_s(lix,cix))
            fprintf(fileID,'%s,',all_s(lix,cix));
        else
            fprintf(fileID,' ,');
        end
    end
    % last column, no separator
    cix = nCol;
    if ~ismissing(all_s(lix,cix))
        fprintf(fileID,'%s',all_s(lix,cix));
    else
        fprintf(fileID,' ');
    end
    fprintf(fileID,'\n');
end
fclose(fileID);


