%% load data
fold = '../results/';
k    = 4;
load([fold 'data_tyler_hermfixedrank_k' int2str(k) '.mat']);
load([fold 'cov_tyler_hermfixedrank_k' int2str(k) '.mat']);
disp(['k=' int2str(k)]);

nN = length(n);
nM = length(meth);
nD = length(d);

%% estimation errors
% divergence hermfixedrank matrices
err_hlr = zeros(nM,length(n),nIt,nD);
% distance Grassman
err_Gr = zeros(nM,length(n),nIt,nD);

% err_tmp = zeros(nM,length(n),nIt);
% err_HPD = zeros(nM,length(n),nIt);

for dix=1:nD
    for it=1:nIt
        for i=1:nN
            n_cur = n(i);
            for m=1:nM
                % divergence hermfixedrank matrices
%                 [err_hermfixedrank(m,i,it),err_Gr(m,i,it),err_tmp(m,i,it)] = divergenceHermfixedrank(theta,theta_all{m,i,it},alpha,beta);
                [err_hlr(m,i,it,dix),err_Gr(m,i,it,dix),~] = divergenceHermfixedrank(theta,theta_all{m,i,it,dix},alpha(dix),beta(dix));
                % distance HPD between eye(p)+phi(theta) and its estimate
    %             err_HPD(m,i,it) = dist_HPD(eye(p)+theta.U*theta.Sig*theta.U', eye(p)+theta_all{m,i,it}.U*theta_all{m,i,it}.Sig*theta_all{m,i,it}.U', alpha, beta);
            end
        end
    end
end


%% write in file

% string array
all_s                = strings(1+nN, 1+nD*nM);
all_s(1,1)           = "n";
all_s(2:1+nN,1)      = string(n);
%
for dix=1:nD
    for mix=1:nM
        all_s(1,1+nD*(mix-1)+dix) = strcat(meth(mix),"_d",string(int2str(d(dix))));
    end
end
for mix=1:nM
    all_s(2:1+nN,1+nD*(mix-1)+1:1+nD*mix) = string(10*log10(squeeze(mean(err_hlr(mix,:,:,:),3))));
end

% write in file
filename = [fold 'err_hlr_k' int2str(k) '.txt'];
fileID   = fopen(filename,'w','native','UTF-8');
%
[nLine,nCol] = size(all_s);
for lix=1:nLine
    for cix=1:nCol-1
        if ~ismissing(all_s(lix,cix))
            fprintf(fileID,'%s,',all_s(lix,cix));
        else
            fprintf(fileID,' ,');
        end
    end
    % last column, no separator
    cix = nCol;
    if ~ismissing(all_s(lix,cix))
        fprintf(fileID,'%s',all_s(lix,cix));
    else
        fprintf(fileID,' ');
    end
    fprintf(fileID,'\n');
end
fclose(fileID);

% err_Gr
% string array
all_s                = strings(1+nN, 1+nD*nM);
all_s(1,1)           = "n";
all_s(2:1+nN,1)      = string(n);
%
for dix=1:nD
    for mix=1:nM
        all_s(1,1+nD*(mix-1)+dix) = strcat(meth(mix),"_d",string(int2str(d(dix))));
    end
end
for mix=1:nM
    all_s(2:1+nN,1+nD*(mix-1)+1:1+nD*mix) = string(10*log10(squeeze(mean(err_Gr(mix,:,:,:),3))));
end

% write in file
filename = [fold 'err_Gr_k' int2str(k) '.txt'];
fileID   = fopen(filename,'w','native','UTF-8');
%
[nLine,nCol] = size(all_s);
for lix=1:nLine
    for cix=1:nCol-1
        if ~ismissing(all_s(lix,cix))
            fprintf(fileID,'%s,',all_s(lix,cix));
        else
            fprintf(fileID,' ,');
        end
    end
    % last column, no separator
    cix = nCol;
    if ~ismissing(all_s(lix,cix))
        fprintf(fileID,'%s',all_s(lix,cix));
    else
        fprintf(fileID,' ');
    end
    fprintf(fileID,'\n');
end
fclose(fileID);







%% some preliminary figure

% figure;
% subplot(3,1,1)
% hold all;
% title('div hermfixedrank');
% for m=1:nM
%     plot(10*log10(n), 10*log10(squeeze(mean(err_hermfixedrank(m,:,:),3))));
% end
% plot(10*log10(n),10*log10(iCRB_hlr),'k');
% plot(10*log10(n),10*log10(iCRB_hlr_bad),'g');
% legend([meth,"CRB","CRB bad"]);
% xlabel('n');
% 
% 
% 
% % figure;
% subplot(3,1,2)
% hold all;
% title('dist Grassman');
% for m=1:nM
%     plot(10*log10(n), 10*log10(squeeze(mean(err_Gr(m,:,:),3))));
% end
% xlabel('n');
% plot(10*log10(n),10*log10(iCRB_Gr),'k');
% legend([meth,"CRB"]);
% 
% 
% % figure;
% subplot(3,1,3)
% hold all;
% title('dist tmp');
% for m=1:nM
%     plot(10*log10(n), 10*log10(squeeze(mean(err_tmp(m,:,:),3))));
% end
% xlabel('n');
% plot(10*log10(n),10*log10(iCRB_USig),'k');
% plot(10*log10(n),10*log10(iCRB_Sig),'g');
% legend([meth,"CRB","CRB bad"]);

% ca marche pas ca
% % figure;
% subplot(4,1,4)
% hold all;
% title('dist HPD');
% for m=1:nM
%     plot(10*log10(n), 10*log10(squeeze(mean(err_HPD(m,:,:),3))));
% end
% legend(meth);
% xlabel('n');
% plot(10*log10(n),10*log10(iCRB_HPD),'k');