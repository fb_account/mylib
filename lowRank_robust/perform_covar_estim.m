%% load data
fold = '../results/';
k    = 4;
load([fold 'data_tyler_hermfixedrank_k' int2str(k) '.mat']);

disp(['k=' int2str(k)]);

%% optimization parameters
maxit = 1000;
tol   = 1e-6;

%% initialize results structures
meth = ["pSCM","TlrMM","TlrRO-st","TlrRO-tr"]; % methods
nM   = length(meth);

solvers = {@steepestdescent,@trustregions};

theta_all    = cell(nM,length(n),nIt,length(d));
% theta_it_all = cell(nM,length(n),nIt,length(d));

%% perform covariance estimation
for dix=1:length(d)
    man = hermfixedrankfactory(p,k,alpha(dix),beta(dix));
    for it=1:nIt
        fprintf(['d=' int2str(d(dix))  '    it=' int2str(it) '    n= ']);
        for i=1:length(n)
            fprintf([int2str(n(i)) ' ']);
            n_cur = n(i);
            % select data
            x_cur = x(:,1:n_cur,it,dix);

            % SCM
            SCM = x_cur*x_cur'/n_cur;
            % projected SCM
            [Us,Ls] = svd(SCM);
            Us      = Us(:,1:k);
            ls      = diag(Ls);
            sigs    = mean(ls(k+1:p));
            pls     = ls(1:k)-sigs*ones(k,1);
            pSCM    = struct('U',Us,'Sig',diag(pls)/sigs);
            %
            theta_all{1,i,it,dix}    = pSCM;
%             theta_it_all{1,i,it,dix} = pSCM;

            % init algos
%             H0        = diag([ones(1,k),zeros(1,p-k)]);
%             [U0,Sig0] = svd(H0,0);
%             theta0    = struct('U',U0(:,1:k),'Sig',Sig0(1:k,1:k));
            theta0 = struct('U',Us,'Sig',eye(k));

            % Tyler, low rank with block MM
            [lrTMM,lrTMM_it] = algo_tyler_lr_SBP15(x_cur,theta0,tol,maxit);
            %
            theta_all{2,i,it,dix}    = lrTMM;
%             theta_it_all{2,i,it,dix} = lrTMM_it;

            % Tyler, low rank with Riemannian optimization
            [costfun_HPD,gradfun_HPD,hessfun_HPD,hessVecfun_HPD] = tyler_HPD(x_cur);
            [costfun,gradfun,hessfun,hessVecfun]                 = HPD2HLR(costfun_HPD,gradfun_HPD,hessfun_HPD,hessVecfun_HPD);
            %
            problem = struct('M',man,'cost',costfun,'egrad',gradfun,'ehess',hessfun,'ehessVec',hessVecfun,'tol',tol);
            options = struct('maxiter',maxit,'verbosity',0,'statsfun',@statsfun,'stopfun',@stopfun);
            %
            for sol=1:length(solvers)
                solver             = solvers{sol};
                [thetaRO,~,infoRO] = solver(problem,theta0,options);
                %
                theta_all{2+sol,i,it,dix}    = thetaRO;
%                 theta_it_all{2+sol,i,it,dix} = [infoRO.x];
%                 disp(length(infoRO));
            end
        end
        fprintf('\n');
    end
end


save([fold 'cov_tyler_hermfixedrank_k' int2str(k) '.mat'],'meth','theta_all');
% save([fold 'cov_tyler_hermfixedrank_k' int2str(k) '.mat'],'meth','theta_all','theta_it_all');

