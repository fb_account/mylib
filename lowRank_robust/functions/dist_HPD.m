function dist=dist_HPD(A,B,alpha,beta)

[UA,LA] = svd(A);
A_inv12 = UA*diag(diag(LA).^-.5)*UA';

tmp = A_inv12*B*A_inv12;
L   = svd(tmp);

dist = sqrt(alpha*sum((log(L)).^2) + beta*(sum(log(L)))^2);

end