function M = hermfixedrankfactory(p,k,alpha,beta)

M.name = @() sprintf('manifold of %dx%d Hermitian positive semidefinite matrices of rank %d with quotient geometry (St(p,n)xHPD(p))/U(p)',p,p,k);

M.dim = @() 2*p*k - k^2;

M.inner = @inner;
function in = inner(X,xi,eta)
    in = real(trace(xi.U'*(eye(p)-.5*X.U*X.U')*eta.U)) + real(alpha*trace((X.Sig\xi.Sig)*(X.Sig\eta.Sig)) + beta*trace(X.Sig\xi.Sig)*trace(X.Sig\eta.Sig));
end

M.norm = @(X, xi) sqrt(M.inner(X, xi, xi));

M.dist = @(x, y) error('herm_psd_fixedrank_factory.dist not implemented yet.');

M.typicaldist = @() sqrt(2*p*k -k^2);

function x=herm(x), x=(x+x')/2; end
function x=skew(x), x=(x-x')/2; end

M.proj = @projection;
function xi = projection(X, Z)
    xi = proj_tangent(X,Z);
    xi = proj_horizontal(X,xi);
end
function xi = proj_tangent(X,Z)
    xi.U   = Z.U - X.U*herm(X.U'*Z.U);
    xi.Sig = herm(Z.Sig);
end
function xih = proj_horizontal(X,xi)
    invS    = inv(X.Sig);
    tmp1    = (1-4*alpha)*eye(k^2) + 2*alpha*(kron(X.Sig.',invS) + kron(invS.',X.Sig));
    tmp2    = X.U'*xi.U + 2*alpha*(xi.Sig/X.Sig - X.Sig\xi.Sig);
    Omeg    = tmp1\tmp2(:);
    Omeg    = reshape(Omeg,k,k);
    Omeg    = Omeg-herm(Omeg); % not mathematically needed, to avoid numerical errors
    xih.U   = xi.U - X.U*Omeg;
    xih.Sig = xi.Sig + Omeg*X.Sig - X.Sig*Omeg;
end


function PhVec = proj_horizontalVec(X)
    invS = inv(X.Sig);
    tmp1 = (1-4*alpha)*eye(k^2) + 2*alpha*(kron(X.Sig.',invS) + kron(invS.',X.Sig));
    tmp2 = [kron(eye(k),X.U'), 2*alpha*(kron(invS.',eye(k)) + kron(eye(k),invS))];
    tmp3 = [-kron(eye(k),X.U); kron(X.Sig.',eye(k)) - kron(eye(k),X.Sig)];
    tmp4 = eye(p*k+k^2)  + tmp3*tmp1*tmp2;
    %
    A = tmp4(1:p*k,1:p*k);
    B = tmp4(1:p*k,p*k+1:p*k+k^2);
    C = tmp4(p*k+1:p*k+k^2,1:p*k);
    D = tmp4(p*k+1:p*k+k^2,p*k+1:p*k+k^2);
    %
    PhVec=[hessVectorizeComplex(A),hessVectorizeComplex(B);hessVectorizeComplex(C),hessVectorizeComplex(D)];
end


M.tangent = M.proj;


M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    rgrad.U   = egrad.U - X.U*egrad.U'*X.U;
    tmp       = trace(herm(egrad.Sig)*X.Sig)/k*X.Sig;
    rgrad.Sig = 1/alpha*(X.Sig*herm(egrad.Sig)*X.Sig - tmp) + 1/(alpha+k*beta)*tmp;
    rgrad.Sig = rgrad.Sig - diag(imag(diag(rgrad.Sig))); 
end

M.ehess2rhess = @ehess2rhess;
function rhess = ehess2rhess(X, egrad, ehess, xi)
    rhess.U = ehess.U - X.U*(ehess.U'*X.U) - X.U*skew(egrad.U'*xi.U)...
        - skew(xi.U*egrad.U')*X.U -.5*(eye(p)-X.U*X.U')*xi.U*X.U'*egrad.U;
    rhess.Sig = 1/alpha*(X.Sig*herm(ehess.Sig)*X.Sig + herm(X.Sig*herm(egrad.Sig)*xi.Sig))...
        - beta/(alpha*(alpha+k*beta))*trace(ehess.Sig*X.Sig + egrad.Sig*xi.Sig)*X.Sig;
    rhess = proj_horizontal(X,rhess);
end

M.ehessVec2rhessVec = @ehessVec2rhessVec;
function rhessVec=ehessVec2rhessVec(X,egrad,ehessVec)
    K_pk = commutationMatrix(p,k);
    K_kk = commutationMatrix(k,k);
    %
    S    = 1/(2*alpha)*kron((X.Sig).',X.Sig);  
    tmp1 = [eye(2*p*k)-hessVectorizeComplexConj(kron((X.U).',X.U)*K_pk), zeros(2*p*k,2*k^2); 
        zeros(2*k^2,2*p*k), hessVectorizeComplex(S)+hessVectorizeComplexConj(S*K_kk)-hessVectorizeComplex(beta/alpha/(alpha+k*beta)*X.Sig(:)*X.Sig(:)')];
    %
    tmp2 = -.5*hessVectorizeComplex(kron(eye(k),X.U*egrad.U') + kron((egrad.U'*X.U).',eye(p)) + kron((X.U'*egrad.U).',eye(p)-X.U*X.U')) ...
        +.5*hessVectorizeComplexConj((kron(egrad.U.',X.U) + kron(X.U.',egrad.U))*K_pk);
    tmp3 = hessVectorizeComplex(1/(2*alpha)*(kron(eye(k),X.Sig*herm(egrad.Sig)) + kron((herm(egrad.Sig)*X.Sig).',eye(k)))...
        - beta/alpha/(alpha+k*beta)*X.Sig(:)*egrad.Sig(:)');
    tmp4 = [tmp2,zeros(2*p*k,2*k^2);zeros(2*k^2,2*p*k),tmp3];
    % Riemannian hessian on the product
    rhessVec = tmp1*ehessVec + tmp4;
    % projection on horizontal space to get hessian on the quotient
    PhVec = proj_horizontalVec(X);
    rhessVec = PhVec*rhessVec;
end



M.retr = @retraction;
function Y = retraction(X,xi,t)
    if nargin<3
        t=1.0;
    end
    
    Omeg = X.U'*xi.U;
    [Q,R] = qr((eye(p)-X.U*X.U')*xi.U,0);
    tmp_xi = [Omeg, -R'; R, zeros(k)];
    tmp    = eye(2*k) + t*tmp_xi + .5*(t^2)*tmp_xi*tmp_xi;
    [U,~,V] = svd(tmp,0);
    tmp = U*V';
    tmp = [X.U,Q]*tmp;
    Y.U = tmp(:,1:k);
    
    Y.Sig = herm(X.Sig + t*xi.Sig + .5*(t^2)*xi.Sig*(X.Sig\xi.Sig));
    
%     while rcond(Y.Sig)<1e-10 || isnan(rcond(Y.Sig)) || isnan(rcond(Y.U'*Y.U))
%         t = t/2;
%         tmp    = eye(2*k) + t*tmp_xi + .5*(t^2)*tmp_xi*tmp_xi;
%         [U,~,V] = svd(tmp,0);
%         tmp = U*V';
%         tmp = [X.U,Q]*tmp;
%         Y.U = tmp(:,1:k);
%         Y.Sig = herm(X.Sig + t*xi.Sig + .5*(t^2)*xi.Sig*(X.Sig\xi.Sig));
%     end
end


M.exp = @exponential;
function Y = exponential(X, xi, t)
    if nargin < 3
        t = 1.0;
    end
    Omeg  = X.U'*xi.U;
    [Q,R] = qr((eye(p)-X.U*X.U')*xi.U,0);
    Y.U   = [X.U,Q]*expm(t*[Omeg,-R';R,zeros(k)])*[eye(k);zeros(k)];
    
    [U,D] = svd(X.Sig);
    sqrtS = U*diag(diag(D).^.5)*U';
    Y.Sig = herm(sqrtS*expm(t*(sqrtS\xi.Sig/sqrtS))*sqrtS);
    
    
%     while isnan(rcond(Y.U'*Y.U)) || rcond(Y.Sig)<1e-6 || isnan(rcond(Y.Sig)) || norm(Y.Sig)<1e-6
%         t = t/2;
%         Omeg  = X.U'*xi.U;
%         [Q,R] = qr((eye(p)-X.U*X.U')*xi.U,0);
%         Y.U   = [X.U,Q]*expm(t*[Omeg,-R';R,zeros(k)])*[eye(k);zeros(k)];
%         [U,D] = svd(X.Sig);
%         sqrtS = U*diag(diag(D).^.5)*U';
%         Y.Sig = herm(sqrtS*expm(t*(sqrtS\xi.Sig/sqrtS))*sqrtS);
%     end
    
    % make sure Y.U orthogonal, not mathematically needed
    [Q,~,] = qr(Y.U,0);
    Y.U = Q;    
end


M.transp = @transport;
function eta2 = transport(x1,x2,eta1)
%     % orthogonal space to x1.U
%     [U1_,~,~] = svd(eye(n)-x1.U*x1.U');
%     U1_ = U1_(:,1:n-p);
%     % orthogonal space to x2.U
%     [U2_,~,~] = svd(eye(n)-x2.U*x2.U');
%     U2_ = U2_(:,1:n-p);
%     %
%     Omeg = x1.U'*eta1.U;
%     K    = U1_'*eta1.U;
%     eta2.U = x2.U*Omeg + U2_*K;
%     
%     [V1,L1] = svd(x1.Sig);
%     invsqrtS1 = V1*diag(diag(L1).^(-0.5))*V1';
%     %
%     [V2,L2] = svd(x2.Sig);
%     sqrtS2 = V2*diag(diag(L2).^(0.5))*V2';
%     %
%     eta2.Sig = sqrtS2*(invsqrtS1*eta1.Sig*invsqrtS1)*sqrtS2;
%     
% %     tmp = lincomb(x2,1,eta2,-1,proj_tangent(x2,eta2));
% %     disp(inner(x2,tmp,tmp));
%     
%     eta2 = projection(x2,eta2); % seems mathematically needed
    eta2 = projection(x2,eta1);
end



M.hash = @(X) ['z' hashmd5([X.U(:) ; X.Sig(:)])];

M.rand = @random;
function X = random()
    [X.U,~] = qr(randn(p,k) + 1i*randn(p,k), 0);  
    tmp = orth(randn(k) +1i*randn(k));
    X.Sig   = tmp*diag(randn(1,k).^2)*tmp';
end

M.randvec = @(X) randomvec(X,M.norm);
function xi = randomvec(X,normfun)
    % random vector on the tangent space
    xi.U   = randn(p,k) + 1i*randn(p,k);
    xi.Sig = randn(k) + 1i*randn(k);
    xi     = projection(X, xi);
    nrm    = normfun(X, xi);
    xi.U   = xi.U / nrm;
    xi.Sig = xi.Sig / nrm;
end

M.lincomb = @lincomb;
function d = lincomb(x, a1, d1, a2, d2)
    
    if nargin == 3
        d.U   = a1*d1.U;
        d.Sig = a1*d1.Sig;
    elseif nargin == 5
        d.U   = a1*d1.U + a2*d2.U;
        d.Sig = a1*d1.Sig + a2*d2.Sig;
    else
        error('Bad use of herm_psd_fixedrank_factory.lincomb.');
    end
end

M.zerovec = @(X) struct('U', zeros(p,k), 'Sig', zeros(k));

% vec and mat are not isometries, because of the scaled inner metric
M.vec = @(X, Z) [real(Z.U(:));imag(Z.U(:)); real(Z.Sig(:)); imag(Z.Sig(:))];
M.mat = @(X, z) struct('U', reshape(z(1:(p*k)), p, k) + 1i*reshape(z((p*k+1):(2*p*k)),p,k),...
                       'Sig', reshape(z((2*p*k+1):(2*p*k+k^2)), k, k) + 1i*reshape(z((2*p*k+k^2+1):end),k,k));
M.vecmatareisometries = @() false;


end