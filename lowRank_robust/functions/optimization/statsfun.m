function stats = statsfun(problem, x, stats)
% function to get some data on optimization
    stats.x = x;
    stats.R = eye(size(x.U,1)) + x.U*x.Sig*x.U';
end