function [R,R_it] = Tyler3( X,R0,tol,maxit )
%TYLER Summary of this function goes here
%   Detailed explanation goes here

%% Variables
nitermax = maxit;
[N,K] = size(X);
err = inf;


%% Init
R           = R0;
R_it        = zeros(N,N,nitermax);
R_it(:,:,1) = R;
niter       = 0;

%% Iterations
while err>tol && niter<nitermax
%     v = chol(R)' \ X;
     [Ur,Lr]=svd(R);
     cR=Ur*diag(diag(Lr).^(0.5));
     v = cR \ X;
    a = mean(v.*conj(v));
    y = X./ sqrt( a(ones(N,1),:) );

    
    Rnew =  y*y' / K;
    Rnew = Rnew / (sum(diag(Rnew)));
    % Criterion
    err = norm(Rnew-R,'fro')/norm(R,'fro');
    R=Rnew;
    niter = niter+1;
    R_it(:,:,niter+1) = Rnew;
end
% R=R./(2*norm(R,'fro')); % Pour changer de normalisation, sinon a commenter

if niter == nitermax 
    disp('bordel 3')
end

end

