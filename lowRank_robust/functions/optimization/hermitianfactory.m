function M = hermitianfactory(n)
    
    

    M.name = @() sprintf('Euclidean space of Hermitian matrices)');
    
    M.dim = @() n^2;
    
    M.inner = @(x, d1, d2) real(d1(:)'*d2(:)); % real not needed in theory
    
    M.norm = @(x, d) norm(d(:), 'fro');
    
    M.dist = @(x, y) norm(x(:)-y(:), 'fro');
    
    M.typicaldist = @() sqrt(prod(dimensions_vec));
    
    M.proj = @(x, d) .5*(d+d');
    
    M.egrad2rgrad = @(x, g) .5*(g+g');
    
    M.ehess2rhess = @(x, eg, eh, d) .5*(eh+eh');
    
    M.tangent = M.proj;
    
    M.exp = @exp;
    function y = exp(x, d, t)
        if nargin == 3
            y = x + t*d;
        else
            y = x + d;
        end
    end
    
    M.retr = M.exp;
	
	M.log = @(x, y) y-x;

    M.hash = @(x) ['z' hashmd5([real(x(:)) ; imag(x(:))])];
    
    M.rand = @random;
    function x = random()
        x = randn(n) + 1i*randn(n);
%         x = .5*(x+x');
        x = x*x';
    end
    
    M.randvec = @randvec;
    function u = randvec(x)
        u = randn(n) + 1i*randn(n);
        u = .5*(u+u');
        u = u / norm(u(:), 'fro');
    end
    
    M.lincomb = @matrixlincomb;
    
    M.zerovec = @(x) zeros(2*n^2);
    
    M.transp = @(x1, x2, d) d;
    
    M.pairmean = @(x1, x2) .5*(x1+x2);
    
%     sz = prod(dimensions_vec);
    M.vec = @(x, u_mat) [real(u_mat(:)) ; imag(u_mat(:))];
    M.mat = @(x, u_vec) reshape(u_vec(1:n^2), [n,n]) ...
                        + 1i*reshape(u_vec(n^2+1:end), [n,n]);
%     M.vecmatareisometries = @() true;

end
