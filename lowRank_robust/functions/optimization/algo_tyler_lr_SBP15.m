function [theta,theta_it] = algo_tyler_lr_SBP15(x,theta0,tol,maxit)

[p,n] = size(x);
k     = size(theta0.U,2);
I     = eye(p);
%
theta_it(1)         = theta0;
theta_it(maxit+1) = struct('U',[],'Sig',[]);
%
R    = I + theta0.U*theta0.Sig*theta0.U';
crit = 1;
it   = 0;
while (crit>tol)&&(it<maxit)
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
    a       = mean(v.*conj(v));
    y       = x./ sqrt( a(ones(p,1),:) );

    M     =  y*y' / n;
    [U,L] = svd(M);
    Ll    = diag(L);
    sig   = sum(Ll(k+1:p))/(p-k);
    P     = Ll(1:k) - sig;
    A     = U(:,1:k);
    
    theta = struct('U',A,'Sig',diag(P)/sig);
    H     = A*diag(P)*A'/sig;
    Rnew  = I + H;
    
    crit           = norm(Rnew-R,'fro')/norm(R,'fro');
    R              = Rnew;
    it             = it+1;
    theta_it(it+1) = theta;
end

theta_it = theta_it(1:it+1);

