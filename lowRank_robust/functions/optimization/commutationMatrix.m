function K=commutationMatrix(m,n)
% Computes the commutation matrix K, such that for all m*n
% matrix A, vec(A.') = K*vec(A).

% compute basis vectors
em = zeros(m,m);
for i=1:m
    em(i,i)   = 1;
end
en = zeros(n,n);
for j=1:n
    en(j,j)   = 1;
end

% compute K
K = zeros(m*n);
for i=1:m
    for j=1:n
        K = K + kron(em(:,i)*en(:,j)',en(:,j)*em(:,i)');
    end
end