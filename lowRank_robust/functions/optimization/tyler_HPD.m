function [costfun,gradfun,hessfun,hessVecfun]=tyler_HPD(x)
    costfun    = @(R)    aux_cost(R,x);
    gradfun    = @(R)    aux_grad(R,x);
    hessfun    = @(R,xi) aux_hess(R,xi,x);
    hessVecfun = @(R)    aux_hessVec(R,x);
end

function cost=aux_cost(R,x)
    [p,n]   = size(x);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
    a       = real(sum(v.*conj(v)));
    %
    cost    = n*log(real(det(R))) + p*sum(log(a));
    cost = cost/p/n;
end

function grad=aux_grad(R,x) % ok
    [p,n] = size(x);
    psi   = aux_psi(R,x); 
    grad  = R\(n*R - p*psi)/R;
    grad  = grad/p/n;
end

function hess=aux_hess(R,xi,x) % ok
    [p,n] = size(x);
    psi  = aux_psi(R,x);
    Dpsi = aux_Dpsi(R,xi,x);
    hess = R\(p*((psi/R)*xi + xi*(R\psi) - Dpsi)-n*xi)/R;
    hess = hess/p/n;
end

function hessVec = aux_hessVec(R,x) % ok
    [p,n]   = size(x);
    psi     = aux_psi(R,x);
    DpsiVec = aux_DpsiVec(R,x);
    tmp1    = kron(R.',R);
    tmp2    = p*(kron((R\psi).',eye(p)) + kron(eye(p),psi/R) - DpsiVec) - n*eye(p^2);
    hessVec = tmp1\tmp2;
    hessVec = hessVectorizeComplex(hessVec/p/n);
end

%%%
function psi = aux_psi(R,x) % ok
    p       = size(x,1);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
    a       = sum(v.*conj(v));
    y       = x./ sqrt(a(ones(p,1),:));
    psi     = y*y';
end

function Dpsi = aux_Dpsi(R,xi,x) % ok
    [p,n]   = size(x);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v1      = cR * x;
    
    tmp = R\xi/R;
    Dpsi = zeros(p);
    for i=1:n
        tmp1 = x(:,i)'*tmp*x(:,i);
        tmp2 = v1(:,i)'*v1(:,i);
        Dpsi = Dpsi + x(:,i)*x(:,i)'*tmp1/(tmp2^2);
    end
end

function DpsiVec = aux_DpsiVec(R,x) % ok
    [p,n]   = size(x);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
    a       = sum(v.*conj(v));
    y       = x./sqrt(a(ones(p,1),:));
    %
    R_inv   = Ur*diag(diag(Lr).^(-1))*Ur';
    y_      = R_inv*y;
    %
    DpsiVec = zeros(p^2);
    for i=1:n
        tmp1    = y(:,i)*y(:,i)';
        tmp1    = tmp1(:);
        tmp2    = y_(:,i)*y_(:,i)';
        tmp2    = tmp2(:);
        DpsiVec = DpsiVec + tmp1*tmp2';
    end
end