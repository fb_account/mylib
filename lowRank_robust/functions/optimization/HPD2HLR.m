function [costfun,gradfun,hessfun,hessVecfun]=HPD2HLR(costfun_HPD,gradfun_HPD,hessfun_HPD,hessVecfun_HPD)
    costfun    = @(theta)    aux_cost(theta,costfun_HPD);
    gradfun    = @(theta)    aux_grad(theta,gradfun_HPD);
    hessfun    = @(theta,xi) aux_hess(theta,xi,gradfun_HPD,hessfun_HPD);
    hessVecfun = @(theta)    aux_hessVec(theta,gradfun_HPD,hessVecfun_HPD);
end

function cost=aux_cost(theta,costfun_HPD) % ok
    p    = size(theta.U,1);
    R    = eye(p) + theta.U*theta.Sig*theta.U';
    cost = costfun_HPD(R);
end

function grad=aux_grad(theta, gradfun_HPD) % ok
    p      = size(theta.U,1);
    R      = eye(p) + aux_phi(theta);
    grad_R = gradfun_HPD(R);
    grad   = struct('U',2*grad_R*theta.U*theta.Sig,'Sig',theta.U'*grad_R*theta.U);
end

function hess=aux_hess(theta,xi,gradfun_HPD,hessfun_HPD) % ok
    p        = size(theta.U,1);
    R        = eye(p) + aux_phi(theta);
    Dphi     = aux_Dphi(theta,xi);
    grad_HPD = gradfun_HPD(R);
    hess_HPD = hessfun_HPD(R,Dphi);
    hess.U   = 2*hess_HPD*theta.U*theta.Sig + 2*grad_HPD*(xi.U*theta.Sig+theta.U*xi.Sig);
    hess.Sig = theta.U'*hess_HPD*theta.U + theta.U'*grad_HPD*xi.U + xi.U'*grad_HPD*theta.U;
end

function hessVec = aux_hessVec(theta,gradfun_HPD,hessVecfun_HPD) % ok
    [p,k]       = size(theta.U);
    K_pk        = commutationMatrix(p,k);
    R           = eye(p) + aux_phi(theta);
    DphiVec     = aux_DphiVec(theta);
    grad_HPD    = gradfun_HPD(R);
    hessVec_HPD = hessVecfun_HPD(R);
    tmp1        = [2*hessVectorizeComplex(kron(theta.Sig.',grad_HPD)), 2*hessVectorizeComplex(kron(eye(k),grad_HPD*theta.U));...
        hessVectorizeComplex(kron(eye(k),theta.U'*grad_HPD))+hessVectorizeComplexConj(kron((grad_HPD*theta.U).',eye(k))*K_pk), zeros(2*k^2)];
    tmp2        = [2*hessVectorizeComplex(kron((theta.U*theta.Sig).',eye(p))); hessVectorizeComplex(kron((theta.U).',theta.U'))] * hessVec_HPD * DphiVec;
    hessVec     = tmp1 + tmp2;
end


%%%
function phi=aux_phi(theta) % ok
    phi = theta.U*theta.Sig*theta.U';
end

function Dphi=aux_Dphi(theta,xi) % ok
    Dphi = theta.U*theta.Sig*xi.U' + xi.U*theta.Sig*theta.U' + theta.U*xi.Sig*theta.U';
end

function DphiVec=aux_DphiVec(theta) % ok
    [p,k]   = size(theta.U);
    K_pk    = commutationMatrix(p,k);
    tmp1    = kron((theta.Sig*theta.U').',eye(p));
    tmp2    = kron(eye(p),theta.U*theta.Sig)*K_pk;
    tmp3    = kron(conj(theta.U),theta.U);
    DphiVec = [hessVectorizeComplex(tmp1)+hessVectorizeComplexConj(tmp2), hessVectorizeComplex(tmp3)];
end

