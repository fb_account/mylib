function L=hessVectorizeComplexConj(A)
% some description on what it allows to do: vectorize Hessians in the
% complex case, when dealing with a conjugate.

RA = real(A);
IA = imag(A);
%
L  = [RA,IA;IA,-RA];