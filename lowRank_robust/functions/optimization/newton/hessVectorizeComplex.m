function L = hessVectorizeComplex(A)
% some description on what it allows to do: vectorize Hessians in the
% complex case.

RA = real(A);
IA = imag(A);
%
L  = [RA,-IA;IA,RA];