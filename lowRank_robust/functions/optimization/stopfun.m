function stopnow = stopfun(problem,x,info,last)
% stopping criterion function
    R = eye(size(x.U,1)) + x.U*x.Sig*x.U';
    if isfield(info,'accepted')
        stopnow = (last>=2 && info(last).accepted && norm(info(last-1).R-R,'fro')/norm(info(last-1).R,'fro') < problem.tol); % ### ok
    else
        stopnow = (last>=2 && norm(info(last-1).R-R,'fro')/norm(info(last-1).R,'fro') < problem.tol); % ### ok
    end
end