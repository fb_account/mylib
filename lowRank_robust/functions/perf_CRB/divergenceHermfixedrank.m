function [dist2,dist2U,dist2Lambda] = divergenceHermfixedrank(theta1,theta2,alpha,beta)
%% Attention c'est la distance au carre

U1 = theta1.U;
U2 = theta2.U;
Lambda1 = theta1.Sig;
Lambda2 = theta2.Sig;

k=1;

[U,~,V]=svd(U1'*U2);

%% Calcul partie sous espace
% dist2U=norm(real(acos(svd(U1'*U2))))^2;


[O1,cosTheta,O2] = svd(theta1.U'*theta2.U);

% orthogonal components
Theta   = real(acos(diag(cosTheta)));
dist2U = sum(Theta.^2);

%% Calcul partie matrice symetrique
% lambda = log(eig((U'*Lambda1*U),(V'*Lambda2*V)));
% dist2Lambda =  k*(real(alpha*sum(lambda.^2)+beta*sum(lambda)^2)) ; 

[U,L]        = svd(theta1.Sig);
Sig1_invsqrt = U*diag(diag(L).^-.5)*U';
%
Sig2_transf  = O1*O2'*theta2.Sig*O2*O1';
%
L = svd(Sig1_invsqrt*Sig2_transf*Sig1_invsqrt);
dist2Lambda = alpha*sum(log(L).^2) + beta*sum(log(L))^2;

        
%% Finalement
dist2 = dist2Lambda+dist2U;
        
end


% function div = divergenceHermfixedrank(theta1,theta2,alpha,beta)
% 
% 
% [O1,cosTheta,O2] = svd(theta1.U'*theta2.U);
% 
% % orthogonal components
% Theta   = real(acos(diag(cosTheta)));
% dist_Gr = sum(Theta.^2);
% 
% % HPD components
% [U,L]        = svd(theta1.Sig);
% Sig1_invsqrt = U*diag(diag(L).^-1)*U';
% %
% Sig2_transf  = O1*O2'*theta2.Sig*O2*O1';
% %
% L = svd(Sig1_invsqrt*Sig2_transf*Sig1_invsqrt);
% dist_HPD = alpha*sum(log(L).^2) + beta*sum(log(L))^2;
% 
% 
% div = dist_Gr+dist_HPD;
% 
% end