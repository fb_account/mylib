function e_U = basisComplexStiefel(U)
    % returns an orthonormal basis of the tangent space of U according to the
    % canonical metric.
    % It contains (2*p-k)*k elements.

    [p,k]  = size(U);

    [U_,~] = svd(eye(p)-U*U');
    U_     = U_(:,1:p-k);


    e1_tmp = basisC_pk(p-k,k);
    e2_tmp = basisSkewHermitian(k);

    e_U    = zeros(p,k,(2*p-k)*k);
    %
    for t=1:2*(p-k)*k
        e_U(:,:,t) = U_*e1_tmp(:,:,t);
    end
    %
    for t=1:k^2
        e_U(:,:,t+2*(p-k)*k) = U*e2_tmp(:,:,t);
    end
end

%% auxilary functions

function e = basisC_pk(p,k)
    % Returns a basis of complex matrices of size p*k.
    % This basis contains 2*p*k matrices: the p*k first are real and the p*k
    % last are imaginary.

    e = zeros(p,k,2*p*k);
    t = 1;
    % real
    for i=1:p
        for j=1:k
            e(i,j,t) = 1;
            t        = t+1;
        end
    end
    % imaginary
    for i=1:p
        for j=1:k
            e(i,j,t) = 1i;
            t        = t+1;
        end
    end
end


function e = basisSkewHermitian(k)
    % Returns a basis of skew-hermitian matrices of size k*k.
    % This basis contains k^2 matrices: the k*(k-1)/2 first are real and the
    % k*(k+1)/2 last are imaginary.
    
    e = zeros(k,k,k^2);
    t = 1;
    % off-diagonal (real)
    for i=1:k-1
        for j=i+1:k
            e(i,j,t) = 1;
            e(j,i,t) = -1;
            t        = t+1;
        end
    end
    % off-diagonal (imaginary)
    for i=1:k-1
        for j=i+1:k
            e(i,j,t) = 1i;
            e(j,i,t) = 1i;
            t        = t+1;
        end
    end
    % diagonal (imaginary)
    for i=1:k
        e(i,i,t) = 1i*sqrt(2);
        t        = t+1;
    end
end