function F=fisherInformationMatrix_CESlowrank(theta,alpha,beta)
% The Fisher information matrix is given up to the factor n (number of
% samples).

e_M      = basisHermfixedrank(theta,alpha,beta);
dimBasis = length(e_M);
%
F        = zeros(dimBasis);
for i=1:dimBasis
    for j=1:dimBasis
        F(i,j) = fisherInformationMetric_CESlowrank(theta,e_M(i),e_M(j),alpha,beta);
    end
end

end