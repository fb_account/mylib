function g=fisherInformationMetric_CESlowrank(theta,xi,eta,alpha,beta)
    p        = size(theta.U,1);
    phi      = aux_phi(theta);
    Dphi_xi  = aux_Dphi(theta,xi);
    Dphi_eta = aux_Dphi(theta,eta);
    %
    g = alpha*real(trace(((eye(p)+phi)\Dphi_xi)*((eye(p)+phi)\Dphi_eta))) + beta*real(trace((eye(p)+phi)\Dphi_xi))*real(trace((eye(p)+phi)\Dphi_eta));
end

%% auxilary functions
function phi=aux_phi(theta) % ok
    phi = theta.U*theta.Sig*theta.U';
end

function Dphi=aux_Dphi(theta,xi) % ok
    Dphi = theta.U*theta.Sig*xi.U' + xi.U*theta.Sig*theta.U' + theta.U*xi.Sig*theta.U';
end