function e_M = basisHermfixedrank(theta,alpha,beta)

[p,k] = size(theta.U);

e_U   = basisComplexStiefel(theta.U);
e_Sig = basisHPD(theta.Sig,alpha,beta);


for i=1:size(e_U,3)
    e_M(i) = struct('U',e_U(:,:,i),'Sig',zeros(k));
end
for i=1:size(e_Sig,3)
    e_M(i+size(e_U,3)) = struct('U',zeros(p,k),'Sig',e_Sig(:,:,i));
end