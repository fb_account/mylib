function dist=distanceGrassman(U1,U2)
% U1 and U2 should be p*k orthogonal matrices

cosTheta = svd(U1'*U2);
Theta = real(acos(cosTheta));

dist = sqrt(sum(Theta.^2));

end