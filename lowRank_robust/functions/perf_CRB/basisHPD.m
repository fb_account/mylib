function e_Sig=basisHPD(Sig,alpha,beta)
    % returns an orthonormal basis of the tangent space of Sig according to
    % the affine invariant metric.
    % It contains k^2 elements.

    k     = size(Sig,1);
    [U,L] = svd(Sig);
    Sig_sqrt = U*diag(diag(L).^.5)*U';

    e_tmp = basisHermitian(k);

    e_Sig = zeros(k,k,k^2);
    for t=1:k^2
        e_Sig(:,:,t)=1/sqrt(alpha)*(Sig_sqrt*e_tmp(:,:,t)*Sig_sqrt)...
            + (sqrt(alpha)-sqrt(alpha+k*beta))/(k*sqrt(alpha)*sqrt(alpha+k*beta))*trace(e_tmp(:,:,t))*Sig;
    end
end



%% auxilary function

function e = basisHermitian(k)
    % Returns a basis of hermitian matrices of size k*k.
    % This basis contains k^2 matrices: the k*(k+1)/2 first are real and the
    % k*(k-1)/2 last are imaginary.

    e = zeros(k,k,k^2);
    t = 1;
    % diagonal (real)
    for i=1:k
        e(i,i,t) = 1;
        t        = t+1;
    end
    % off-diagonal (real)
    for i=1:k-1
        for j=i+1:k
            e(i,j,t) = 1/sqrt(2);
            e(j,i,t) = 1/sqrt(2);
            t        = t+1;
        end
    end
    % off-diagonal (imaginary)
    for i=1:k-1
        for j=i+1:k
            e(i,j,t) = 1i/sqrt(2);
            e(j,i,t) = -1i/sqrt(2);
            t        = t+1;
        end
    end
end