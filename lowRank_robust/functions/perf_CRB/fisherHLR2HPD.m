function P = fisherHLR2HPD(theta,alpha,beta)

[p,k] = size(theta.U);

% basis for theta
e_theta    = basisHermfixedrank(theta,alpha,beta);
dimBasis_theta   = length(e_theta); % = 2*p*k;
dimAmbient = 2*p*k+2*k^2;
% matrix with basis elements vectorized
E_theta = zeros(dimAmbient,dimBasis_theta);
for i=1:dimBasis_theta
    E_theta(:,i) = [real(e_theta(i).U(:));imag(e_theta(i).U(:));real(e_theta(i).Sig(:));imag(e_theta(i).Sig(:))];
end

% basis for R
R          = eye(p)+aux_phi(theta);
e_R        = basisHPD(R,alpha,beta);
dimBasis_R   = length(e_R); % = p^2
dimAmbient = 2*p^2;
% matrix with basis elements vectorized
E_R = zeros(dimAmbient,dimBasis_R);
for i=1:dimBasis_R
    tmp = e_R(:,:,i);
    E_R(:,i) = [real(tmp(:));imag(tmp(:))];
end

% change of coordinate from theta to R
% DphiVec = aux_DphiVec(theta);
% P = E_R'*DphiVec*E_theta;

% % basis check
% metricBasis = zeros(dimBasis_R);
% for i=1:dimBasis_R
%     for j=1:dimBasis_R
%         metricBasis(i,j) = aux_metric(R,e_R(:,:,j),e_R(:,:,i),alpha,beta);
%     end
% end
% % this should be very close to zero
% disp(norm(metricBasis-eye(dimBasis_R),'fro')/norm(eye(dimBasis_R),'fro'));

P = zeros(dimBasis_R,dimBasis_theta);
for i=1:dimBasis_R
    for j=1:dimBasis_theta
        P(i,j) = aux_metric(R,aux_Dphi(theta,e_theta(j)),e_R(:,:,i),alpha,beta);
    end
end

end

function metric=aux_metric(R,xi,eta,alpha,beta)
    metric = real(alpha*trace((R\xi)*(R\eta)) + beta*trace(R\xi)*trace(R\eta));
end

function phi=aux_phi(theta) % ok
    phi = theta.U*theta.Sig*theta.U';
end

function Dphi=aux_Dphi(theta,xi) % ok
    Dphi = theta.U*theta.Sig*xi.U' + xi.U*theta.Sig*theta.U' + theta.U*xi.Sig*theta.U';
end

function DphiVec=aux_DphiVec(theta) % ok
    [p,k]   = size(theta.U);
    K_pk    = commutationMatrix(p,k);
    tmp1    = kron((theta.Sig*theta.U').',eye(p));
    tmp2    = kron(eye(p),theta.U*theta.Sig)*K_pk;
    tmp3    = kron(conj(theta.U),theta.U);
    DphiVec = [hessVectorizeComplex(tmp1)+hessVectorizeComplexConj(tmp2), hessVectorizeComplex(tmp3)];
end