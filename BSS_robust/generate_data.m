%% some parameters
n     = 10;                          % matrix size
K     = 30;                          % number of covariance matrices
T_all = [15,25,50,75,100,500,1000];  % sample sizes
T_max = T_all(end);                  % maximum sample size
nIt   = 100;                         % number of repetitions
d     = 3;                           % degrees of freedom of the t-distribution
condA = 10;                          % conditionings of the mixing matrix

%% generate covariance matrices

% generate source powers
Lambda = randn(n,K).^2;
% mixing matrix with fixed conditionning
tmp = randn(n);
[U,~,V] = svd(tmp);
l_min = 1/sqrt(condA);
l_max = sqrt(condA);
l = (l_max-l_min).*rand(n-2,1) + l_min;
l_all = [l_min;l_max;l];
L = diag(l_all(randperm(n)));
A = U*L*V';
% more conditionning checks
for k=1:K
    while cond(A*diag(Lambda(:,k))*A') > 1e4
        Lambda(:,k) = randn(n,1).^2;
    end
end
tmp = sum(Lambda,2);
% BSSfactory structure
theta = struct('A',A,'Lambda',Lambda./tmp);

%% draw random samples from covariance matrices
x_all = zeros(n,T_max,K,nIt);
for k=1:K
    C     = theta.A*diag(theta.Lambda(:,k))*theta.A';
    [~,p] = chol(C);
    if p~=0
        disp(['generate_data: C(:,:,' int2str(k) ') not positive definite']);
    end
    R = chol(C)';
    for it=1:nIt
        tmp = mvtrnd(eye(n),d,T_max)';
        x_all(:,:,k,it) = R*tmp;
%         x_all(:,:,k,it) = gene_tdistrib_real(n,T_max,C,d);
    end
end

%% save data
save('../results/data_BSS','n','K','T_all','nIt','d','condA','theta','x_all');