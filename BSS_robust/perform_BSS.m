%% load data
load('../results/data_BSS.mat');

%% solvers parameters
tol   = 1e-6;
maxit = 1000;


% Tyler
[cost_tyler_SPD,grad_tyler_SPD] = tyler_SPD();
[cost_tdist_SPD,grad_tdist_SPD] = MLE_tdist_SPD(d);
man                             = BSSfactory(n,K);
options                         = struct('maxiter',maxit,'verbosity',0);%,'statsfun',@statsfun,'stopfun',@stopfun);
solvers                         = {@conjugategradient};%,@trustregions};

warning('off', 'manopt:getHessian:approx');

% solutions structure
theta_all = cell(1+length(solvers),length(T_all),nIt,length(condA));

meth = ["jadiag","tyler"];

%% perform BSS

for it=1:nIt
    fprintf(['it=' int2str(it) '    T= ']);
    for tix=1:length(T_all)
        T = T_all(tix);
        fprintf([int2str(T_all(tix)) ' ']);
        % select data
        x = squeeze(x_all(:,1:T_all(tix),:,it));


        % compute SCMs
        C = zeros(n,n,K);
        for k=1:K
            C(:,:,k) = x(:,:,k)*x(:,:,k)'/T;%_all(tix);
        end
        L = chol(mean(C,3),'lower');

        
        % BSS through AJD with jadiag
        opt_jadiag    = struct('B0',inv(L),'weights',ones(1,K),'tol',tol,'maxiter',maxit);
        B_jadiag      = m_jadiag(C,opt_jadiag);
        A_jadiag      = inv(B_jadiag);
        Lambda_jadiag = zeros(n,K);
        for k=1:K
            Lambda_jadiag(:,k) = diag(B_jadiag*C(:,:,k)*B_jadiag');
        end
        theta_jadiag         = struct('A',A_jadiag,'Lambda',Lambda_jadiag);
        theta_all{1,tix,it}  = theta_jadiag;
            

        % BSS with Tyler or MLE t-dist
%         [costfun,gradfun] = SPD2BSS(x,cost_tyler_SPD,grad_tyler_SPD);
        [costfun,gradfun] = SPD2BSS(x,cost_tdist_SPD,grad_tdist_SPD);
        problem           = struct('M',man,'cost',costfun,'egrad',gradfun,'tol',tol);
        % initialization
        A0      = L;
        Lambda0 = zeros(n,K);
        for k=1:K
            Lambda0(:,k) = diag(A0\C(:,:,k)/A0');
        end
        theta0 = struct('A',A0,'Lambda',Lambda0);
        theta0 = man.projonman(theta0);
        % optimization
        for sol=1:length(solvers)
            solver  = solvers{sol};
            theta_t = solver(problem,theta0,options);
            %
            theta_all{1+sol,tix,it} = theta_t;
        end

            
%         10*log10(critMoAm(theta.A,inv(theta_jadiag.A)))
%         10*log10(BSSerrormeasure(theta,theta_jadiag))
%         10*log10(critMoAm(theta.A,inv(theta_t.A)))
%         10*log10(BSSerrormeasure(theta,theta_t))

    end
    fprintf('\n');
end

%% save results
save('../results/results_BSS','theta_all','meth');
