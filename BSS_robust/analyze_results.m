%% load data
load('../results/data_BSS.mat');
load('../results/results_BSS.mat');

%% compute perf indexes

err_SPD      = zeros(nIt,length(meth),length(T_all));
err_SPD_norm = zeros(nIt,length(meth),length(T_all));
err_MoAm     = zeros(nIt,length(meth),length(T_all));

% normalize det theta
Lambda_norm = zeros(n,K);
for k=1:K
    Lambda_norm(:,k) = theta.Lambda(:,k)/(det(theta.A*diag(theta.Lambda(:,k))*theta.A')^(1/n));
end
theta_norm = struct('A',theta.A,'Lambda',Lambda_norm);
%
for tix=1:length(T_all)
    for mix=1:length(meth)
        for it=1:nIt
            theta_estim = theta_all{mix,tix,it};
            % normalize det theta_estim
            Lambda_norm = zeros(n,K);
            for k=1:K
                Lambda_norm(:,k) = theta_estim.Lambda(:,k)/(det(theta_estim.A*diag(theta_estim.Lambda(:,k))*theta_estim.A')^(1/n));
            end
            theta_estim_norm = struct('A',theta_estim.A,'Lambda',Lambda_norm);

            err_SPD(it,mix,tix)      = BSSerrormeasure(theta,theta_estim);
            err_SPD_norm(it,mix,tix) = BSSerrormeasure(theta_norm,theta_estim_norm);
            err_MoAm(it,mix,tix)     = critMoAm(theta.A,inv(theta_estim.A));
        end
    end
end
% mean perf
err_SPD_mean      = squeeze(10*log10(mean(err_SPD,1)));
err_SPD_norm_mean = squeeze(10*log10(mean(err_SPD_norm,1)));
err_MoAm_mean     = squeeze(10*log10(mean(err_MoAm,1)));

err_all_mean = cat(3,err_SPD_mean,err_SPD_norm_mean,err_MoAm_mean);


%% write in file

err_string = ["SPD","SPD_norm","MA"];

% string array
all_s                = strings(1+length(T_all), 1+length(meth)*3);
all_s(1,1)           = "T";
all_s(2:1+length(T_all),1)      = string(T_all);
%
for eix=1:length(err_string)
    for mix=1:length(meth)
        all_s(1,1+length(meth)*(eix-1)+mix) = strcat(err_string(eix),"_",meth(mix));
    end
end
for eix=1:length(err_string)
    all_s(2:1+length(T_all),1+length(meth)*(eix-1)+1:1+length(meth)*eix) = string(err_all_mean(:,:,eix)');
end

% write in file
filename = '../results/results_BSS.txt';
fileID   = fopen(filename,'w','native','UTF-8');
%
[nLine,nCol] = size(all_s);
for lix=1:nLine
    for cix=1:nCol-1
        if ~ismissing(all_s(lix,cix))
            fprintf(fileID,'%s,',all_s(lix,cix));
        else
            fprintf(fileID,' ,');
        end
    end
    % last column, no separator
    cix = nCol;
    if ~ismissing(all_s(lix,cix))
        fprintf(fileID,'%s',all_s(lix,cix));
    else
        fprintf(fileID,' ');
    end
    fprintf(fileID,'\n');
end
fclose(fileID);

