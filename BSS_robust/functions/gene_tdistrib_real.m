function [Z] = gene_tdistrib_real(M, K, Scatter, d)

%% Scatter's root
C = chol(Scatter)';
% [Ur,Lr]=svd(Scatter);
% C=Ur*diag(diag(Lr).^(0.5))*Ur';
for i=1:M
    C(i,i) = real(C(i,i));
end



    
%% CES representation
% % Uniform distribution on the Complex unit Sphere
% % u =^d y/|y| , with y ~ CN (0 , I )
% u = 1/sqrt(2)*(randn(M,K) + 1i *randn(M,K));
% u = u * diag( 1./sqrt(real(diag( u' * u))) ) ;
% % Second order modualr variate
% % G =^d  chi-squared_M * d / chi-squared_d 
% Gauss_M = 1/sqrt(2)*(randn(M,K) + 1i *randn(M,K));
% Chisquared_M = real(diag( Gauss_M'*Gauss_M ));
% Gauss_d = 1/sqrt(2)*(randn(d,K) + 1i *randn(d,K));
% Chisquared_d = real(diag( Gauss_d'*Gauss_d ));
% Q = d * Chisquared_M ./ Chisquared_d ;
% % Samples
% Z = C * u * diag(sqrt(Q)) ;

%% CG representation
% % Speckle
% % y ~ CN (0 , I )
% y = 1/sqrt(2)*(randn(M,K) + 1i *randn(M,K));
% % Texture
% % Tau =^d  d / chi-squared_d
% Gauss_d = 1/sqrt(2)*(randn(d,K) + 1i *randn(d,K));
% Chisquared_d = real(diag( Gauss_d'*Gauss_d ));
% Tau = d ./ Chisquared_d ;
% % Samples
% Z = C * y * diag(sqrt(Tau)) ;

%% faster CG representation
% Speckle
% y ~ CN (0 , I )
y = randn(M,K);
% Texture
Gauss_d = randn(d,K);
Tau = sqrt( d./ real(sum( Gauss_d.*conj(Gauss_d) ,1 )) );
% Samples
Z =  repmat(Tau,M,1).*(C * y ) ;

end
