function [theta,crit,info,options] = BSSfromJadiag(x,options)

% parameters
[n,T,K] = size(x);

% compute SCMs
C = zeros(n,n,K);
for k=1:K
    C(:,:,k) = x(:,:,k)*x(:,:,k)'/T;
end
% whitening
% L  = chol(mean(C,3),'lower');
% Cw = zeros(n,n,K);
% for k=1:K
%     Cw(:,:,k) = L\C(:,:,k)/L';
% end
Cw = C;
% perform AJD
[Bw,Cout,info,options] = m_jadiag(Cw,options);
% B                   = Bw/L;
B = Bw;
% compute theta
Lambda = zeros(n,K);
for k=1:K
   Lambda(:,k) = diag(B*C(:,:,k)*B');
%     Lambda(:,k) = diag(Cout(:,:,k));
end
A     = inv(B);
theta = struct('A',A,'Lambda',Lambda);

crit = 0;
for k=1:K
    Lambda(:,k) = diag(A\C(:,:,k)/A');
    crit = crit + norm(C(:,:,k) - A*diag(Lambda(:,k))*A','fro')^2/K;
end


end