function [costfun,gradfun] = SPD2BSS(x,costfun_SPD,gradfun_SPD)
    costfun    = @(theta)    aux_cost(theta,x,costfun_SPD);
    gradfun    = @(theta)    aux_grad(theta,x,gradfun_SPD);
end

function cost=aux_cost(theta,x,costfun_SPD)
    K    = size(theta.Lambda,2);
    cost = 0;
    for k=1:K
        C    = theta.A*diag(theta.Lambda(:,k))*theta.A';
        cost = cost + costfun_SPD(C,x(:,:,k))/K;
    end
end

function grad=aux_grad(theta,x,gradfun_SPD)
    K    = size(theta.Lambda,2);
    grad = struct('A',zeros(size(theta.A)),'Lambda',zeros(size(theta.Lambda)));
    for k=1:K
        C                = theta.A*diag(theta.Lambda(:,k))*theta.A';
        grad_C           = gradfun_SPD(C,x(:,:,k));
        grad.A           = grad.A + 2*grad_C*theta.A*diag(theta.Lambda(:,k))/K;
        grad.Lambda(:,k) = diag(theta.A'*grad_C*theta.A)/K;
    end
end