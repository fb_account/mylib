function crit = critMoAm(A,B)

% crit = critMoAm(A,B)
%
% function computing the Moreau-Amari index for matrices A and B. This is
% usually used to estimate the performances of (joint) blind source
% separation algorithms when both the mixing and unmixing matrices are
% known.
%
% Inputs:
%   - A: nxpxM array containing true mixing matrices used to simulate data
%   - B: pxnxM array containing estimated unmixing matrices
%
% where n, p and M correspond to the number of channels, sources and 
% datasets respectively.
%
% Output:
%   - crit: 1xM vector containing the Moreau-Amari index for each dataset
%
% reference: SELF-ADAPTIVE SOURCE SEPARATION BY DIRECT AND RECURSIVE
%            NETWORKS. O. Macchi, E. Moreau
%            Proc. International Conference on Digital Signal Processing
%            (DSP93), 1993.
%
% *** History: 18-Feb-2015
% *** Author: Florent BOUCHARD, GIPSA-Lab, 2015

crit=NaN;
if any([isempty(A) isempty(B)])
    return;
else
    % get variables
    p = size(A,2);
    % M = size(A,3);
    
    % normalize columns of A and lines of B to avoid scale indeterminancy issues
    A = multinormc(A);
    B = multitransp(multinormc(multitransp(B)));
    
    % compute product B'A
    P = multiprod(B,A);
    
    % rows
    sr = sum(squeeze(sum(abs(P),1)./max(abs(P),[],1)-1));
    % columns
    sc = sum(squeeze(sum(abs(P),2)./max(abs(P),[],2)-1));
    
    crit = (sr+sc)/2/p/(p-1);
end
end

%% old code using for loops
% % initialize output structure
% crit = zeros(1,M);
%
% for m=1:M
%     % normalize columns of A and B to avoid scale indeterminancy issues
%     for i=1:N
%         A(:,i,m) = A(:,i,m)/sqrt(A(:,i,m)'*A(:,i,m));
%         B(i,:,m) = B(i,:,m)/sqrt(B(i,:,m)*B(i,:,m)');
%     end
%     % compute product B*A
%     P = B(:,:,m)*A(:,:,m);
%     s = 0;
%     % rows
%     for i=1:p
%         s = s + sum(abs(P(i,:)))/max(abs(P(i,:))) - 1;
%     end
%     % columns
%     for i=1:p
%         s = s + sum(abs(P(:,i)))/max(abs(P(:,i))) - 1;
%     end
%     crit(m) = s/2/p/(p-1);
% end

