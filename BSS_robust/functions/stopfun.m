function stopnow = stopfun(problem,x,info,last)
% stopping criterion function
    stopnow = (last>=2 && BSSerrormeasure(x,info(last-1).theta) < problem.tol); % ### ok
end

