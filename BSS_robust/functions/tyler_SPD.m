function [costfun,gradfun] = tyler_SPD()
    costfun    = @(R,x)    aux_cost(R,x);
    gradfun    = @(R,x)    aux_grad(R,x);
end

function cost=aux_cost(R,x)
    [p,n]   = size(x);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
%     cR = chol(R)';
%     v  = cR\x;
    a       = real(sum(v.*conj(v)));
    %
    cost    = n*log(real(det(R))) + p*sum(log(a));
    cost = cost/p/n;
end

function grad=aux_grad(R,x)
    [p,n] = size(x);
    psi   = aux_psi(R,x); 
    grad  = R\(n*R - p*psi)/R;
    grad  = grad/p/n;
end


function psi = aux_psi(R,x) % ok
    p       = size(x,1);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
%     cR = chol(R)';
%     v  = cR\x;
    a       = sum(v.*conj(v));
    y       = x./ sqrt(a(ones(p,1),:));
    psi     = y*y';
end