function [costfun,gradfun] = MLE_tdist_SPD(d)
    costfun    = @(R,x)    aux_cost(R,x,d);
    gradfun    = @(R,x)    aux_grad(R,x,d);
end

function cost=aux_cost(R,x,d)
    [p,n]   = size(x);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
%     cR = chol(R)';
%     v  = cR\x;
    a       = real(sum(v.*conj(v)));
    %
    cost    = n*log(real(det(R))) + (d+p)*sum(log(d+a));
    cost = cost/(d+p)/n;
end

function grad=aux_grad(R,x,d)
    [p,n] = size(x);
    psi   = aux_psi(R,x,d); 
    grad  = R\(n*R - (d+p)*psi)/R;
    grad  = grad/(d+p)/n;
end


function psi = aux_psi(R,x,d) % ok
    p       = size(x,1);
    [Ur,Lr] = svd(R);
    cR      = diag(diag(Lr).^(-0.5))*Ur';
    v       = cR * x;
%     cR = chol(R)';
%     v  = cR\x;
    a       = sum(v.*conj(v));
    y       = x./ sqrt(d + a(ones(p,1),:));
    psi     = y*y';
end