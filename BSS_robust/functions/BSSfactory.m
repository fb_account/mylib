function M = BSSfactory(n,K)

M.name = @() sprintf('manifold GL(%d)x(D^++(%d))^%d, sum of diagonal matrices normalized',n,n,K);

M.dim = @() n^2 + n*(K-1);

M.inner = @inner;
function in = inner(X,xi,eta)
    in = trace((xi.A/X.A)'*(eta.A/X.A));
    in = in + sum(sum((X.Lambda.^-2).*xi.Lambda.*eta.Lambda));
%     for k=1:size(X.Lambda,3)
%         in = in + sum(diag(X.Lambda(:,:,k)).^-2.*diag(xi.Lambda(:,:,k)).*diag(eta.Lambda(:,:,k)));
%     end
end

M.norm = @(X, xi) sqrt(M.inner(X, xi, xi));

M.dist = @(x, y) error('BSSfactory.dist not implemented yet.');

M.typicaldist = @() sqrt(n^2 + n*(K-1));

M.proj = @projection;
function xi = projection(X, Z)
    xi.A = Z.A;
    
    tmp1      = sum(X.Lambda.^2,2);
    tmp2      = sum(Z.Lambda,2);
    tmp       = tmp2./tmp1;
    xi.Lambda = Z.Lambda - tmp.*X.Lambda.^2;
    
%     tmp1 = zeros(1,size(X.Lambda,1));
%     tmp2 = zeros(1,size(X.Lambda,1));
%     for k=1:size(X.Lambda,3)
%         tmp1 = tmp1 + diag(X.Lambda(:,:,k)).^2;
%         tmp2 = tmp2 + diag(Z.Lambda(:,:,k));
%     end
%     tmp       = tmp2./tmp1;
%     xi.Lambda = zeros(size(X.Lambda));
%     for k=1:size(X.Lambda,3)
%         xi.Lambda(:,:,k) = Z.Lambda(:,:,k) - diag(tmp.*diag(X.Lambda(:,:,k)).^2);
%     end
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    rgrad.A      = egrad.A*(X.A'*X.A);
    rgrad.Lambda = X.Lambda.^2.*egrad.Lambda;
    
    rgrad = projection(X,rgrad);
    
%     rgrad.Lambda = zeros(size(X.Lambda));
%     for k=1:size(X.Lambda,3)
%         rgrad.Lambda(:,:,k) = diag(diag(X.Lambda(:,:,k)).^2.*egrad.Lambda(:,:,k));
%     end
end

M.retr = @retraction;
function Y = retraction(X,xi,t)
    if nargin<3
        t=1.0;
    end
    
    tmp      = (X.A'*X.A)\xi.A';
    Y.A      = X.A + t*xi.A +.5*t^2*(xi.A*(X.A\xi.A) + xi.A*tmp*X.A - X.A*tmp*xi.A);
    Y.Lambda = X.Lambda + t*xi.Lambda + .5*t^2*(xi.Lambda.^2./X.Lambda);
    
    Y = projonman(Y);
end

M.exp = @retraction;


M.transp = @transport;
function eta2 = transport(x1,x2,eta1)
    eta2 = projection(x2,eta1);
end

M.rand = @random;
function X = random()
    X.A      = randn(n);
    X.Lambda = randn(n,K).^2;
    
    X = projonman(X);
end

M.randvec = @(X) randomvec(X,M.norm);
function xi = randomvec(X,normfun)
    xi.A      = randn(n);
    xi.Lambda = randn(n,K);
    xi        = projection(X,xi);
    nrm       = normfun(X,xi);
    xi.A      = xi.A / nrm;
    xi.Lambda = xi.Lambda /nrm;
end

M.lincomb = @lincomb;
function d = lincomb(x, a1, d1, a2, d2)
    
    if nargin == 3
        d.A      = a1*d1.A;
        d.Lambda = a1*d1.Lambda;
    elseif nargin == 5
        d.A      = a1*d1.A + a2*d2.A;
        d.Lambda = a1*d1.Lambda + a2*d2.Lambda;
    else
        error('Bad use of BSSfactory.lincomb.');
    end
end

M.zerovec = @(X) struct('A', zeros(n), 'Lambda', zeros(n,K));

M.projonman = @projonman;
function Y = projonman(X)
    tmp = sum(X.Lambda,2);
    Y   = struct('A', X.A,'Lambda',X.Lambda./tmp);
end

end