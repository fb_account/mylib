function err = BSSerrormeasure(theta1,theta2)

K = size(theta1.Lambda,2);
if size(theta2.Lambda,2)~=K, error('BSSerrormeasure - theta1 and theta2 inconsistent'); end

err = 0;
for k=1:K
    C1      = theta1.A*diag(theta1.Lambda(:,k))*theta1.A';
    C2      = theta2.A*diag(theta2.Lambda(:,k))*theta2.A';
    [U1,D1] = svd(C1);
    C1_i12  = U1*diag(diag(D1).^-.5)*U1';
    L       = svd(C1_i12*C2*C1_i12);
    err     = err + sum(log(L).^2)/K;
end