function [dist] = dist_nat(R1,R2)
        lambda = log(eig(R1,R2));
        dist =  real(sum(lambda.^2)) ;
end
