function x = multinormc(x)

% function that extends normc matlab function to multi-dimensional arrays.
% Matrix representations are supposed to be over the two first dimensions.
%
% Input:
%   - x: n x p x... array
%
% Output:
%   - x: n x p x... array normalized over the second dimension
%
% *** History: 05-Jan-2015
% *** Author: Florent BOUCHARD, GIPSA-Lab, 2015

sz = size(x);
if length(sz) == 2
    sz(3) = 1;
end
n = sz(1);
% p = sz(2);
norms = sqrt(multiprod(ones([n,1,sz(3:end)]),multidiag(multiprod(multitransp(x),x))));
x = x./norms;
end