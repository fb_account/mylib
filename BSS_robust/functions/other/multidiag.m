function [d,x_diag,x_off] = multidiag(x)

% function that returns the diagonal elements of a multi-dimensional array.
% Matrix representations are supposed to be over the two first dimensions.
%
% Input:
%   - x: n x p x... array
%
% Outputs:
%   - d:      1 x min(n,p) x... array containing the diagonal elements of x
%             over the two first dimensions
%   - x_diag: n x p x... array containing the diagonal part of x
%   - x_off:  n x p x... array containing the off-diagonal part of x
%
% *** History: 05-Jan-2015
% *** Author: Florent BOUCHARD, GIPSA-Lab, 2015

sz = size(x);
if length(sz) == 2
    sz(3) = 1;
end
n = sz(1);
p = sz(2);
l = prod(sz)/n/p;
% get indices corresponding to the diagonal elements
k = (1:n+1:n*p)';
k = repmat(k,1,l);
k = bsxfun(@plus,k,(0:(l-1))*n*p);
k = reshape(k,[1,min(n,p),sz(3:end)]);
% get outputs
d        = x(k);
x_off    = x;
x_off(k) = 0;
x_diag   = x - x_off;
end