function M = GL_diagposdef_factory(n,K)


M.name = @() sprintf('manifold GL(%d)x(D^++(%d))^%d',n,n,K);

M.dim = @() n^2 + n*(K-1);

M.inner = @inner;
function in = inner(X,xi,eta)
    in = trace((xi.A/X.A)'*(eta.A/X.A));
    in = in + sum(sum((X.Lambda.^-2).*xi.Lambda.*eta.Lambda));
end

M.norm = @(X, xi) sqrt(M.inner(X, xi, xi));

M.dist = @(x, y) error('GL_diagposdef_factory.dist not implemented yet.');

M.typicaldist = @() sqrt(n^2 + n*(K-1));

M.proj = @projection;
function xi = projection(X, Z)
    xi = Z;
end

M.tangent = M.proj;

M.egrad2rgrad = @egrad2rgrad;
function rgrad = egrad2rgrad(X, egrad)
    rgrad.A      = egrad.A*(X.A'*X.A);
    rgrad.Lambda = X.Lambda.^2.*egrad.Lambda;
end

M.retr = @retraction;
function Y = retraction(X,xi,t)
    if nargin<3
        t=1.0;
    end
    tmp     = (X.A'*X.A)\xi.A';
    YA      = X.A + t*xi.A +.5*t^2*(xi.A*(X.A\xi.A) + xi.A*tmp*X.A - X.A*tmp*xi.A);
    YLambda = X.Lambda + t*xi.Lambda + .5*t^2*(xi.Lambda.^2./X.Lambda);
    Y = struct('A',YA,'Lambda',YLambda);
end

M.exp = @exponential;
function Y = exponential(X,xi,t)
    if nargin<3
        t=1.0;
    end
    tmp1    = xi.A/X.A;
    tmp2    = tmp1 - tmp1';
    YA      = expm(t*tmp2)*expm(t*tmp1')*X.A;
%     some fix to correct eventual numerical instability
    % (may occur during linesearch)
    while rcond(YA)<1e-10 || isnan(rcond(YA))
        t = t/2;
        YA = expm(t*tmp2)*expm(t*tmp1')*X.A;
    end 
    YLambda = X.Lambda.*exp(t*xi.Lambda./X.Lambda);
    Y       = struct('A',YA,'Lambda',YLambda);
end

M.transp = @transport;
function eta2 = transport(x1,x2,eta1)
    eta2 = projection(x2,eta1);
end

M.rand = @() struct('A',randn(n),'Lambda',randn(n,K).^2);

M.randvec = @(X) randomvec(X,M.norm);
function xi = randomvec(X,normfun)
    xi  = struct('A',randn(size(X.A)),'Lambda',randn(size(X.Lambda)));
    nrm = normfun(X,xi);
    xi  = struct('A',xi.A/nrm,'Lambda',xi.Lambda/nrm);
end

M.lincomb = @lincomb;
function d = lincomb(x, a1, d1, a2, d2)
    
    if nargin == 3
        d.A      = a1*d1.A;
        d.Lambda = a1*d1.Lambda;
    elseif nargin == 5
        d.A      = a1*d1.A + a2*d2.A;
        d.Lambda = a1*d1.Lambda + a2*d2.Lambda;
    else
        error('Bad use of GL_diagposdef_factory.lincomb.');
    end
end

M.zerovec = @(X) struct('A', zeros(n), 'Lambda', zeros(n,K));

end