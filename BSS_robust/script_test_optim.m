%% some parameters
n     = 10;  % matrix size
K     = 30;  % number of covariance matrices
T     = 50; % sample sizes
d     = 3;   % degrees of freedom of the t-distribution
condA = 10;  % conditioning of the mixing matrix

%% generate some covariance matrices
% generate source powers
Lambda = randn(n,K).^2;
% mixing matrix with fixed conditionning
tmp = randn(n);
[U,~,V] = svd(tmp);
l_min = 1/sqrt(condA);
l_max = sqrt(condA);
l = (l_max-l_min).*rand(n-2,1) + l_min;
l_all = [l_min;l_max;l];
L = diag(l_all(randperm(n)));
A = U*L*V';

% for k=1:K
%     while cond(A*diag(Lambda(:,k))*A') > 1e3
%         Lambda(:,k) = randn(n,1).^2;
%     end
% end

% for k=1:K
%     Lambda(:,k) = Lambda(:,k)/trace(A*diag(Lambda(:,k))*A');
% %     trace(A*diag(Lambda(:,k))*A')
% end

% BSSfactory structure
theta = struct('A',A,'Lambda',Lambda);

man = BSSfactory(n,K);
theta = man.projonman(theta);

%% generate some data
x = zeros(n,T,K);
for k=1:K
    C     = theta.A*diag(theta.Lambda(:,k))*theta.A';
    [~,p] = chol(C);
    if p~=0
        disp(['generate_data: C(:,:,' int2str(k) ') not positive definite']);
    end
     x(:,:,k) = gene_tdistrib_real(n,T,C,d);
%      x(:,:,k) = chol(C)'*randn(n,T);
end

%% check gradient

% man = symmetricfactory(n);
man = sympositivedefinitefactory(n);
[cost_tyler_SPD,grad_tyler_SPD] = tyler_SPD();
[cost_tdist_SPD,grad_tdist_SPD] = MLE_tdist_SPD(d);
problem   = [];
problem.M = man;
problem.cost  = @(C) cost_tyler_SPD(C,x(:,:,1));
problem.egrad = @(C) grad_tyler_SPD(C,x(:,:,1));

Cscm = x(:,:,1)*x(:,:,1)'/T;

R_tmp=randn(n);
R_tmp=R_tmp*R_tmp';
xi_tmp = randn(n);
% xi_tmp=xi_tmp*xi_tmp';
xi_tmp = .5*(xi_tmp+xi_tmp');
xi_tmp = xi_tmp / norm(xi_tmp(:), 'fro');

figure;checkgradient(problem,Cscm,xi_tmp);

%% other
% man = GL_diagposdef_factory(n,K);
man = BSSfactory(n,K);

problem   = [];

% man = productmanifold(struct('A',euclideanfactory(n,n),'Lambda',euclideanfactory(n,K)));

problem.M = man;

[costfun,gradfun] = SPD2BSS(x,cost_tdist_SPD,grad_tdist_SPD);

A_tmp = randn(n);
Lambda_tmp = randn(n,K).^2;
theta_tmp = struct('A',A_tmp,'Lambda',Lambda_tmp);
theta_tmp = man.projonman(theta_tmp);

xi_tmp = man.randvec(theta_tmp);

problem.cost  = costfun;
problem.egrad = gradfun;

Cscm = zeros(n,n,K);
for k=1:K
    Cscm(:,:,k) = x(:,:,k)*x(:,:,k)'/T;
end
L = chol(mean(Cscm,3),'lower');
A0 = L;
Lambda0 = zeros(n,K);
for k=1:K
    Lambda0(:,k) = diag(A0\Cscm(:,:,k)/A0');
%     cond(A0*diag(Lambda0(:,k))*A0')
end
theta0 = struct('A',A0,'Lambda',Lambda0);
theta0 = man.projonman(theta0);

figure;checkgradient(problem,theta0,xi_tmp);

%%
options = struct('maxiter',100,'verbosity',2);%,'statsfun',@statsfun,'stopfun',@stopfun);
[theta_tyler,~,info_tyler] = trustregions(problem,theta0,options);
